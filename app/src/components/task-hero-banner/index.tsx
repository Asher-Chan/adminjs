import React from "react";
import Image from "next/image";
import Link from "next/link";

import styles from "./index.module.scss";

type Props = {
  taskPosition: number;
  moduleId: number;
  moduleTitle: string;
  companyLogoURL: string;
  taskOverviewImgURL: string;
  taskTitle: string;
};

const TaskHeroBanner: React.FC<Props> = ({
  taskOverviewImgURL,
  taskPosition,
  taskTitle,
  moduleTitle,
  companyLogoURL,
  moduleId,
}) => {
  return (
    <div className={styles.container}>
      {/* Module Details */}
      <div className={styles.modulesDetailsContainer}>
        <Image
          src={companyLogoURL}
          width="180px"
          height="70px"
          alt="Company Logo"
        />
        <h1>{moduleTitle}</h1>
        <p>{`Task ${taskPosition}: ${taskTitle}`}</p>
        <Link href={`/modules/overview/${moduleId}`}>
          <a className={styles.moduleOverviewLink}>
            Module Overview | Task {taskPosition}
          </a>
        </Link>
      </div>

      {/* Task Opening Image */}
      <div>
        {!!taskOverviewImgURL && (
          <Image
            src={taskOverviewImgURL}
            width="300px"
            height="300px"
            alt="task opening image"
          />
        )}
      </div>
    </div>
  );
};

export default TaskHeroBanner;
