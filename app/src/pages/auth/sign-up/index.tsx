import React from "react";
import { getSession, signIn } from "next-auth/react";
import { GetServerSideProps, NextPage } from "next";

import TextInput from "@/components/inputs/text-input";
import Button from "@/components/button";

import styles from "./index.module.scss";

type Props = {
  error: boolean;
  errorMessage?: string;
};

const Register: NextPage<Props> = ({ error, errorMessage }) => {
  return (
    <>
      <h4>Create a New Account</h4>

      <Button
        label="Google Sign Up"
        onClickHandler={() => signIn("google", { redirect: true })}
        variant="secondary"
      />

      <form action="/api/auth/sign-up" method="POST">
        <div className={styles.divider} />
        {error && errorMessage && <div>{errorMessage}</div>}
        <TextInput label="Full Name" name="fullName" type="text" />
        <TextInput label="Email" name="email" type="email" />
        <TextInput label="Password" name="password" type="password" />
        <div className={styles.buttonContainer}>
          <Button type="submit" label="Sign Up" />
        </div>
      </form>
    </>
  );
};

export default Register;

export const getServerSideProps: GetServerSideProps<Props> = async (ctx) => {
  const session = await getSession(ctx);

  if (session?.auth) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  const { error, emailExists } = ctx.query as {
    error?: string;
    emailExists?: string;
  };

  if (error && error === "true") {
    return {
      props: {
        error: true,
        errorMessage: "Invalid email or password",
      },
    };
  }

  if (emailExists && emailExists === "true") {
    return {
      props: {
        error: true,
        errorMessage: "Email already in use",
      },
    };
  }

  return {
    props: {
      error: false,
    },
  };
};
