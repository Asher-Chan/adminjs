import {
  ActionResponse,
  ActionContext,
  BaseRecord,
  ActionRequest,
} from 'adminjs';

import { Module } from '../../../models';
import { resourceIds } from '../../../constants/resource-ids';

export const populateModulesData = async (
  originalResponse: ActionResponse,
  req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const companyModules = await Module.findMany({
    where: {
      companyId: parseInt(req.params.recordId),
    },
  });

  // eslint-disable-next-line no-underscore-dangle
  const moduleResource = context._admin.findResource(resourceIds.MODULES);

  const moduleRecords = companyModules?.map((module) =>
    new BaseRecord(
      {
        ...module,
        itemLink: context.h.showUrl(resourceIds.MODULES, module.id.toString()),
      },
      moduleResource
    ).toJSON(context.currentAdmin)
  );

  // Combined with companyId, for direct add module no need to select a company
  const addNewLink = context.h.newUrl(
    resourceIds.MODULES,
    `?companyId=${req.params.recordId}`
  );

  return {
    ...originalResponse,
    record: {
      ...record,
      params: {
        ...record.params,
        addNewLink,
      },
      populated: {
        ...record.populated,
        modules: moduleRecords,
      },
    },
  };
};
