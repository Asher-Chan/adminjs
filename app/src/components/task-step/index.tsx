import React from "react";
import Image from "next/image";
import { Step, StepResource, FileTypes } from "@prisma/client";

import styles from "./index.module.scss";

export type StepWithResource = Step & {
  stepResources?: StepResource[];
};

type Props = {
  step: StepWithResource;
  stepPosition: number;
};

const TaskStep: React.FC<Props> = ({ step, stepPosition }) => {
  const mediaURLs: string[] = step.mediaURLs
    ? JSON.parse(step.mediaURLs as string)
    : [];

  const isMediaVideo = (mediaURL: string): boolean => {
    const splitURL = mediaURL.split(".");
    const fileExtension = splitURL[splitURL.length - 1];

    return ["mp4", "mov", "avi", "wmv"].includes(fileExtension);
  };

  const getResourceDetails = (
    type: FileTypes
  ): { iconName: string; buttonLabel: string; onClickHandler: () => void } => {
    let iconName = "";
    let buttonLabel = "";
    let onClickHandler = () => ({});

    // Use switch case

    if (type === "WEBSITE_LINK") {
      buttonLabel = "Go to website";
      iconName = "WEBSITE_ICON";
    }

    if (type === "PDF") {
      buttonLabel = "Download PDF";
      iconName = "PDF";
    }

    if (type === "DOCUMENT") {
      buttonLabel = "Download Document";
      iconName = "FOLDER";
    }

    if (type === "ZIP") {
      buttonLabel = "Download zipped file";
      iconName = "ZIP";
    }

    return {
      iconName,
      buttonLabel,
      onClickHandler,
    };
  };

  return (
    <div className={styles.container}>
      {/* Step Details */}
      <h3>{stepPosition}</h3>
      <div>{step.title}</div>
      {step.desc && <div dangerouslySetInnerHTML={{ __html: step.desc }} />}

      {/* Step Media(s) */}
      {!!mediaURLs.length &&
        mediaURLs.map((media) => {
          if (isMediaVideo(media)) {
            return <video key={media} src={media}></video>;
          }

          return (
            <Image
              key={media}
              src={media}
              alt="media"
              width="600px"
              height="350px"
            />
          );
        })}

      {/* Step Resources */}
      {!!step.stepResources?.length &&
        step.stepResources.map((resource) => {
          const { buttonLabel, iconName, onClickHandler } = getResourceDetails(
            resource.type
          );

          return (
            <div key={resource.id}>
              <div>{iconName}</div>
              <div>{resource.title}</div>
              <p>{resource.desc}</p>
              <button>{buttonLabel}</button>
            </div>
          );
        })}
    </div>
  );
};

export default TaskStep;
