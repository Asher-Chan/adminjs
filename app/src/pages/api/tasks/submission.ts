import { v2 as cloudinary } from "cloudinary";
import multiparty from "multiparty";

import { prisma } from "@/utils/db";
import { NextAPIRes, NextAPIReq } from "@/types/index";

// Put in ENV
cloudinary.config({
  cloud_name: "baked-potatoes",
  api_key: "842563187481379",
  api_secret: "PGvJsBKSuYksUsNWbomNRLDpIH4",
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async function handler(req: NextAPIReq, res: NextAPIRes) {
  const form = new multiparty.Form();

  // Get talentId from context? or passed in body
  // Get taskId from body
  // Get moduleId from body

  // Just for testing
  const talentId = 1;
  const taskId = 2;
  const moduleId = 1;

  let moduleProgress = await prisma.moduleProgress.findFirst({
    where: {
      talentId,
      moduleId,
    },
  });

  if (!moduleProgress) {
    res
      .status(400)
      .json({ error: true, errorMessage: "Talent not enrolled in module" });
    return;
  }

  // Likely need to to an extra check
  // If after submitting this, all tasks are completed then update `moduleProgress` to completed, eligible for certificate etc
  // Or this might be done after company has checked submissions

  try {
    form.parse(req, async (err, _fields, files) => {
      if (err) {
        throw new Error("Error parsing form");
      }

      const url = await cloudinary.uploader.upload(
        files.file[0].path,
        { resource_type: "raw" },
        (err) => {
          if (err) {
            console.log(err);
            throw new Error("Error uploading image");
          }
        }
      );

      if (moduleProgress?.id) {
        await prisma.submission.create({
          data: {
            status: "COMPLETED",
            moduleProgressId: moduleProgress.id,
            talentId,
            taskId,
            submissionURL: url.url,
          },
        });
      }
    });

    res.status(200).redirect(`/modules/task/${taskId}`);
  } catch (err) {
    const errMessage = !(err instanceof Error)
      ? "An error occured"
      : err.message;
    res.status(400).json({ error: true, errorMessage: errMessage });
  }
}
