import React, {ChangeEvent} from "react";

import styles from "./index.module.scss";

type Props = {
  name: string;
  label: string;
  type?: string;
  value?: string | number;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
};

const TextInput: React.FC<Props> = ({
  name,
  value,
  onChange,
  label,
  type = "text",
  required = false,
}) => {
  return (
    <div className={styles.inputContainer}>
      <label className={styles.label}>{label}</label>
      <input
        name={name}
        className={styles.input}
        value={value}
        onChange={(e) => onChange?.(e)}
        type={type}
        required={required}
      />
    </div>
  );
};

export default TextInput;
