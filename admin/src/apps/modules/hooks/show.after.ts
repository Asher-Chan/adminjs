import {
  ActionResponse,
  ActionContext,
  BaseRecord,
  ActionRequest,
} from 'adminjs';

import { Task } from '../../../models';
import { resourceIds } from '../../../constants/resource-ids';

export const populateTasksData = async (
  originalResponse: ActionResponse,
  req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const moduleTasks = await Task.findMany({
    where: {
      moduleId: parseInt(req.params.recordId),
    },
    orderBy: {
      createdAt: 'asc',
    },
  });

  // eslint-disable-next-line no-underscore-dangle
  const taskResource = context._admin.findResource(resourceIds.TASKS);

  const taskRecords = moduleTasks?.map((task) =>
    new BaseRecord(
      {
        ...task,
        itemLink: context.h.showUrl(resourceIds.TASKS, task.id.toString()),
      },
      taskResource
    ).toJSON(context.currentAdmin)
  );

  // Combined with companyId, for direct add task no need to select module
  const addNewLink = context.h.newUrl(
    resourceIds.TASKS,
    `?moduleId=${req.params.recordId}`
  );

  return {
    ...originalResponse,
    record: {
      ...record,
      params: {
        ...record.params,
        addNewLink,
      },
      populated: {
        ...record.populated,
        tasks: taskRecords,
      },
    },
  };
};
