AdminJS.UserComponents = {}
import LoggedIn from '../src/admin/components/user-nav'
AdminJS.UserComponents.LoggedIn = LoggedIn
import Component2 from '../src/admin/components/dashboard'
AdminJS.UserComponents.Component2 = Component2
import Component3 from '../src/admin/components/parent-record'
AdminJS.UserComponents.Component3 = Component3
import Component4 from '../src/admin/components/address.show'
AdminJS.UserComponents.Component4 = Component4
import Component5 from '../src/admin/components/display-name.edit'
AdminJS.UserComponents.Component5 = Component5
import Component6 from '../src/admin/components/image-upload.edit'
AdminJS.UserComponents.Component6 = Component6
import Component7 from '../src/admin/components/image-view.show'
AdminJS.UserComponents.Component7 = Component7
import Component8 from '../src/admin/components/multi-image-view.show'
AdminJS.UserComponents.Component8 = Component8
import Component9 from '../src/admin/components/steps.show'
AdminJS.UserComponents.Component9 = Component9
import Component10 from '../src/admin/components/tasks-list.show'
AdminJS.UserComponents.Component10 = Component10
import Component11 from '../src/apps/companies/components/brand-color.edit'
AdminJS.UserComponents.Component11 = Component11
import Component12 from '../src/apps/companies/components/brand-color.show'
AdminJS.UserComponents.Component12 = Component12
import Component13 from '../src/apps/companies/components/modules.show'
AdminJS.UserComponents.Component13 = Component13
import Component14 from '../src/apps/modules/components/completion-time.edit'
AdminJS.UserComponents.Component14 = Component14
import Component15 from '../src/apps/module-progress/components/certificate-status'
AdminJS.UserComponents.Component15 = Component15
import Component16 from '../src/apps/module-progress/components/duration.list'
AdminJS.UserComponents.Component16 = Component16
import Component17 from '../src/apps/module-progress/components/task-submission'
AdminJS.UserComponents.Component17 = Component17
import Component18 from '../src/apps/module-progress/components/view-submissions'
AdminJS.UserComponents.Component18 = Component18
import Component19 from '../src/apps/steps/components/multi-image.edit'
AdminJS.UserComponents.Component19 = Component19
import Component20 from '../src/apps/steps/components/resources.edit'
AdminJS.UserComponents.Component20 = Component20
import Component21 from '../src/apps/steps/components/resources.show'
AdminJS.UserComponents.Component21 = Component21
import Component22 from '../src/apps/tasks/components/multi-creatable-select'
AdminJS.UserComponents.Component22 = Component22
import Component23 from '../src/apps/tasks/components/tags.show'
AdminJS.UserComponents.Component23 = Component23
import Component24 from '../src/apps/certificates/components/file-url'
AdminJS.UserComponents.Component24 = Component24
import Component25 from '../src/admin/components/cert-generator-page'
AdminJS.UserComponents.Component25 = Component25