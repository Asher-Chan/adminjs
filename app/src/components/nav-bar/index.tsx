import React from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { signOut, useSession, signIn } from "next-auth/react";

import UpsurgeLogo from "../../public/upsurge-logo-full.png";

import Button from "../button";

import styles from "./index.module.scss";

const NavBar: React.FC = () => {
  const { data, status } = useSession();
  const router = useRouter();

  return (
    <nav className={styles.navbar}>
      <Image
        src={UpsurgeLogo}
        className={styles.logo}
        alt="Upsurge Logo"
        onClick={() => router.push("/")}
      />

      <ul className={styles.navMenu}>
        <Link href="/modules">
          <li>Modules</li>
        </Link>

        {status === "authenticated" && (
          <Link href="/dashboard/my-modules">
            <li>My Modules</li>
          </Link>
        )}

        <Link href="/contact-us">
          <li>Contact Us</li>
        </Link>

        <Link href="/become-hiring-partner">
          <li>Become Hiring Partner</li>
        </Link>

        {status !== "authenticated" && (
          <Link href="/auth/company/sign-in">
            <li>Company Log In</li>
          </Link>
        )}
      </ul>

      <div className={styles.userContainer}>
        {status === "authenticated" && (
          <div className={styles.user}>
            <div className={styles.email}>{data.auth.email}</div>
            <div className={styles.name}>{data.auth.fullName}</div>
          </div>
        )}

        {status === "authenticated" ? (
          <Button
            label="Sign Out"
            onClickHandler={() =>
              signOut({
                callbackUrl: "/auth/sign-in",
              })
            }
          />
        ) : (
          <>
            <Button
              label="Sign Up"
              onClickHandler={() => router.push("/auth/sign-up")}
              variant="secondary"
            />
            <Button label="Sign In" onClickHandler={() => signIn()} />
          </>
        )}
      </div>
    </nav>
  );
};

export default NavBar;
