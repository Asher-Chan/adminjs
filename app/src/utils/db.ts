import { PrismaClient } from "@prisma/client";

// Put all the middleware here
// Eg may need: Whenever fetching a talent or company, auto populate their data from their user model

export const prisma = new PrismaClient();
