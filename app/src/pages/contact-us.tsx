import React from "react";
import { NextPage } from "next";
import { useMutation } from "react-query";
import { useForm, FormProvider } from "react-hook-form";

import TextInput from "@/components/form-inputs/text-input";
import Button from "@/components/button";

type FormValues = {
  name: string;
  email: string;
  messageTitle: string;
  message: string;
};

const ContactUs: NextPage = () => {
  const formMethods = useForm<FormValues>({
    defaultValues: {
      name: "",
      email: "",
      messageTitle: "",
      message: "",
    },
  });

  const {
    handleSubmit,
    reset,
    formState: { isSubmitting },
  } = formMethods;

  const { mutate, isLoading, data } = useMutation(
    async (formValues: FormValues) => {
      try {
        const res = await fetch("/api/contact-us", {
          method: "POST",
          body: JSON.stringify({
            ...formValues,
          }),
          headers: {
            "Content-Type": "application/json",
          },
        });
        reset();
        return await res.json();
      } catch (err) {
        throw err;
      }
    }
  );

  return (
    <div>
      <h3>Contact Us</h3>

      <FormProvider {...formMethods}>
        <form onSubmit={handleSubmit((d) => mutate(d))}>
          <TextInput label="Name" name="name" type="text" required />

          <TextInput label="Email" name="email" type="email" required />

          <TextInput
            label="Message Title"
            name="messageTitle"
            type="text"
            required
          />

          <TextInput
            label="Message"
            name="message"
            type="text-area"
            rows={8}
            required
          />

          <Button
            type="submit"
            disabled={isLoading || isSubmitting}
            label="Submit"
          />
        </form>
      </FormProvider>
    </div>
  );
};

export default ContactUs;
