import React, { ReactElement } from "react";

import { requireAuthentication } from "@/utils/auth";
import { NextPageWithLayout } from "@/types/index";
import DashboardLayout from "@/layouts/dashboard-layout";

type Props = {};

const Distinctions: NextPageWithLayout<Props> = ({}) => {
  return <div>Dashboard - My Distinctions</div>;
};

export default Distinctions;

export const getServerSideProps = requireAuthentication<Props>(async (ctx) => {
  return {
    props: {},
  };
});

Distinctions.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
