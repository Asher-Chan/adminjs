import React from "react";
import { NextPage, GetServerSideProps } from "next";
import { Company, Module, SubmissionStatus } from "@prisma/client";

import ModuleHeroBanner from "@/components/module-hero-banner";
import TaskList, { TaskWithTags } from "@/components/task-list";
import { prisma } from "@/utils/db";

// Should only be able to access this page if there is a moduleProgress attached to current talent & module

type Props = {
  module: Module & {
    company?: Company;
  };
  tasks: TaskWithTags[];
  numOfTalentsEnrolled: number;
  progressPercentage: number;
  moduleStats: {
    certifiedTalents: number;
    moduleSubmissions: number;
    talentAcquistions: number;
  };
};

const ModuleOverview: NextPage<Props> = ({
  module,
  tasks,
  numOfTalentsEnrolled,
  progressPercentage,
  moduleStats,
}) => {
  return (
    <div>
      <ModuleHeroBanner
        companyLogoURL={module.company?.logoURL || ""}
        moduleId={module.id}
        moduleTitle={module.title}
        openingImgURL={module.openingImageURL}
        numOfTalentsEnrolled={numOfTalentsEnrolled}
        numOfTasks={tasks.length}
      />

      <div>
        <TaskList tasks={tasks} moduleId={module.id} />
      </div>
      <div>
        <div>Your Progress: ({progressPercentage}%)</div>
        {tasks.map((task) => {
          return <div key={task.id}>{task.shorthandTitle}</div>;
        })}
      </div>
    </div>
  );
};

export default ModuleOverview;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const moduleId = parseInt(ctx.query.id as string);

  const selectedModule = await prisma.module.findUnique({
    where: {
      id: moduleId,
    },
    include: {
      company: true,
      tasks: {
        select: {
          tags: true,
          desc: true,
          id: true,
          title: true,
          shorthandTitle: true,
        },
        orderBy: {
          createdAt: "asc",
        },
      },
    },
  });

  const numOfTalentsEnrolled =
    (await prisma.moduleProgress.count({
      where: {
        moduleId,
      },
    })) || 0;

  // Either just get count, or findMany and verify as well
  const talentSubmissions = await prisma.submission.count({
    where: {
      talentId: 1, // make this dynamic
      status: SubmissionStatus.COMPLETED,
      task: {
        id: {
          // Possible to have duplicates here?
          in: selectedModule?.tasks?.map((task) => task.id),
        },
      },
    },
  });

  const moduleSubmissions = await prisma.submission.count({
    where: {
      task: {
        id: {
          // Possible to have duplicates here?
          in: selectedModule?.tasks?.map((task) => task.id),
        },
      },
    },
  });

  const certifiedTalents = await prisma.certificate.count({
    where: {
      moduleId,
    },
  });

  const progressPercentage =
    Math.round(
      (talentSubmissions / (selectedModule?.tasks?.length || 0)) * 100
    ) || 0;

  const props: Props = {
    module: JSON.parse(JSON.stringify(selectedModule)),
    tasks: selectedModule?.tasks || [],
    numOfTalentsEnrolled,
    moduleStats: {
      moduleSubmissions,
      talentAcquistions: 0, // Cannot be done
      certifiedTalents,
    },
    progressPercentage,
  };

  return {
    props,
  };
};
