import {
  ActionContext,
  ActionRequest,
  ActionResponse,
  BaseRecord,
} from 'adminjs';
import { resourceIds } from '../../../constants/resource-ids';

import { Task } from '../../../models';

export const newTaskHandler = async (
  req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
) => {
  const { title, desc, tagsToConnect, tagsToCreate, summary, module } =
    req.payload;

  const task = await Task.create({
    data: {
      title,
      desc,
      summary,
      module: {
        connect: {
          id: parseInt(module),
        },
      },
      tags: {
        ...(tagsToConnect.length ? { connect: tagsToConnect } : {}),
        ...(tagsToConnect.length ? { connect: tagsToConnect } : {}),
        ...(tagsToCreate.length
          ? {
              create: tagsToCreate.map((tag) => ({
                name: tag.name,
              })),
            }
          : {}),
      },
    },
  });

  const record = new BaseRecord(task, context.resource).toJSON(
    context.currentAdmin
  );

  return {
    record,
    redirectUrl: context.h.showUrl(resourceIds.TASKS, record.params.id),
  };
};
