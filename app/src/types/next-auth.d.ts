import NextAuth, { DefaultSession } from "next-auth";
import { User, UserRoles } from "@prisma/client";

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: DefaultSession["user"];
    auth: User;

    // TODO: Add this bits of data to the session
    role: UserRoles;
    roleId: Int; // TalentId OR CompanyID AND default to userId for ADMIN

    // OR

    // Can easily query objects with talentId OR companyID
    // -> WHERE: { talent: { userId: session.auth.id } }
    // No need to store extra data thats not needed
  }
}
