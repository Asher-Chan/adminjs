import { ActionRequest } from 'adminjs';

import { TaskFormValues } from '../../../types/forms';
import { uploadFile } from '../../../utils/cloudinary';

export const handleImages = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { uploadedOverviewImage, ...task } =
      req.payload as TaskFormValues;

    if (uploadedOverviewImage?.path) {
      task.overviewImgURL = await uploadFile({
        filePath: uploadedOverviewImage.path,
      });
    }


    return {
      ...req,
      payload: task,
    };
  }

  return req;
};
