import React from "react";
import Image from "next/image";
import Link from "next/link";

import styles from "./index.module.scss";

type Props = {
  moduleId: number;
  companyLogoURL: string;
  openingImgURL: string;
  moduleTitle: string;
  numOfTasks: number;
  numOfTalentsEnrolled: number;
};

const ModuleHeroBanner: React.FC<Props> = ({
  moduleId,
  companyLogoURL,
  openingImgURL,
  moduleTitle,
  numOfTasks,
  numOfTalentsEnrolled,
}) => {
  return (
    <div className={styles.container}>
      {/* Module Details */}
      <div className={styles.modulesDetailsContainer}>
        <Image
          src={companyLogoURL}
          width="180px"
          height="70px"
          alt="Company Logo"
        />
        <h1>{moduleTitle}</h1>
        <p>Welcome to this virtual experience module, Randy!</p>
        <p>
          Here&apos;s a breakdown of what to expect. There are {numOfTasks}{" "}
          tasks. Complete the tasks sequentially to receive your certificate.
          All the best!
        </p>
        <Link href={`/modules/${moduleId}`}>
          <a className={styles.moduleLink}>View Module Description</a>
        </Link>
      </div>

      {/* Module Opening Image */}
      <div>
        <Image
          src={openingImgURL}
          width="300px"
          height="300px"
          alt="module opening image"
        />
      </div>
    </div>
  );
};

export default ModuleHeroBanner;
