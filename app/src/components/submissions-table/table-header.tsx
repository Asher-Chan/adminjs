import React from "react";
import { HeaderGroup } from "react-table";
import { TableHead, TableRow, TableCell } from "@mui/material";

import styles from "./index.module.scss";

type Props = {
  headerGroups: HeaderGroup[];
};

const TableHeader: React.FC<Props> = ({ headerGroups }) => (
  <TableHead>
    {headerGroups.map((headerGroup) => {
      const { key, ...headerGroupProps } = headerGroup.getHeaderGroupProps();

      return (
        <TableRow key={key} {...headerGroupProps}>
          {headerGroup.headers.map((column) => {
            const { key, ...headerProps } = column.getHeaderProps();

            return (
              <TableCell key={key} {...headerProps}>
                {column.render("Header")}
              </TableCell>
            );
          })}
        </TableRow>
      );
    })}
  </TableHead>
);

export default TableHeader;
