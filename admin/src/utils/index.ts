import { DMMFClass } from '@prisma/client/runtime';

/**
 * Get Prisma dmmf models (Data Model Meta Format)
 */
export const getModels = (prisma) => {
  // eslint-disable-next-line no-underscore-dangle
  return (prisma as any)._dmmf as DMMFClass;
};
