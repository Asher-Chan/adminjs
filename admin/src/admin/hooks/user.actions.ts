import { ActionContext, ActionRequest, ActionResponse } from 'adminjs';
import bcrypt from 'bcrypt';

import {
  CompanyOrTalentFormValues,
  SortedAttachedUserPayload,
} from '../../types/forms';
import { Address } from '../../models';

/**
 * Before hook to sort payload
 */
export const sortData = async (req: ActionRequest, context: ActionContext) => {
  if (req.method === 'post') {
    const {
      // User
      email,
      phone,
      password,
      profileImgURL,
      displayName,
      fullName,
      isActive,
      linkedinUsername,

      // Address
      firstLine,
      secondLine,
      state,
      country,
      postCode,
      city,

      uploadedLogoFile,
      uploadedProfilePhoto,
      uploadedSignatureFile,
      ...others
    } = req.payload as CompanyOrTalentFormValues;

    const sortedPayload = {
      attachedUser: {
        email,
        phone,
        displayName: displayName || fullName,
        fullName,
        isActive: !!isActive,
        linkedinUsername,
        profileImgURL,
        ...(password
          ? {
              password: await bcrypt.hash(
                password,
                Number(process.env.HASH_ROUNDS)
              ),
            }
          : {}),
      },
      address: {
        firstLine,
        secondLine,
        state,
        country,
        city,
        postCode,
      },
      ...others,
    } as SortedAttachedUserPayload;

    if (uploadedLogoFile?.path) {
      context.uploadedLogoFile = uploadedLogoFile.path;
    }

    if (uploadedProfilePhoto?.path) {
      context.uploadedProfilePhoto = uploadedProfilePhoto.path;
    }

    if (uploadedSignatureFile?.path) {
      context.uploadedSignatureFile = uploadedSignatureFile.path;
    }

    return {
      ...req,
      payload: {
        ...sortedPayload,
      },
    };
  }
  return req;
};

export const populateUserData = async (
  originalResponse: ActionResponse
): Promise<ActionResponse> => {
  const { record, ...response } = originalResponse;
  const { user } = record.populated;
  const { id: userId, password, ...userData } = user?.params || {};

  const userAddress = await Address.findUnique({
    where: {
      userId,
      id: userData?.addressId,
    },
  });

  const { id: addressId, ...addressData } = userAddress;

  return {
    ...response,
    record: {
      ...record,
      params: {
        ...record.params,
        userId,
        ...userData,
        ...(addressData || {}),
      },
    },
  };
};
