import { ModuleWithTalentProgress } from "@/types/modules";
import { NextAPIReq, NextAPIRes } from "@/types/index";
import { prisma } from "@/utils/db";

export const getAllModuleProgressOfTalent: (
  userId: number,
  filterOptions?: {
    moduleTitle?: string;
    taskTags?: string[];
    pageSize: number;
    cursor: number;
  }
) => Promise<ModuleWithTalentProgress[]> = async (userId, filterOptions) => {
  const talent = await prisma.talent.findUnique({
    where: {
      userId,
    },
    include: {
      moduleProgress: {
        ...(!filterOptions?.cursor
          ? {}
          : {
              cursor: { id: filterOptions.cursor },
              // Get one extra to get next cursor
              take: (filterOptions?.pageSize || 0) + 1,
            }),
        include: {
          module: {
            ...(filterOptions?.taskTags?.length || filterOptions?.moduleTitle
              ? {
                  where: {
                    // Tags filter
                    ...(filterOptions?.taskTags?.length
                      ? {
                          tasks: {
                            some: {
                              tags: {
                                some: {
                                  name: {
                                    in: filterOptions.taskTags,
                                  },
                                },
                              },
                            },
                          },
                        }
                      : {}),
                    // Search filter - Module title
                    ...(filterOptions?.moduleTitle
                      ? {
                          title: {
                            contains: filterOptions.moduleTitle,
                          },
                        }
                      : {}),
                  },
                }
              : {}),
            include: {
              company: {
                include: {
                  user: true,
                },
              },
            },
          },
        },
      },
    },
  });

  if (!talent) {
    throw new Error("Talent not found");
  }

  const talentsModules: ModuleWithTalentProgress[] = talent.moduleProgress.map(
    (mp) => ({
      ...mp.module,
      moduleProgressId: mp.id,
      status: mp.status,
    })
  );

  return talentsModules;
};

export default async function handler(
  req: NextAPIReq<{ userId: string }>,
  res: NextAPIRes<{ modules: ModuleWithTalentProgress[] }>
) {
  const userId = parseInt(req.query.userId) || undefined;

  if (!userId) {
    res.status(400).json({ error: true, errorMessage: "Invalid user id" });
    return;
  }

  try {
    const data = await getAllModuleProgressOfTalent(userId);
    res.status(200).json({
      error: false,
      modules: data,
    });
  } catch (err) {
    const errMessage = !(err instanceof Error)
      ? "Error fetching modules"
      : err.message;
    res.status(400).json({ error: true, errorMessage: errMessage });
  }
}
