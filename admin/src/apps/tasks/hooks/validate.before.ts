import { ActionRequest, ValidationError } from 'adminjs';

import { TaskFormValues } from '../../../types/forms';

export const validateData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { desc } = req.payload as TaskFormValues;

    const errors: { [key: string]: { message: string } } = {};

    if (!desc) {
      errors.desc = {
        message: 'Task desciption is required',
      };
    }

    const numOfErrors = Object.keys(errors).length;

    if (numOfErrors) {
      throw new ValidationError(
        {
          ...errors,
        },
        {
          message: 'Some field are missing or invalid.',
        }
      );
    }
  }

  return req;
};
