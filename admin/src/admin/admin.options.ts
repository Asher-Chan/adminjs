import { AdminJSOptions } from 'adminjs';

import { resourceIds } from '../constants/resource-ids';
import { getModels } from '../utils';
import companyResource from '../apps/companies/company.resource';
import certificateResource from '../apps/certificates/certificate.resource';
import moduleProgressResource from '../apps/module-progress/module-progress.resource';
import moduleResource from '../apps/modules/module.resource';
import stepResource from '../apps/steps/step.resource';
import taskResource from '../apps/tasks/task.resource';
import talentResource from '../apps/talents/talent.resource';
import userResource from '../apps/users/user.resource';
import prisma from '../prisma-client';

import { locale } from './admin.locale';
import { branding } from './admin.branding';
import { dashboardHandler } from './hooks/dashboard.handler';
import { CertGeneratorPage, Dashboard } from './components.bundler';
import { navigation } from './admin.navigation';
import { certGeneratorPageHandler } from './hooks/cert-generator-page.handler';

export default {
  rootPath: '/admin',
  loginPath: '/admin/login',
  logoutPath: '/admin/logout',
  locale,
  branding,
  dashboard: {
    handler: dashboardHandler,
    component: Dashboard,
  },
  pages: {
    'Cert Generator': {
      // Instead of certGenerator
      handler: certGeneratorPageHandler,
      icon: 'Certificate',
      component: CertGeneratorPage,
    },
  },
  resources: [
    companyResource,
    moduleResource,
    taskResource,
    stepResource,
    talentResource,
    moduleProgressResource,
    userResource,
    certificateResource,
    // Tags & Submission resource put here directly, not enough configs to have to move them into separate folders
    {
      resource: { model: getModels(prisma).modelMap.Tag, client: prisma },
      options: {
        id: resourceIds.TAGS,
        navigation: navigation.TAGS,
      },
    },
    {
      resource: {
        model: getModels(prisma).modelMap.Submission,
        client: prisma,
      },
      options: {
        id: resourceIds.SUBMISSIONS,
        navigation: navigation.SUBMISSIONS,
        properties: {
          id: {
            isVisible: false,
          },
          talent: {
            type: 'reference',
            reference: resourceIds.TALENTS,
          },
          task: {
            type: 'reference',
            reference: resourceIds.TASKS,
          },
        },
      },
    },
  ],
  assets: {
    // Custom CSS overrides, hacky but only way for some components
    styles: ['/css/styles.css'],
  },
} as AdminJSOptions;
