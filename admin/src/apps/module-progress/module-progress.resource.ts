import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import {
  CertificateStatus,
  DurationProperty,
  ViewSubmissions,
} from '../../admin/components.bundler';
import prisma from '../../prisma-client';

import { populateSubmissionsData } from './hooks/submissions.action';

const options: ResourceOptions = {
  id: resourceIds.MODULE_PROGRESS,
  navigation: navigation.MODULE_PROGRESS,
  listProperties: [
    'talent',
    'module',
    'createdAt',
    'duration',
    'certificateStatus',
  ],
  filterProperties: [],
  actions: {
    new: {
      isVisible: false,
      isAccessible: false,
    },
    edit: {
      isVisible: false,
      isAccessible: false,
    },
    delete: {
      isVisible: false,
      isAccessible: false,
    },
    show: {
      component: ViewSubmissions,
      after: populateSubmissionsData,
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    module: {
      type: 'reference',
      reference: resourceIds.MODULES,
    },
    talent: {
      type: 'reference',
      reference: resourceIds.TALENTS,
    },
    duration: {
      type: 'string',
      components: {
        list: DurationProperty,
      },
    },
    certificateStatus: {
      components: {
        list: CertificateStatus,
        show: CertificateStatus,
      },
    },
  },
};

export default {
  resource: {
    model: getModels(prisma).modelMap.ModuleProgress,
  },
  options,
};
