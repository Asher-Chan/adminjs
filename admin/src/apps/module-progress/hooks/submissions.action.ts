import {
  ActionResponse,
  ActionContext,
  BaseRecord,
  ActionRequest,
} from 'adminjs';

import { Submission, Module } from '../../../models';
import { resourceIds } from '../../../constants/resource-ids';

export const populateSubmissionsData = async (
  originalResponse: ActionResponse,
  req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const module = await Module.findUnique({
    where: {
      id: record.params.module,
    },
    select: {
      // All tasks in this module
      tasks: true,
    },
  });

  const submissions = await Submission.findMany({
    where: {
      talentId: record.params.talent,
      moduleProgressId: record.params.id,
    },
  });

  const moduleTasks = module.tasks;

  // eslint-disable-next-line no-underscore-dangle
  const taskResource = context._admin.findResource(resourceIds.TASKS);

  // eslint-disable-next-line no-underscore-dangle
  const submissionResource = context._admin.findResource(
    resourceIds.SUBMISSIONS
  );

  const taskRecords = moduleTasks?.map((task) =>
    new BaseRecord(task, taskResource).toJSON(context.currentAdmin)
  );

  const submissionRecords = submissions?.map((submission) =>
    new BaseRecord(submission, submissionResource).toJSON(context.currentAdmin)
  );

  return {
    ...originalResponse,
    record: {
      ...record,
      populated: {
        ...record.populated,
        tasks: taskRecords,
        submissions: submissionRecords,
      },
    },
  };
};
