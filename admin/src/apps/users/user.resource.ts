import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import { ImageUpload, ImageView } from '../../admin/components.bundler';
import prisma from '../../prisma-client';

import { showAdminsOnly } from './hooks/list.before';
import { handleBeforeSave } from './hooks/new.before';
import { handleImageUpload } from './hooks/handle-image.before';
import { handlePasswordBeforeSave } from './hooks/edit.before';
import { removePasswordFromPayload } from './hooks/edit.after';

const options: ResourceOptions = {
  id: resourceIds.USERS,
  navigation: navigation.USERS,
  listProperties: [
    'fullName',
    'displayName',
    'email',
    'isActive',
    'lastLoginAt',
  ],
  filterProperties: [
    'fullName',
    'email',
    'lastLoginAt',
    'isActive',
    'displayName',
  ],
  actions: {
    new: {
      before: [handleBeforeSave, handleImageUpload],
    },
    edit: {
      before: [handleImageUpload, handlePasswordBeforeSave],
      after: removePasswordFromPayload,
    },
    list: {
      before: showAdminsOnly,
    },
  },
  properties: {
    id: {
      isVisible: { list: false, edit: false },
    },
    email: {
      isRequired: true,
    },
    fullName: {
      isTitle: true,
      isVisible: { list: false, edit: true, show: true },
    },
    linkedinUsername: {
      isVisible: false,
    },
    uploadedProfileImg: {
      custom: {
        fieldName: 'profileImgURL',
      },
      components: {
        edit: ImageUpload,
      },
      isVisible: {
        edit: true,
      },
    },
    profileImgURL: {
      isVisible: {
        show: true,
      },
      components: {
        show: ImageView,
      },
    },
    password: {
      type: 'password',
      isRequired: true,
      isVisible: {
        edit: true,
      },
    },
    role: {
      isVisible: false,
    },
    isVerified: {
      isVisible: false,
    },
    talent: {
      isVisible: false,
    },
    company: {
      isVisible: false,
    },
    lastLoginAt: {
      isVisible: false,
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.User },
  options,
};
