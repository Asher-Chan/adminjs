import {
  ActionContext,
  ActionRequest,
  ActionResponse,
  BaseRecord,
} from 'adminjs';
import { resourceIds } from '../../../constants/resource-ids';

import { Step } from '../../../models';
import { uploadFile } from '../../../utils/cloudinary';

export const handleResources = async (
  req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
) => {
  if (req.method === 'post') {
    const resources = [];
    const files = [];
    const fileURLs = [];
    const { resourceCount } = req.payload;

    Object.entries(req.payload).forEach(([key, value]) => {
      if (key.includes('resourceFile')) {
        if (value?.path) {
          files.push({
            path: value.path,
            file: value,
          });
        }
      }
    });

    if (files.length) {
      for (let i = 0; i < files.length; i += 1) {
        fileURLs.push({
          path: files[i].path,
          // eslint-disable-next-line no-await-in-loop
          url: await uploadFile({
            filePath: files[i].path,
            options: { resource_type: 'raw' },
          }),
        });
      }
    }

    Array.from(Array(Number(resourceCount || 0)).keys()).forEach((_r, idx) => {
      const isNew = req.payload[`resources.${idx}.__isNew__`] === 'true';
      const isDeleted =
        req.payload[`resources.${idx}.__isDeleted__`] === 'true';
      const id = req.payload[`resources.${idx}.id`];
      const title = req.payload[`resources.${idx}.title`];
      const desc = req.payload[`resources.${idx}.desc`];
      const type = req.payload[`resources.${idx}.type`];
      let resourceURL = req.payload[`resources.${idx}.resourceURL`] || '';
      let resourceFile = req.payload[`resources.${idx}.resourceFile`] || '';

      if (!isDeleted) {
        resourceFile = fileURLs.find(({ path }) => path === resourceFile.path);

        if (resourceFile) {
          resourceURL = resourceFile.url;
        }
      }

      const resource = {
        id: isNew ? '' : id,
        title: title || '',
        desc: desc || '',
        type: type || '',
        resourceURL,
        isNew,
        isDeleted,
      };

      resources.push(resource);
    });

    const { recordId } = req.params;
    // Fix this using task || taskId, just use one
    const { title, desc, mediaURLs, task, taskId } = req.payload;

    const step = await Step.upsert({
      where: {
        id: parseInt(recordId) || 0,
      },
      create: {
        title,
        desc,
        task: {
          connect: {
            id: parseInt(task || taskId),
          },
        },
        mediaURLs: JSON.stringify(mediaURLs),
        stepResources: {
          createMany: {
            data: resources.map(
              ({ id, isNew, isDeleted, ...resource }) => resource
            ),
          },
        },
      },
      update: {
        title,
        desc,
        mediaURLs: JSON.stringify(mediaURLs),
        stepResources: {
          ...(resources.length && { deleteMany: {} }),
          ...(resources.length && { set: [] }),
          createMany: {
            data: resources?.flatMap(({ id, isNew, isDeleted, ...resource }) =>
              !isDeleted ? resource : []
            ),
          },
        },
      },
    });

    const record = new BaseRecord(step, context.resource).toJSON(
      context.currentAdmin
    );

    return {
      record,
      redirectUrl: context.h.showUrl(resourceIds.STEPS, record.id),
    };
  }

  const step = await Step.findUnique({
    where: {
      id: parseInt(req.params.recordId),
    },
    include: {
      stepResources: true,
      task: true,
    },
  });

  const record = new BaseRecord(step, context.resource).toJSON(
    context.currentAdmin
  );

  return {
    record: {
      ...record,
      populated: {
        ...record.populated,
        stepResources: step.stepResources || [],
      },
    },
    redirectUrl: context.h.showUrl(resourceIds.STEPS, record.id),
  };
};
