import React from "react";
import { Box, Typography, styled } from "@mui/material";

import Text from "@/components/text";

type Props = {
  moduleTitle: string;
  completedDate?: Date;
  currentTaskNumber?: number;
};

const DotSuccess = styled("span")(
  ({ theme }) => `
    border-radius: 22px;
    background: ${theme.colors.success.main};
    width: ${theme.spacing(1.5)};
    height: ${theme.spacing(1.5)};
    display: inline-block;
    margin-right: ${theme.spacing(0.5)};
`
);

const DotWarning = styled("span")(
  ({ theme }) => `
    border-radius: 22px;
    background: ${theme.colors.warning.main};
    width: ${theme.spacing(1.5)};
    height: ${theme.spacing(1.5)};
    display: inline-block;
    margin-right: ${theme.spacing(0.5)};
`
);

const ModuleCell: React.FC<Props> = ({
  moduleTitle,
  completedDate,
  currentTaskNumber,
}) => {
  const isModuleCompleted = !!completedDate;

  return (
    // <div className={styles.moduleCell}>
    //   <div className={styles.moduleTitle}>{moduleTitle}</div>
    //   <div className={styles.completionStatus}>
    //     <div
    //       className={classnames(
    //         styles.status,
    //         isModuleCompleted ? styles.__complete : styles.__inProgress
    //       )}
    //     />
    //     {isModuleCompleted
    //       ? `Completed on ${completedDate.toDateString()}`
    //       : `In progress | Task ${currentTaskNumber || "-"}`}
    //   </div>
    // </div>
    <Box display="flex" alignItems="center">
      <Box
        sx={{
          ml: 1,
        }}
      >
        <Typography variant="h5" noWrap>
          {/* Fix a width on this to have elipsis for module title */}
          {moduleTitle}
        </Typography>
        <Typography
          variant="body2"
          noWrap
          sx={{
            display: "flex",
            alignItems: "center",
            mr: 2,
          }}
        >
          {isModuleCompleted ? <DotSuccess /> : <DotWarning />}
          <Text color="warning">
            {isModuleCompleted
              ? `Completed on ${completedDate.toDateString()}`
              : `In progress | Task ${currentTaskNumber || "-"}`}
          </Text>
        </Typography>
      </Box>
    </Box>
  );
};

export default ModuleCell;
