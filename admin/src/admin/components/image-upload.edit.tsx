import React from 'react';
import {
  Box,
  Label,
  DropZone,
  DropZoneProps,
  DropZoneItem,
} from '@adminjs/design-system';
import { EditPropertyProps } from 'adminjs';
import styled from 'styled-components';

const ErrorText = styled(Box)`
  color: red;
  font-size: 0.75rem;
`;

const ImageUpload: React.FC<EditPropertyProps> = ({
  property,
  onChange,
  record,
}) => {
  const uploadedFile = record.params[property.custom.fieldName]; // Change name
  const fileToUpload = record.params[property.path];

  const { errors } = record;

  const handleDropZoneChange: DropZoneProps['onChange'] = (files) => {
    onChange(property.path, property.props.multipleFiles ? files : files[0]);
  };

  const handleOnRemove = () => {
    onChange(property.custom.fieldName, '');
  };

  return (
    <Box mb={24}>
      <Label>{property.label}</Label>
      <DropZone
        validate={{
          mimeTypes: [
            'image/gif',
            'image/jpeg',
            'video/mp4',
            'image/png',
            'image/svg+xml',
            'video/webm',
            'image/webp',
          ],
        }}
        onChange={handleDropZoneChange}
        multiple={!!property.props.multipleFiles}
      />
      {uploadedFile && !fileToUpload && (
        <DropZoneItem onRemove={handleOnRemove} src={uploadedFile} />
      )}
      {errors[property.propertyPath] && (
        <ErrorText mt={12}>{errors[property.propertyPath].message}</ErrorText>
      )}
    </Box>
  );
};

export default ImageUpload;
