import { ActionRequest } from 'adminjs';
import ejs from 'ejs';
import path from 'path';

import { Module, Talent, Certificate } from '../../models';
import { convertHTMLtoPDF } from '../../utils/certificate-generator';
import { uploadCertificatePDF } from '../../utils/cloudinary';

export const certGeneratorPageHandler = async (req: ActionRequest) => {
  if (req.method === 'get') {
    const modules = await Module.findMany({
      select: {
        id: true,
        title: true,
      },
    });

    const talents = await Talent.findMany({});

    return {
      modules,
      talents,
    };
  }

  if (req.method === 'post') {
    const { moduleId, talentId } = req.payload;

    if (!moduleId || !talentId) {
      return {
        certPDFLink: '',
      };
    }

    const cert = await Certificate.create({
      data: {
        module: {
          connect: {
            id: moduleId,
          },
        },
        talent: {
          connect: {
            id: talentId,
          },
        },
      },
      include: {
        module: {
          include: {
            company: {
              include: {
                user: true,
              },
            },
          },
        },
        talent: {
          include: {
            user: true,
          },
        },
      },
    });

    const certTemplateData = {
      companyLogoURL: cert.module?.company?.logoURL || '',
      talentsName: cert.talent.user.fullName || '',
      certId: cert.certId || '',
      moduleTitle: cert.module.title || '',
      upsurgeSignatureFileURL:
        'https://res.cloudinary.com/baked-potatoes/image/upload/v1651736360/upsurge-signature.png',
      companySignatureFileURL: cert.module.company.signatureFileURL || '',
      companySignatureName: cert.module.company.signatureName || '',
      companySignatureTitle: cert.module.company.signatureTitle || '',
    };

    try {
      // Generate html -> ejs
      const certHTML = await ejs.renderFile(
        path.join(__dirname, '../../templates/certificate.ejs'),
        certTemplateData
      );

      // Convert to pdf
      const certPDFBuffer = await convertHTMLtoPDF(certHTML);

      const certPDFLink = await uploadCertificatePDF(certPDFBuffer);

      await Certificate.update({
        where: {
          id: cert.id,
        },
        data: {
          fileURL: certPDFLink,
        },
      });

      return {
        certPDFLink,
      };
    } catch (err) {
      console.log(err);
      return {};
    }
  }

  return {};
};
