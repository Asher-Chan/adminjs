-- CreateTable
CREATE TABLE `RegistrationForm` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `moduleId` INTEGER NOT NULL,
    `talentId` INTEGER NOT NULL,
    `reasonInterested` VARCHAR(191) NOT NULL,
    `companyKnowledgeLevel` INTEGER NOT NULL,
    `joinCompanyPotentialLevel` INTEGER NOT NULL,
    `likelinessToApplyLevel` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `RegistrationForm` ADD CONSTRAINT `RegistrationForm_talentId_fkey` FOREIGN KEY (`talentId`) REFERENCES `Talent`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `RegistrationForm` ADD CONSTRAINT `RegistrationForm_moduleId_fkey` FOREIGN KEY (`moduleId`) REFERENCES `Module`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
