import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, H4 } from '@adminjs/design-system';
import styled from 'styled-components';

const Description = styled(Box)`
  font-size: 0.8rem;
  color: grey;
`;

const ResourcesShow: React.FC<ShowPropertyProps> = ({ record }) => {
  const stepResources = record.populated.stepResources as unknown as any[];

  if (!stepResources?.length) {
    return null;
  }

  return (
    <Box my={36}>
      <Box flex flexDirection="row">
        <Box flexGrow={1}>
          <H4>Resources</H4>
        </Box>
      </Box>

      {stepResources.map((item, idx) => {
        return (
          <Box variant="card" key={item.id} mb={18}>
            <Box mb={12}>
              {idx + 1}. {item.title}
            </Box>
            <Description mb={12}>{item.desc}</Description>
            <a href={item.resourceURL || ''} target="_blank" rel="noreferrer">
              View resource
            </a>
          </Box>
        );
      })}
    </Box>
  );
};

export default ResourcesShow;
