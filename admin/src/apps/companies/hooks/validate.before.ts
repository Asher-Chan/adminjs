
import { ActionRequest, ValidationError } from 'adminjs';

import { User } from '../../../models';
import { SortedCompanyPayload } from '../../../types/forms';

export const validateData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { attachedUser, address, ...company } =
      req.payload as SortedCompanyPayload;

    const errors: { [key: string]: { message: string } } = {};

    if (!company.briefIntro) {
      errors.briefIntro = {
        message: 'Brief intro is required',
      };
    }

    const emailTaken = await User.findUnique({
      where: {
        email: attachedUser.email,
      },
    });

    if (!!emailTaken && emailTaken.id.toString() !== company.userId) {
      errors.email = {
        message: 'Email must be unique',
      };
    }

    const numOfErrors = Object.keys(errors).length;

    if (numOfErrors) {
      throw new ValidationError(
        {
          ...errors,
        },
        {
          message: 'Some field are missing or invalid.',
        }
      );
    }
  }

  return req;
};
