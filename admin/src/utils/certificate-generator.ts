import pdf from 'html-pdf';

export const convertHTMLtoPDF = (html: string): Promise<Buffer> =>
  new Promise((resolve, reject) => {
    pdf
      .create(html, {
        width: '820px',
        height: '540px',
        type: 'pdf',
        orientation: 'landscape',
      })
      .toBuffer((err, buffer) => {
        if (err) {
          return reject(err);
        }
        return resolve(buffer);
      });
  });
