import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";
import * as bcrypt from "bcrypt";

import { prisma } from "@/utils/db";
import { UserRoles } from "@prisma/client";

export default NextAuth({
  callbacks: {
    jwt: ({ token, user }) => {
      if (user) {
        token.id = user.id;
        // token.user = user; -> Data from first login
      }

      return token;
    },
    session: async ({ session, token }) => {
      if (token && token.email) {
        session.id = token.id;

        // This will fetch latest data on each check
        const user = await prisma.user.findUnique({
          where: {
            email: token.email,
          },
        });

        if (user) {
          session.auth = user;
        }
      }

      return session;
    },
  },
  pages: {
    signIn: "/auth/sign-in",
    error: "/auth/sign-in",
  },
  session: {
    strategy: "jwt",
  },
  secret: "test",
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID!,
      clientSecret: process.env.GOOGLE_SECRET!,
      checks: "both",
      profile: async (profile) => {
        const user = await prisma.user.findUnique({
          where: {
            email: profile.email,
          },
        });

        if (!user) {
          const newUser = await prisma.user.create({
            data: {
              email: profile.email,
              isActive: true,
              isGoogleLogin: true,
              isVerified: profile.email_verified,
              profileImgURL: profile.picture || "",
              role: UserRoles.TALENT,
              fullName: profile.name || "",
              talent: {
                create: {},
              },
            },
          });

          return {
            ...newUser,
            id: newUser.id.toString(), // Must be a string
          };
        }

        if (!user.isGoogleLogin) {
          const updatedUser = await prisma.user.update({
            where: {
              id: user.id,
            },
            data: {
              isGoogleLogin: true,
            },
          });

          return {
            ...updatedUser,
            id: user.id.toString(), // Must be a string
          };
        }

        return {
          ...user,
          id: user.id.toString(), // Must be a string
        };
      },
      authorization: {
        params: {
          prompt: "consent",
          access_type: "offline",
          response_type: "code",
        },
      },
    }),
    // Can probably duplicate this for company 
    CredentialsProvider({
      id: "custom-credentials",
      name: "Credentials",
      credentials: {
        email: { label: "Email", type: "text", placeholder: "jsmith" },
        password: { label: "Password", type: "password" },
        requiredUserRole: { type: "hidden" },
      },
      authorize: async (credentials, req) => {
        const user = await prisma.user.findUnique({
          where: {
            email: credentials?.email,
          },
        });

        const isCorrectUserRole = user?.role === credentials?.requiredUserRole;

        if (
          !user ||
          !credentials?.password ||
          !user.password ||
          !isCorrectUserRole
        ) {
          throw new Error("Invalid user");
        }

        // Implement password checking
        const isValidPassword = await bcrypt.compare(
          credentials?.password,
          user.password
        );

        if (!isValidPassword) {
          throw new Error("User or password is incorrect");
        }

        return user;
      },
    }),
  ],
});
