import {
  Company,
  User,
  Address,
  Talent,
  Module,
  Step,
  Task,
} from '@prisma/client';
import { UploadedFile } from 'adminjs';

export type AttachedUser = Pick<
  User,
  | 'displayName'
  | 'email'
  | 'isActive'
  | 'linkedinUsername'
  | 'fullName'
  | 'phone'
  | 'profileImgURL'
  | 'password'
>;

export type AttachedUserAddress = Pick<
  Address,
  'firstLine' | 'secondLine' | 'city' | 'state' | 'country' | 'postCode'
>;

export type CompanyOrTalentFormValues = AttachedUser &
  AttachedUserAddress & {
    uploadedLogoFile?: UploadedFile;
    uploadedProfilePhoto?: UploadedFile;
    uploadedSignatureFile?: UploadedFile;
  };

export type SortedAttachedUserPayload = {
  attachedUser: AttachedUser;
  address: AttachedUserAddress;
  userId: string;
};

export type SortedCompanyPayload = Company & SortedAttachedUserPayload;

export type SortedTalentPayload = Talent & SortedAttachedUserPayload;

export type ModuleFormValues = Module & {
  uploadedOpeningImage?: UploadedFile;
  uploadedIntroVideo?: UploadedFile;
  company?: string;
};

// Please use same naming, payload || formValues

export type TaskFormValues = Task & {
  uploadedOverviewImage?: UploadedFile;
};

export type StepFormValues = Step & {
  uploadedMediaFiles?: UploadedFile[];
};
