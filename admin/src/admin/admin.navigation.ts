import { ResourceOptions } from 'adminjs';
import { ResourceNames } from '../types';

// eslint-disable-next-line no-unused-vars
const navigation: { [key in ResourceNames]: ResourceOptions['navigation'] } = {
  COMPANIES: { icon: 'Portfolio' },
  CERTIFICATES: { icon: 'Certificate' },
  MODULES: { icon: 'Document' },
  MODULE_PROGRESS: { icon: 'Report' },
  SUBMISSIONS: false,
  STEPS: false,
  TALENTS: { icon: 'User' },
  TASKS: false,
  USERS: { icon: 'User' },
  TAGS: false,
};

export { navigation };
