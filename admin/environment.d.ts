/* eslint-disable no-unused-vars */
declare global {
  namespace NodeJS {
    interface Global {}
    interface ProcessEnv {
      PORT: string;

      DB_HOST: string;
      DB_PORT: string;
      DB_NAME: string;
      DB_USER: string;
      DB_PASSWORD: string;

      COOKIE_PASSWORD: string;
      HASH_ROUNDS: string;

      CLOUDINARY_CNAME: string;
      CLOUDINARY_API_KEY: string;
      CLOUDINARY_API_SECRET: string;

      CLOUDINARY_URL: string;
    }
  }
}

export {};
