import React from "react";
import classnames from "classnames";

import styles from "./index.module.scss";

type Props = {
  status: "success" | "error" | "warning";
  customMessage?: string;
};

const StatusBar: React.FC<Props> = ({ status, customMessage }) => {
  let message = '';
  let icon = null; // TODO
  let color = "";

  switch (status) {
    case "success":
      message = "Success!";
      color = "#59A310";
      break;
    case "error":
      message = "An error occured.";
      color = "#CC2F2C";
      break;
    case "warning":
      message = "You have been warned.";
      color = "#FED330";
      break;
    default:
      break;
  }

  return (
    <div className={classnames(styles.statusBar, styles[`__${status}`])}>{customMessage || message}</div>
  );
};

export default StatusBar;
