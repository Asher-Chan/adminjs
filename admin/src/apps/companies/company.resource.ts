import { ResourceOptions } from 'adminjs';

import { handleNewCompany } from './hooks/new.handler';
import { handleFetchCompanies } from './hooks/list.handler';
import { fetchPopulatedCompanies } from './hooks/list.after';
import { handleBeforeSave } from './hooks/edit.before';
import { populateModulesData } from './hooks/show.after';
import { redirectAfterSave } from './hooks/edit.after';
import { validateData } from './hooks/validate.before';

import {
  BrandColorDisplay,
  BrandColorInput,
  ImageUpload,
  ImageView,
  ModulesList,
} from '../../admin/components.bundler';
import { populateUserData, sortData } from '../../admin/hooks/user.actions';
import { userProperties } from '../shared-resources/user.resource';
import { resourceIds } from '../../constants/resource-ids';
import { getModels } from '../../utils';
import { navigation } from '../../admin/admin.navigation';
import prisma from '../../prisma-client';

const options: ResourceOptions = {
  id: resourceIds.COMPANIES,
  navigation: navigation.COMPANIES,
  listProperties: [
    'fullName',
    'email',
    'registrationNumber',
    'isActive',
    'createdAt',
  ],
  filterProperties: [
    'email',
    'displayName',
    'fullName',
    'registrationNumber',
    'isActive',
    'createdAt',
  ],
  actions: {
    new: {
      before: [sortData, validateData],
      handler: handleNewCompany,
    },
    edit: {
      before: [sortData, validateData, handleBeforeSave],
      after: [populateUserData, redirectAfterSave],
    },
    show: {
      icon: 'Portfolio',
      after: [populateUserData, populateModulesData],
    },
    list: {
      handler: handleFetchCompanies,
      after: fetchPopulatedCompanies,
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    // User Model Fields
    ...userProperties,
    // Company Model Fields
    briefIntro: {
      type: 'textarea',
      props: {
        isRequired: true,
        rows: 10,
      },
      isRequired: true,
    },
    registrationNumber: {
      isRequired: true,
    },
    yearFounded: {
      isRequired: true,
    },
    brandColor: {
      isRequired: true,
      components: {
        edit: BrandColorInput,
        show: BrandColorDisplay,
      },
    },
    uploadedLogoFile: {
      custom: {
        fieldName: 'logoURL', // Change key to better name
      },
      components: {
        edit: ImageUpload,
      },
      isVisible: {
        edit: true,
      },
    },
    logoURL: {
      isVisible: {
        show: true,
      },
      components: {
        show: ImageView,
      },
    },
    isDeleted: {
      isVisible: false,
    },
    deletedAt: {
      isVisible: false,
    },
    uploadedSignatureFile: {
      position: 121,
      custom: {
        fieldName: 'signatureFileURL',
      },
      components: {
        edit: ImageUpload,
      },
      isVisible: {
        edit: true,
      },
    },
    signatureFileURL: {
      isVisible: {
        show: true,
      },
      components: {
        show: ImageView,
      },
    },
    // Modules
    modules: {
      reference: resourceIds.MODULES,
      position: 120,
      isVisible: {
        show: true,
      },
      props: {
        itemName: 'module',
      },
      components: {
        show: ModulesList,
      },
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.Company },
  options,
};
