import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { populateUserData, sortData } from '../../admin/hooks/user.actions';
import { userProperties } from '../shared-resources/user.resource';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import prisma from '../../prisma-client';

import { handleBeforeSave } from './hooks/edit.before';
import { handleNewTalent } from './hooks/new.handler';
import { handleFetchTalents } from './hooks/list.before';
import { redirectAfterSave } from './hooks/edit.after';
import { validateData } from './hooks/validate.before';
import { fetchPopulatedTalents } from './hooks/list.after';

const options: ResourceOptions = {
  id: resourceIds.TALENTS,
  navigation: navigation.TALENTS,
  listProperties: ['fullName', 'email', 'jobTitle', 'isActive', 'createdAt'],
  filterProperties: [
    'fullName',
    'displayName',
    'jobTitle',
    'email',
    'isActive',
    'createdAt',
  ],
  actions: {
    new: {
      before: [sortData, validateData],
      handler: handleNewTalent,
    },
    edit: {
      before: [sortData, validateData, handleBeforeSave],
      after: [populateUserData, redirectAfterSave],
    },
    show: {
      after: populateUserData,
    },
    list: {
      handler: handleFetchTalents,
      after: fetchPopulatedTalents,
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    // User Model Fields
    ...userProperties,
    // Talent Model Fields
    isDeleted: {
      isVisible: false,
    },
    deletedAt: {
      isVisible: false,
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.Talent },
  options,
};
