import React from "react";
import { Controller, useFormContext } from "react-hook-form";

import styles from "./index.module.scss";

type SharedProps = {
  name: string;
  label: string;
  rows?: number;
  required?: boolean;
  min?: number;
  max?: number;
};

type Props =
  | (SharedProps & {
      type: "text" | "email" | "text-area";
      defaultValue?: string;
    })
  | (SharedProps & {
      type: "number";
      defaultValue?: number;
    });

const TextInput: React.FC<Props> = ({
  name,
  defaultValue,
  label,
  type,
  rows = 5,
  required = false,
  min,
  max,
}) => {
  const { control } = useFormContext();

  return (
    <div className={styles.inputContainer}>
      <label className={styles.label}>{required ? `${label}*` :label}</label>
      <Controller
        rules={{
          required,
          ...(type === 'number' && min ? { min } : {}),
          ...(type === 'number' && max ? { max } : {}),
        }}
        render={({ field }) =>
          type === "text-area" ? (
            <textarea
              rows={rows}
              {...field}
              className={styles.input}
              required={required}
            />
          ) : (
            <input
              {...field}
              className={styles.input}
              type={type}
              required={required}
            />
          )
        }
        control={control}
        name={name}
        defaultValue={defaultValue}
      />
    </div>
  );
};

export default TextInput;
