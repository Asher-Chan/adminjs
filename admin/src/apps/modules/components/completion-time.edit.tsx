import React, { useState } from 'react';
import { Box, Label } from '@adminjs/design-system';
import { EditPropertyProps } from 'adminjs';
import Select, { StylesConfig } from 'react-select';
import styled from 'styled-components';

type Option = {
  value: number;
  label: string;
};

const ErrorText = styled(Box)`
  color: red;
  font-size: 0.75rem;
`;

// Whats the limits??
// Include validations such as, start cannot be higher than end, vice versa
// Depending on validations show what possible options are left
const options: Option[] = Array.from(Array(10).keys()).map((i) => {
  const number = i + 1;
  return {
    value: number,
    label: number.toString(),
  };
});

const selectStlyes: StylesConfig = {
  control: (provided) => ({
    ...provided,
    borderRadius: 0,
    width: '80px',
  }),
  menu: (provided) => ({
    ...provided,
    width: '80px',
  }),
};

const CompletionTimeEdit: React.FC<EditPropertyProps> = ({
  record,
  onChange,
  property,
}) => {
  const { estCompletionTimeStart: start, estCompletionTimeEnd: end } =
    record.params;
  const { errors } = record;

  const startOption = options.find((option) => option.value === start);
  const endOption = options.find((option) => option.value === end);

  const [startTimeOption, setStartTimeOption] = useState(startOption || null);
  const [endTimeOption, setEndTimeOption] = useState(endOption || null);

  const handleStartTimeChange = (selectedOption?: Option) => {
    if (!selectedOption) {
      onChange('estCompletionTimeStart', null);
      setStartTimeOption(null);
      return;
    }
    setStartTimeOption(selectedOption);
    onChange('estCompletionTimeStart', selectedOption.value);
  };

  const handleEndTimeChange = (selectedOption?: Option) => {
    if (!selectedOption) {
      onChange('estCompletionTimeEnd', null);
      setEndTimeOption(null);
      return;
    }
    setEndTimeOption(selectedOption);
    onChange('estCompletionTimeEnd', selectedOption.value);
  };

  return (
    <Box mb={36}>
      <Label required={property.isRequired}>{property.label}</Label>
      {/** Can probably be separated into another component */}
      <div
        style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}
      >
        <Select
          styles={selectStlyes}
          name="estCompletionTimeStart"
          value={startTimeOption}
          onChange={handleStartTimeChange}
          options={options}
          // isClearable
          placeholder=""
        />
        <div style={{ margin: '0 0.8rem' }}>to</div>
        <Select
          styles={selectStlyes}
          name="estCompletionTimeEnd"
          value={endTimeOption}
          onChange={handleEndTimeChange}
          options={options}
          // isClearable
          placeholder=""
        />
        <div style={{ margin: '0 0.8rem' }}>hours</div>
      </div>
      <Box mt={12}>
        {errors.estCompletionTimeStart && (
          <ErrorText>- {errors.estCompletionTimeStart.message}</ErrorText>
        )}
        {errors.estCompletionTimeEnd && (
          <ErrorText>- {errors.estCompletionTimeEnd.message}</ErrorText>
        )}
        {errors.estCompletionTime && (
          <ErrorText>- {errors.estCompletionTime.message}</ErrorText>
        )}
      </Box>
    </Box>
  );
};

export default CompletionTimeEdit;
