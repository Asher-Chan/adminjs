import AdminJS from 'adminjs';

// Overrides
AdminJS.bundle('./components/user-nav', 'LoggedIn');

// Shared components
export const Dashboard = AdminJS.bundle('./components/dashboard');
export const ParentRecord = AdminJS.bundle('./components/parent-record');
export const AddressDisplay = AdminJS.bundle('./components/address.show');
export const DisplayNameInput = AdminJS.bundle(
  './components/display-name.edit'
);
export const ImageUpload = AdminJS.bundle('./components/image-upload.edit');
export const ImageView = AdminJS.bundle('./components/image-view.show');
export const MultiImageView = AdminJS.bundle(
  './components/multi-image-view.show'
);
export const StepsList = AdminJS.bundle('./components/steps.show');
export const TasksList = AdminJS.bundle('./components/tasks-list.show');

// Companies
export const BrandColorInput = AdminJS.bundle(
  '../apps/companies/components/brand-color.edit'
);
export const BrandColorDisplay = AdminJS.bundle(
  '../apps/companies/components/brand-color.show'
);
export const ModulesList = AdminJS.bundle(
  '../apps/companies/components/modules.show'
);

// Modules
export const CompletionTimeInput = AdminJS.bundle(
  '../apps/modules/components/completion-time.edit'
);

// Module Progress
export const CertificateStatus = AdminJS.bundle(
  '../apps/module-progress/components/certificate-status'
);
export const DurationProperty = AdminJS.bundle(
  '../apps/module-progress/components/duration.list'
);
export const TaskSubmission = AdminJS.bundle(
  '../apps/module-progress/components/task-submission'
);
export const ViewSubmissions = AdminJS.bundle(
  '../apps/module-progress/components/view-submissions'
);

// Steps
export const MultiImageInput = AdminJS.bundle(
  '../apps/steps/components/multi-image.edit'
);
export const ResourcesInput = AdminJS.bundle(
  '../apps/steps/components/resources.edit'
);
export const Resources = AdminJS.bundle('../apps/steps/components/resources.show');

// Tasks
export const MultiCreatableSelect = AdminJS.bundle(
  '../apps/tasks/components/multi-creatable-select'
);
export const Tags = AdminJS.bundle('../apps/tasks/components/tags.show');

// Certificates
export const FileURL = AdminJS.bundle('../apps/certificates/components/file-url');

// Cert Generator page
export const CertGeneratorPage = AdminJS.bundle(
  './components/cert-generator-page'
);
