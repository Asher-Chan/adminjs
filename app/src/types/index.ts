import type { NextApiRequest, NextApiResponse, NextPage } from "next";
import { ReactElement, ReactNode } from "react";

// Better name please
export type PageProps<Props> =
  | {
      error: true;
      errorMessage: string;
    }
  | ({
      error: false;
    } & Props);

export type NextAPIReq<
  TQuery extends {
    [key: string]: string | string[];
  } = {
    [key: string]: string | string[];
  },
  TBody = void
> = NextApiRequest & {
  query: TQuery;
  // Not providing types
  body: TBody;
};

// Default to void makes it optional
export type NextAPIRes<
  TResponse = {
    error: false;
    [key: string]: any;
  }
> = NextApiResponse<
  | {
      error: true;
      errorMessage: string;
      errorCode?: number;
    }
  | ({
      error: false;
    } & TResponse)
>;

export type NextPageWithLayout<P = {}> = NextPage<P> & {
  getLayout?: (page: ReactElement) => ReactNode;
};
