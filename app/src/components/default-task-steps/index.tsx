import React from "react";

import styles from "./index.module.scss";

type Props = {};

const DefaultTaskSteps: React.FC<Props> = () => {
  return (
    <div>
      <div className={styles.container}>
        <h3>Submit your work</h3>
        <div>
          You may resubmit work, iterate upon and reattempt work as many times
          as you want until you complete all the tasks and mark module as
          complete. After module is complete to make any more changes, you will
          have to request resubmission from your module dashboard!
        </div>
      </div>

      <div className={styles.container}>
        <h3>Unlock the model work</h3>
        <div>
          Download an example of work done on this topic. To unlock this model
          answer, please submit your work for this task.
        </div>
      </div>
    </div>
  );
};

export default DefaultTaskSteps;
