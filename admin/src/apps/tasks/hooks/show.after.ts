import {
  ActionResponse,
  ActionContext,
  BaseRecord,
  ActionRequest,
} from 'adminjs';

import { Step, Task } from '../../../models';
import { resourceIds } from '../../../constants/resource-ids';

export const populateStepsData = async (
  originalResponse: ActionResponse,
  req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const taskSteps = await Step.findMany({
    where: {
      taskId: parseInt(req.params.recordId),
    },
  });

  // eslint-disable-next-line no-underscore-dangle
  const stepResource = context._admin.findResource(resourceIds.STEPS);

  const stepRecords = taskSteps?.map((step) =>
    new BaseRecord(
      {
        ...step,
        itemLink: context.h.showUrl(resourceIds.STEPS, step.id.toString()),
      },
      stepResource
    ).toJSON(context.currentAdmin)
  );

  const addNewLink = context.h.newUrl(
    resourceIds.STEPS,
    `?taskId=${req.params.recordId}`
  );

  return {
    ...originalResponse,
    record: {
      ...record,
      params: {
        ...record.params,
        addNewLink,
      },
      populated: {
        ...record.populated,
        steps: stepRecords,
      },
    },
  };
};

export const populateTasksData = async (
  originalResponse: ActionResponse,
  _req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const tasks = await Task.findMany({
    where: {
      moduleId: record.params.module,
    },
    orderBy: {
      createdAt: 'asc',
    },
  });

  const { tags } = await Task.findUnique({
    where: {
      id: record.params.id,
    },
    select: {
      tags: true,
    },
  });

  const taskRecords = tasks?.map((task) =>
    new BaseRecord(
      {
        ...task,
        itemLink: context.h.showUrl(resourceIds.TASKS, task.id.toString()),
      },
      context.resource
    ).toJSON(context.currentAdmin)
  );

  return {
    ...originalResponse,
    record: {
      ...record,
      params: {
        ...record.params,
        tags: tags.map((tag) => tag.name),
      },
      populated: {
        ...record.populated,
        tasks: taskRecords,
      },
    },
  };
};
