import {
  GetServerSideProps,
  GetServerSidePropsContext,
  GetServerSidePropsResult,
} from "next";
import { Session } from "next-auth";
import { getSession } from "next-auth/react";

export const requireAuthentication = <
  // Thank u chung wei
  TProps extends { [key: string]: any } = { [key: string]: any }
>(
  gssp: (
    context: GetServerSidePropsContext,
    session: Session
  ) => Promise<GetServerSidePropsResult<TProps>>,
  redirectDestination?: string
): GetServerSideProps => {
  return async (ctx: GetServerSidePropsContext) => {
    const session = await getSession({ req: ctx.req });

    if (!session?.auth.id) {
      return {
        redirect: {
          destination: redirectDestination || "/auth/sign-in",
          permanent: false,
        },
      };
    }

    return await gssp(ctx, session);
  };
};
