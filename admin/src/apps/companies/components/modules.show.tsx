import React from 'react';
import { Box, H4, Button, Icon } from '@adminjs/design-system';
import { BaseRecord, ShowPropertyProps } from 'adminjs';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

const Module = styled(Box)`
  background: #8172e0;
  border-radius: 6px;
  padding: 1.2rem;
  color: #fff;
  margin-bottom: 0.8rem;

  &:hover {
    background: #9d93dc;
    cursor: pointer;
  }
`;

const Modules: React.FC<ShowPropertyProps> = ({ record }) => {
  const history = useHistory();
  const records = record.populated.modules as unknown as BaseRecord[];

  const onClickItem = (itemLink: string) => {
    history.push(itemLink);
  };

  const onClickAddNewItem = () => {
    history.push(record.params.addNewLink);
  };

  return (
    <Box>
      <Box flex flexDirection="row">
        <Box flexGrow={1}>
          <H4>Modules</H4>
        </Box>
        <Box flexShrink={0}>
          <Button flex variant="light" onClick={onClickAddNewItem}>
            <Icon icon="Add" />
            Add Module
          </Button>
        </Box>
      </Box>

      {records?.length ? (
        records.map((item) => {
          return (
            <Module
              key={item.params.id}
              onClick={() => onClickItem(item.params.itemLink)}
            >
              {item.params.title}
            </Module>
          );
        })
      ) : (
        <div>No modules have been added yet.</div>
      )}
    </Box>
  );
};

export default Modules;
