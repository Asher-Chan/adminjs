import React from 'react';
import { EditPropertyProps } from 'adminjs';
import { Box, Label, Input } from '@adminjs/design-system';

const DisplayNameEdit: React.FC<EditPropertyProps> = ({
  record,
  onChange,
  property,
}) => {
  const { fullName, displayName: inputValue } = record.params;

  return (
    <Box mb={40}>
      <Label required={property.isRequired}>
        Display Name (Auto-populated if empty)
      </Label>
      <Input
        value={inputValue || fullName}
        onChange={(e) => onChange(property.path, e.target.value)}
        width="100%"
      />
    </Box>
  );
};

export default DisplayNameEdit;
