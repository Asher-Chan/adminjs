import { Module } from "@prisma/client";
import { getSession } from "next-auth/react";

import { prisma } from "@/utils/db";
import { getAllModuleProgressOfTalent } from "./mine";
import { NextAPIRes, NextAPIReq } from "@/types/index";

export default async function handler(
  req: NextAPIReq<{
    cursor: string;
    search?: string;
    tags?: string;
    status?: string;
  }>,
  res: NextAPIRes<{ modules: Module[]; nextCursor?: number }>
) {
  // This might break, need to test with more data
  // Should reset cursor on new filters
  const cursor = parseInt(req.query.cursor) || null;
  const search = req.query.search;
  const tags = req.query?.tags?.split(",") || [];
  const status = req.query.status;
  const pageSize = 1;

  let modules: Module[] = [];

  // TODO: Refine This query into one
  if (status) {
    const session = await getSession({ req });
    // Get user id
    const userId = session?.auth.id;
    if (!userId) {
      res.status(400).json({ error: true, errorMessage: "Talent not found" });
      return;
    }
    // TODO: Instead of this can try doing using the same query below, search module progress attached to each module WHERE same talent ID & filter by correct status
    // Filter all module progress of this talent where IN PROGRESS, COMPLETED
    try {
      modules = await getAllModuleProgressOfTalent(
        userId,
        cursor
          ? {
              cursor,
              pageSize,
              moduleTitle: search,
              taskTags: tags,
            }
          : undefined
      );
    } catch (err) {
      // Turn into reusable code
      const errMessage = !(err instanceof Error)
        ? "Error fetching modules"
        : err.message;
      res.status(400).json({ error: true, errorMessage: errMessage });
    }
  } else {
    modules = await prisma.module.findMany({
      ...(cursor === null ? {} : { cursor: { id: cursor } }),
      take: pageSize + 1, // Get one extra to get next cursor
      ...(tags.length || search
        ? {
            where: {
              // Tags filter
              ...(tags.length
                ? {
                    tasks: {
                      some: {
                        tags: {
                          some: {
                            name: {
                              in: tags,
                            },
                          },
                        },
                      },
                    },
                  }
                : {}),
              // Search filter
              ...(search
                ? {
                    title: {
                      contains: search,
                    },
                  }
                : {}),
            },
          }
        : {}),
    });
  }

  // Status filter will have to be different query all together

  res.status(200).json({
    error: false,
    modules:
      modules.length === pageSize + 1 ? modules.slice(0, pageSize) : modules,
    nextCursor:
      modules.length === pageSize + 1
        ? modules[modules.length - 1].id
        : undefined,
  });
}
