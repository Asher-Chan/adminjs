import {
  Company,
  User,
  Module,
  ModuleProgress,
  Task,
  Tag,
  Submission,
  Talent,
} from "@prisma/client";

// Either create a type for each use case
// OR
// Create types for each model with all their populated relations

export type CompanyWithUser = Company & {
  user?: User | null;
};

export type TaskWithTags = Task & {
  tags: Tag[];
};

export type ModuleWithTalentProgress = Module & {
  company?: CompanyWithUser | null;
  status: ModuleProgress["status"];
  moduleProgressId: number;
};

export type ModuleWithModuleProgress = Module & {
  company: CompanyWithUser | null;
  moduleProgress: ModuleProgress;
};

export type ModuleWithCompany = Module & {
  company: CompanyWithUser | null;
};

export type ModuleWithCompanyAndTasks = Module & {
  company?: CompanyWithUser | null;
  tasks: TaskWithTags[];
};

export type TalentsModuleSubmissionProgress = ModuleProgress & {
  module?: ModuleWithCompanyAndTasks | null;
  submissions: Submission[];
  talent: Talent;
};
