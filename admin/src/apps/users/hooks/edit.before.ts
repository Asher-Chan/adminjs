import { ActionRequest } from 'adminjs';
import bcrypt from 'bcrypt';

export const handlePasswordBeforeSave = async (req: ActionRequest) => {
  const { password, ...payload } = req.payload;

  if (!password) {
    return req;
  }

  return {
    ...req,
    payload: {
      ...payload,
      password: await bcrypt.hash(password, Number(process.env.HASH_ROUNDS)),
    },
  };
};
