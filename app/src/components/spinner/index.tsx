import React from "react";
import classnames  from 'classnames';

import styles from "./index.module.scss";

type Props = {
  size?: "small" | "medium" | "large";
};

const Spinner: React.FC<Props> = ({ size = 'small' }) => (
  <div className={classnames(styles.loadingSpinner, styles[`__${size}`])} />
);

export default Spinner;