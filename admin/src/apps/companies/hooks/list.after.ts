import { ActionResponse, BaseRecord } from 'adminjs';

export const fetchPopulatedCompanies = async (
  originalResponse: ActionResponse
): Promise<ActionResponse> => {
  const { records, ...responseData } = originalResponse;

  const populatedRecords = (records as BaseRecord[]).map((record) => ({
    ...record,
    params: {
      ...record.params,
      fullName: record.params['user.fullName'],
      email: record.params['user.email'],
      createdAt: record.params['user.createdAt'],
      isActive: record.params['user.isActive'],
      displayName: record.params['user.displayName'],
    },
  }));

  return { ...responseData, records: populatedRecords };
};
