import {
  ActionResponse,
  ActionRequest,
  ActionContext,
  BaseRecord,
} from 'adminjs';

import { resourceIds } from '../../../constants/resource-ids';
import { StepResource, Step } from '../../../models';

export const populateStepsResourceData = async (
  originalResponse: ActionResponse,
  req: ActionRequest
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const stepResources = await StepResource.findMany({
    where: {
      stepId: parseInt(req.params.recordId),
    },
  });

  return {
    ...originalResponse,
    record: {
      ...record,
      params: {
        ...record.params,
      },
      populated: {
        ...record.populated,
        stepResources,
      },
    },
  };
};

export const populateStepsData = async (
  originalResponse: ActionResponse,
  _req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  const { record } = originalResponse;

  const steps = await Step.findMany({
    where: {
      taskId: record.params.task,
    },
  });

  const stepRecords = steps?.map((step) =>
    new BaseRecord(
      {
        ...step,
        itemLink: context.h.showUrl(resourceIds.STEPS, step.id.toString()),
      },
      context.resource
    ).toJSON(context.currentAdmin)
  );

  return {
    ...originalResponse,
    record: {
      ...record,
      populated: {
        ...record.populated,
        steps: stepRecords,
      },
    },
  };
};
