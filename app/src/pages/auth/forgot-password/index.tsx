import React, { FormEventHandler } from "react";
import type { GetServerSideProps, NextPage } from "next";
import { getSession } from "next-auth/react";
import { useMutation } from "react-query";

import TextInput from "@/components/inputs/text-input";
import Button from "@/components/button";
import StatusBar from "@/components/status-bar";

type MutationParams = {
  email: string;
};

const forgotPassword = async ({ email }: MutationParams) => {
  const res: Response = await fetch("/api/user/forgot-password", {
    method: "POST",
    body: JSON.stringify({
      email,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });

  if (res.ok) {
    return res.json();
  }

  throw new Error("Error occured");
};

const ForgotPassword: NextPage = () => {
  const mutation = useMutation<{}, Error, MutationParams>(async (params) =>
    forgotPassword(params)
  );

  const onSubmitHandler: FormEventHandler<HTMLFormElement> = (
    e: React.SyntheticEvent
  ) => {
    e.preventDefault();
    const target = e.target as typeof e.target & { email: { value: string } };
    mutation.mutate({ email: target.email.value });
  };

  return (
    <div>
      {mutation.isSuccess ? (
        <StatusBar status="success" customMessage="An email has been sent" />
      ) : null}
      {mutation.isError ? <StatusBar status="error" /> : null}
      <h4>Forgot password</h4>

      <form onSubmit={onSubmitHandler}>
        <TextInput name="email" label="Email" type="email" required />

        <Button
          type="submit"
          label="Submit"
          disabled={mutation.isLoading}
          loading={mutation.isLoading}
        />
      </form>
    </div>
  );
};

export default ForgotPassword;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getSession(ctx);

  if (session?.auth) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
