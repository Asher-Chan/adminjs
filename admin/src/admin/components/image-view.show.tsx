import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';

const ImageView: React.FC<ShowPropertyProps> = ({ record, property }) => {
  const url = record.params[property.path];

  return url ? (
    <Box>
      <Label uppercase variant="light">
        {property.label}
      </Label>
      <img src={url} alt={property.label} />
    </Box>
  ) : null;
};

export default ImageView;
