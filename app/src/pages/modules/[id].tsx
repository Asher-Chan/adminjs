import React from "react";
import { NextPage, GetServerSideProps } from "next";
import Image from "next/image";

import { prisma } from "@/utils/db";
import { ModuleWithCompanyAndTasks } from "@/types/modules";

type Props = {
  module: ModuleWithCompanyAndTasks;
};

const ModulePage: NextPage<Props> = ({ module }) => {
  const getTaskNumber = (idx: number): string => {
    return ((idx + 1) / 10).toFixed(1);
  };

  return (
    <div>
      {!!module?.company?.logoURL && (
        <Image
          src={module.company.logoURL}
          alt="Logo url"
          width="300px"
          height="100px"
        />
      )}
      <div>{module.title}</div>
      <div>Registration Fee: {module.registrationFee}</div>
      <div>
        Est completion time: {module.estCompletionTimeStart} -
        {module.estCompletionTimeEnd} hrs
      </div>
      <button>Join Now</button>

      <hr />

      <h4>Why Join this module?</h4>
      <div>{module.desc}</div>

      <h4>What is in this module?</h4>
      <div>
        {module.tasks.length
          ? module.tasks.map((task, idx) => {
              return (
                <div key={task.id}>
                  <div>
                    {getTaskNumber(idx)} {task.title} - {task.shorthandTitle}
                  </div>
                  <div>{task.desc}</div>

                  <h5>What will you learn?</h5>
                  <ul>
                    {task.tags.length
                      ? task.tags.map((tag) => <li key={tag.id}>{tag.name}</li>)
                      : null}
                  </ul>

                  <button>Open Task</button>
                </div>
              );
            })
          : null}
      </div>

      {!!module.introVideoURL && <video src={module.introVideoURL}></video>}
    </div>
  );
};

export default ModulePage;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const moduleId = parseInt(ctx.query.id as string) || null;

  if (!moduleId) {
    return {
      notFound: true,
      props: {},
    };
  }

  const selectedModule = await prisma.module.findUnique({
    where: {
      id: moduleId,
    },
    include: {
      company: {
        include: {
          user: true,
        },
      },
      tasks: {
        include: {
          tags: true,
        },
      },
    },
  });

  if (!selectedModule) {
    return {
      notFound: true,
      props: {},
    };
  }

  const props: Props = {
    module: JSON.parse(JSON.stringify(selectedModule)),
  };

  return {
    props,
  };
};
