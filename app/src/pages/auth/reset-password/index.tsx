import React, { FormEventHandler } from "react";
import type { GetServerSideProps, NextPage } from "next";
import { useMutation } from "react-query";

import TextInput from "@/components/inputs/text-input";
import Button from "@/components/button";
import StatusBar from "@/components/status-bar";

type Props = {
  token: string;
  id: string;
};

type MutationParams = {
  token: string;
  id: string;
  password: string;
};

const resetPassword = async ({ token, id, password }: MutationParams) => {
  const res: Response = await fetch("/api/user/reset-password", {
    method: "POST",
    body: JSON.stringify({
      token,
      id,
      password,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });

  if (res.ok) {
    return res.json();
  }

  throw new Error("Error occured");
};

const ResetPassword: NextPage<Props> = ({ token, id }) => {
  const mutation = useMutation<{}, Error, MutationParams>(async (params) =>
    resetPassword(params)
  );

  // Create a function wrapper where u can pass target types in
  const onSubmitHandler: FormEventHandler<HTMLFormElement> = (
    e: React.SyntheticEvent
  ) => {
    e.preventDefault();
    const target = e.target as typeof e.target & {
      password: { value: string };
    };
    mutation.mutate({ password: target.password.value, token, id });
  };

  return (
    <div>
      {mutation.isSuccess ? (
        <StatusBar status="success" customMessage="An email has been sent" />
      ) : null}
      {mutation.isError ? <StatusBar status="error" /> : null}
      <h4>Reset password</h4>

      <form onSubmit={onSubmitHandler}>
        <TextInput name="password" label="New Password" type="password" />

        <Button type="submit" label="Submit" />
      </form>
    </div>
  );
};

export default ResetPassword;

export const getServerSideProps: GetServerSideProps<Props> = async (ctx) => {
  const { token, id } = ctx.query as { token: string; id: string };

  if (!token || !id) {
    return {
      redirect: {
        destination: "/auth/sign-in",
        permanent: true,
      },
    };
  }

  return {
    props: {
      token,
      id,
    },
  };
};
