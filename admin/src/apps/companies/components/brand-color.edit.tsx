import React from 'react';
import { EditPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';
import { TwitterPicker } from 'react-color';

const BrandColorEdit: React.FC<EditPropertyProps> = ({
  record,
  onChange,
  resource,
  property,
}) => {
  const { brandColor: inputValue } = record.params;
  const { isRequired } = resource.properties[property.path];

  return (
    <Box mb={24}>
      <Label required={isRequired}>Brand Color</Label>
      {/** Probably use different picker */}
      <TwitterPicker
        color={inputValue}
        triangle="hide"
        styles={{ default: { card: { boxShadow: 'none' } } }}
        width="100%"
        onChange={(e) => onChange(property.path, e.hex)}
      />
      {/** Can be prettified */}
      <div
        style={{
          backgroundColor: inputValue,
          width: '60px',
          height: '30px',
          borderRadius: '10px',
        }}
      />
    </Box>
  );
};

export default BrandColorEdit;
