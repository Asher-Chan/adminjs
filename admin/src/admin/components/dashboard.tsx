import React, { useState, useEffect } from 'react';
import { ApiClient } from 'adminjs';
import { useHistory } from 'react-router-dom';
import { Box, Header, Icon } from '@adminjs/design-system';
import styled from 'styled-components';

type StatsData = {
  totalCompanies: number;
  totalTalents: number;
  totalCertificates: number;
  totalModules: number;
};

type CardProps = {
  onClickHandler: () => void;
  title: string;
  totalNumber: string;
  iconName: string;
};

const Card = styled(Box)`
  width: 35%;
  margin-left: 2rem;
  margin-right: 2rem;
  margin-bottom: 2rem;
  border-radius: 6px;
  background-color: white;
  padding: 1.4rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transition: 0.2s ease;
  border: 1px solid transparent;

  &:hover {
    border-color: #8172e0;
    cursor: pointer;

    .hiddenText {
      opacity: 1;
    }
  }

  @media (max-width: 768px) {
    width: 75%;
    margin-left: 0.5rem;
    margin-right: 0.5rem;
  }
`;

const CardHeader = styled(Box)`
  width: 100%;
  display: flex;
  justify-content: start;
  align-items: center;
  font-size: 1rem;
`;

const CardContent = styled(Box)`
  width: 100%;
  padding-top: 4.8rem;
  display: flex;
  justify-content: space-between;
  font-size: 2rem;
  color: #8172e0;
  font-weight: normal;

  .hiddenText {
    opacity: 0;
    color: #8172e0;
    text-decoration: underline;
    font-weight: normal;
    font-size: 0.9rem;
    transition: 0.2s ease;
  }
`;

const DashboardCard: React.FC<CardProps> = ({
  title,
  totalNumber,
  iconName,
  onClickHandler,
}) => {
  return (
    <Card variant="card" onClick={onClickHandler}>
      <CardHeader>
        <Icon size={32} mr={8} icon={iconName} />
        {title}
      </CardHeader>
      <CardContent>
        <Box className="hiddenText">
          <Icon color="#8172e0" mr={4} icon="ArrowRight" /> View More
        </Box>
        {totalNumber}
      </CardContent>
    </Card>
  );
};

const Dashboard: React.FC = () => {
  const [dashboardData, setDashboardData] = useState<StatsData | null>(null);
  const history = useHistory();

  const api = new ApiClient();

  const fetchStats = async () => {
    const data = await api.getDashboard();
    setDashboardData(data.data as StatsData);
  };

  const onClickHandler = (redirectLink: string): void =>
    history.push(redirectLink);

  useEffect(() => {
    fetchStats();
  }, []);

  return (
    <Box
      px={28}
      height="100%"
      variant="white"
      flex
      justifyContent="start"
      flexDirection="column"
    >
      <Header>Dashboard</Header>

      {/** Statistics */}
      <Box
        mt={28}
        flex
        flexDirection="row"
        justifyContent="center"
        flexWrap="wrap"
      >
        <DashboardCard
          onClickHandler={() => onClickHandler('/admin/resources/Companies')}
          title="Companies"
          totalNumber={dashboardData?.totalCompanies?.toString() || '-'}
          iconName="Portfolio"
        />
        <DashboardCard
          onClickHandler={() => onClickHandler('/admin/resources/Talents')}
          title="Talents"
          totalNumber={dashboardData?.totalTalents?.toString() || '-'}
          iconName="User"
        />
        <DashboardCard
          onClickHandler={() => onClickHandler('/admin/resources/Certificates')}
          title="Certificates Issued"
          totalNumber={dashboardData?.totalCertificates?.toString() || '-'}
          iconName="Certificate"
        />
        <DashboardCard
          onClickHandler={() => onClickHandler('/admin/resources/Modules')}
          title="Modules"
          totalNumber={dashboardData?.totalModules?.toString() || '-'}
          iconName="Document"
        />
      </Box>
    </Box>
  );
};

export default Dashboard;
