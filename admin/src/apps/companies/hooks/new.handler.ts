import {
  ActionContext,
  ActionRequest,
  ActionResponse,
  BaseRecord,
  ValidationError,
} from 'adminjs';
import { UserRoles } from '@prisma/client';

import { User } from '../../../models';
import { SortedCompanyPayload } from '../../../types/forms';
import { uploadFile } from '../../../utils/cloudinary';
import { resourceIds } from '../../../constants/resource-ids';

export const handleNewCompany = async (
  req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
): Promise<ActionResponse> => {
  const { attachedUser, address, ...company } =
    req.payload as SortedCompanyPayload;

  if (!req.payload.briefIntro) {
    throw new ValidationError(
      {
        briefIntro: {
          message: 'Brief intro is required',
        },
      },
      {
        message: 'Some field are missing',
      }
    );
  }

  if (context.uploadedLogoFile) {
    company.logoURL = await uploadFile({ filePath: context.uploadedLogoFile });
  }

  if (context.uploadedSignatureFile) {
    company.signatureFileURL = await uploadFile({ filePath: context.uploadedSignatureFile });
  }

  const newUser = await User.create({
    data: {
      ...attachedUser,
      ...(context.uploadedProfilePhoto
        ? {
            profileImgURL: await uploadFile({
              filePath: context.uploadedProfilePhoto,
            }),
          }
        : {}),
      role: UserRoles.COMPANY,
      isVerified: true,

      // Address
      address: { create: { ...address } },

      // Company
      company: { create: { ...company } },
    },
    include: {
      company: true,
    },
  });

  const record = new BaseRecord(newUser, context.resource).toJSON(
    context.currentAdmin
  );

  return {
    record,
    redirectUrl: context.h.showUrl(
      resourceIds.COMPANIES,
      newUser.company.id.toString()
    ),
  };
};
