import { ActionContext, ActionRequest, ActionResponse } from 'adminjs';

import { resourceIds } from '../../../constants/resource-ids';

export const redirectAfterSave = async (
  originalResponse: ActionResponse,
  req: ActionRequest,
  context: ActionContext
): Promise<ActionResponse> => {
  if (req.method === 'post') {
    const { record } = originalResponse;

    const redirectUrl = context.h.showUrl(resourceIds.TASKS, record.params.id);

    return {
      ...originalResponse,
      redirectUrl,
    };
  }

  return originalResponse;
};
