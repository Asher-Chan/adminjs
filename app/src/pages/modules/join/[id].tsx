import React from "react";
import { NextPage } from "next";
import Image from "next/image";
import { useMutation } from "react-query";
import { useForm, FormProvider } from "react-hook-form";

import { prisma } from "@/utils/db";
import { ModuleWithCompany } from "@/types/modules";
import { PageProps } from "@/types/index";
import { requireAuthentication } from "@/utils/auth";
import TextInput from "@/components/form-inputs/text-input";
import Button from "@/components/button";

type Props = PageProps<{
  module: ModuleWithCompany;
  talentId: number;
}>;

type FormValues = {
  reasonInterested: string;
  companyKnowledgeLevel: number;
  joinCompanyPotentialLevel: number;
  likelinessToApplyLevel: number;
};

const JoinModule: NextPage<Props> = (props) => {
  const formMethods = useForm<FormValues>({
    defaultValues: {
      reasonInterested: "",
      companyKnowledgeLevel: 0,
      joinCompanyPotentialLevel: 0,
      likelinessToApplyLevel: 0,
    },
  });

  const {
    handleSubmit,
    formState: { errors },
    reset,
  } = formMethods;
  const { error, mutate, isLoading } = useMutation(() => {
    // TODO: Use axios
    return fetch("/api/modules/registration-form", {
      method: "POST",
      body: JSON.stringify({
        moduleId: module.id,
        talentId,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    });
  });

  if (props.error) {
    return <div>{props.errorMessage}</div>;
  }

  const { module, talentId } = props;
  const { company } = module;

  return (
    <div>
      {isLoading && <div>Loading...</div>}
      {!!error && <div>Error occured</div>}

      {!!company.logoURL && (
        <Image
          src={company.logoURL}
          alt="company logo"
          width="150px"
          height="50px"
        />
      )}
      <div>{company.user?.fullName}</div>
      <div>{module.title}</div>

      <br />

      <FormProvider {...formMethods}>
        <TextInput
          name="reasonInterested"
          label="Why are you interested in this module"
          type="text"
        />
        <TextInput
          name="companyKnowledgeLevel"
          label="How much do you currently know"
          type="number"
        />
        <TextInput
          name="joinCompanyPotentialLevel"
          label="How confident are you in potentially working with us in the future"
          type="number"
        />
        <TextInput
          name="likelinessToApplyLevel"
          label="Based on what you know, how likely are you to apply here"
          type="number"
        />

        <Button label="Submit" type="submit" />
      </FormProvider>
    </div>
  );
};

export default JoinModule;

export const getServerSideProps = requireAuthentication<Props>(
  async (ctx, session) => {
    const moduleId = parseInt(ctx.query.id as string) || null;

    if (!moduleId) {
      return {
        redirect: {
          destination: "/modules",
          permanent: false,
        },
      };
    }

    const talent = await prisma.talent.findUnique({
      where: {
        userId: session.auth.id,
      },
    });

    if (!talent) {
      return {
        props: {
          error: true,
          errorMessage: "Talent not found",
        },
      };
    }

    const selectedModule = await prisma.module.findUnique({
      where: {
        id: moduleId,
      },
      include: {
        company: {
          include: {
            user: true,
          },
        },
      },
    });

    return {
      props: {
        error: false,
        module: JSON.parse(JSON.stringify(selectedModule)),
        talentId: talent.id,
      },
    };
  }
);
