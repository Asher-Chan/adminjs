import React from "react";
import type { NextPage } from "next";
import { useForm, FormProvider } from "react-hook-form";
import { useMutation } from "react-query";

import TextInput from "@/components/form-inputs/text-input";
import Button from "@/components/button";
import FileInput from "@/components/form-inputs/file-input";
import StatusBar from "@/components/status-bar";

type FormValues = {
  name: string;
  email: string;
  companyName: string;
  contactNumber: string;
  message: string;
  attachment?: FileList;
};

const becomeHiringPartner = async ({
  name,
  email,
  companyName,
  contactNumber,
  message,
  attachment,
}: FormValues) => {
  const formData = new FormData();

  formData.append("name", name);
  formData.append("email", email);
  formData.append("companyName", companyName);
  formData.append("contactNumber", contactNumber);
  formData.append("message", message);

  if (attachment?.[0]) {
    formData.append("attachment", attachment[0]);
  }

  const res: Response = await fetch("/api/become-hiring-partner", {
    method: "POST",
    body: formData,
  });

  if (res.ok) {
    return res.json();
  }

  throw new Error("Error occured");
};

const BecomeHiringPartner: NextPage = () => {
  const mutation = useMutation<{}, Error, FormValues>(async (params) =>
    becomeHiringPartner(params)
  );

  const formMethods = useForm<FormValues>({
    defaultValues: {
      name: "",
      email: "",
      companyName: "",
      // Phone number validation
      contactNumber: "",
      message: "",
      attachment: undefined,
    },
  });

  const {
    formState: { isSubmitting },
    handleSubmit,
    reset,
    watch,
  } = formMethods;

  const onSubmitHandler = (formValues: FormValues) => {
    mutation.mutate(formValues, {
      onSuccess: () => reset(),
    });
  };

  console.log(watch("attachment"));

  return (
    <div>
      {mutation.isSuccess ? (
        <StatusBar status="success" customMessage="An email has been sent" />
      ) : null}
      {mutation.isError ? <StatusBar status="error" /> : null}
      <h4>Become a Hiring Partner</h4>

      <FormProvider {...formMethods}>
        <form onSubmit={handleSubmit(onSubmitHandler)}>
          <TextInput label="Name" name="name" type="text" required />
          <TextInput label="Email" name="email" type="email" required />
          <TextInput
            label="Company Name"
            name="companyName"
            type="text"
            required
          />
          <TextInput
            label="Contact Number"
            name="contactNumber"
            type="text"
            required
            // Phone number text-mask ?
          />
          <TextInput
            rows={8}
            label="Message"
            name="message"
            type="text-area"
            required
          />

          <FileInput name="attachment" />

          <Button
            loading={mutation.isLoading || isSubmitting}
            label="Submit"
            type="submit"
          />
        </form>
      </FormProvider>
    </div>
  );
};

export default BecomeHiringPartner;
