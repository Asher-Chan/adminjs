import { v2 as cloudinary, UploadApiOptions } from 'cloudinary';

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CNAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

export const uploadFile = async ({
  filePath,
  options,
}: {
  filePath: string;
  options?: UploadApiOptions;
  // When editing logo example
  prevFileToRemove?: string;
}): Promise<string> => {
  const url = await cloudinary.uploader.upload(filePath, options, (err) => {
    if (err) {
      console.log(err);
      throw new Error('Error uploading image');
    }
  });

  return url.url;
};

export const uploadCertificatePDF = async (certBuffer: Buffer): Promise<string> => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader
      .upload_stream(
        {
          folder: 'certficates',
        },
        (error, result) => {
          if (error) {
            return reject(error);
          }
          return resolve(result.url);
        }
      )
      .end(certBuffer);
  });
};
