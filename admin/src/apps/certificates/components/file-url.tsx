import React from 'react';
import { BasePropertyProps } from 'adminjs';
import { Link, Icon, Box, Label } from '@adminjs/design-system';
import { saveAs } from 'file-saver';

const FileURL: React.FC<BasePropertyProps> = ({ record, where }) => {
  const handleDownloadCert = () => {
    // Can add dynamic file name
    saveAs(record.params.fileURL);
  };

  function renderLink() {
    return record.params.fileURL ? (
      <Link onClick={handleDownloadCert} variant="primary">
        <Icon icon="Download" />
        Download Cert
      </Link>
    ) : (
      <Box> - </Box>
    );
  }

  return where === 'show' ? (
    <Box mb={24}>
      <Label mb={12} uppercase variant="light">
        Certificate
      </Label>
      {renderLink()}
    </Box>
  ) : (
    renderLink()
  );
};

export default FileURL;
