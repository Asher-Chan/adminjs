import { ResourceNames } from '../types';

// eslint-disable-next-line no-unused-vars
export const resourceIds: { [x in ResourceNames]: string } = {
  COMPANIES: 'Companies',
  TALENTS: 'Talents',
  USERS: 'Admins', // Cannot alter navigation
  MODULES: 'Modules',
  TASKS: 'Tasks',
  STEPS: 'Steps',
  TAGS: 'Tags',
  SUBMISSIONS: 'Submissions',
  MODULE_PROGRESS: 'Module Progress',
  CERTIFICATES: 'Certificates',
};
