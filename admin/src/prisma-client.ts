import { PrismaClient } from '@prisma/client';

// TS PROBELM PLEASE FIX

// // add prisma to the NodeJS global type
// interface CustomNodeJsGlobal extends NodeJS.Global {
//   prisma: PrismaClient;
// }

// // Prevent multiple instances of Prisma Client in development
// declare var global: CustomNodeJsGlobal;

// const prisma = global.prisma || new PrismaClient();

// if (process.env.NODE_ENV === 'development') global.prisma = prisma;

const prisma = new PrismaClient();

export default prisma;
