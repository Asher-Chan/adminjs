import React, { useState, useEffect } from 'react';
import { ApiClient } from 'adminjs';
import { Box, Header, Button, Label } from '@adminjs/design-system';
import Select from 'react-select';

const CertGeneratorPage: React.FC = () => {
  const [moduleOptions, setModuleOptions] = useState([]);
  const [talentOptions, setTalentOptions] = useState([]);
  const [certData, setCertData] = useState({
    moduleId: undefined,
    talentId: undefined,
    certPDFLink: '',
  });

  const api = new ApiClient();

  const fetchOptions = async () => {
    const result = await api.getPage({
      pageName: 'Cert Generator',
      method: 'GET',
    });

    const { modules, talents } = result.data;

    setModuleOptions(
      modules?.length
        ? modules.map((module) => ({
            label: module.title,
            value: module.id,
          }))
        : []
    );

    setTalentOptions(
      talents?.length
        ? talents.map((talent) => ({
            label: talent.user.fullName,
            value: talent.id,
          }))
        : []
    );
  };

  const handleSubmit = async () => {
    setCertData({
      moduleId: undefined,
      talentId: undefined,
      certPDFLink: '',
    });
    try {
      const result = await api.getPage({
        pageName: 'Cert Generator',
        method: 'POST',
        data: {
          moduleId: certData.moduleId,
          talentId: certData.talentId,
        },
      });

      if (result.data.certPDFLink) {
        setCertData({
          moduleId: undefined,
          talentId: undefined,
          certPDFLink: result.data.certPDFLink,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchOptions();
  }, []);

  return (
    <Box height="100%" variant="white">
      <Header>Certificate Generator</Header>
      {/** Module Select */}
      <Box mb={16}>
        <Label>Module</Label>
        <Select
          name="Module"
          options={moduleOptions}
          onChange={(option) =>
            setCertData((prevData) => ({
              ...prevData,
              moduleId: option.value,
            }))
          }
        />
      </Box>
      {/** Talent Select */}
      <Box mb={16}>
        <Label>Talent</Label>
        <Select
          name="Talent"
          options={talentOptions}
          onChange={(option) =>
            setCertData((prevData) => ({
              ...prevData,
              talentId: option.value,
            }))
          }
        />
      </Box>

      <Box flex justifyContent="end" my={28}>
        <Button variant="primary" onClick={handleSubmit}>
          Generate Certificate
        </Button>
      </Box>

      {certData.certPDFLink ? (
        <Box mt={24}>
          <a href={certData.certPDFLink || ''} target="_blank" rel="noreferrer">
            View Certificate
          </a>
        </Box>
      ) : null}
    </Box>
  );
};

export default CertGeneratorPage;
