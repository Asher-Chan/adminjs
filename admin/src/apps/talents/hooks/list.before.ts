import {
  ActionResponse,
  BaseRecord,
  ActionRequest,
  ActionContext,
} from 'adminjs';

import { Talent } from '../../../models';

// TODO: (AC) Extract into separate function to be re-used
export const handleFetchTalents = async (
  req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
) => {
  const { query } = req;
  const perPage = 10;

  const { page = 1 } = query || {};

  const email: string = query['filters.email'];
  const fullName: string = query['filters.fullName'];
  const isActive: string = query['filters.isActive'];
  const jobTitle: string = query['filters.jobTitle'];
  const displayName: string = query['filters.displayName'];
  const createdAtFrom: string = query['filters.createdAt~~from'];
  const createdAtTo: string = query['filters.createdAt~~to'];

  const talents = await Talent.findMany({
    skip: (page - 1) * perPage,
    take: perPage,
    where: {
      ...(jobTitle ? { jobTitle: { contains: jobTitle } } : {}),
      isDeleted: false,
      user: {
        ...(email ? { email: { contains: email } } : {}),
        ...(fullName ? { fullName: { contains: fullName } } : {}),
        ...(displayName ? { displayName: { contains: displayName } } : {}),
        ...(isActive ? { isActive: isActive === 'true' || false } : {}),
        ...(createdAtFrom || createdAtTo
          ? {
              createdAt: {
                ...(createdAtFrom ? { gte: new Date(createdAtFrom) } : {}),
                ...(createdAtTo ? { lt: new Date(createdAtTo) } : {}),
              },
            }
          : {}),
      },
    },
    include: {
      user: {
        select: {
          email: true,
          fullName: true,
          createdAt: true,
          displayName: true,
          isActive: true,
        },
      },
    },
  });

  const records = talents.map((talent) =>
    new BaseRecord(talent, context.resource).toJSON(context.currentAdmin)
  );

  return {
    meta: {
      total: records.length,
      perPage,
      page,
      direction: 'asc',
    },
    records,
  };
};
