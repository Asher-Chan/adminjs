import { ActionResponse } from 'adminjs';

// When editing
export const removePasswordFromPayload = async (
  originalResponse: ActionResponse
): Promise<ActionResponse> => {
  const { record, ...response } = originalResponse;

  return {
    ...response,
    record: {
      ...record,
      params: {
        ...record.params,
        password: '',
      },
    },
  };
};
