import { NextFunction, Request, Response } from 'express';
import { UserRoles } from '@prisma/client';
import bcrypt from 'bcrypt';

import prisma from '../prisma-client';

/**
 * Required auth function to be passed into adminjs
 */
export const adminjsAuthentication = async (
  email: string,
  password: string
) => {
  const user = await prisma.user.findFirst({
    where: {
      email,
      role: UserRoles.ADMIN,
    },
  });
  if (user) {
    const matchedUser = await bcrypt.compare(password, user.password);
    if (matchedUser) {
      await prisma.user.update({
        where: {
          email,
        },
        data: {
          lastLoginAt: new Date(),
        },
      });

      return {
        ...user,
      };
    }
  }
  return false;
};

// If req.fields have email means user has already had access
export const checkAccess = (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  const { query, fields } = req;

  // User with no access, fields returns {}, user with access but does not fill in form returns { email: '', password: '' }
  if ('email' in fields || 'password' in fields) {
    req.query = {
      ...query,
      whiteroom: '',
    };
  }

  next();
};

// Check the login path includes whiteroom, eg: /admin/login?whiteroom
export const checkLoginRouteFlag = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { query } = req;

  if (!('whiteroom' in query)) {
    res.status(403).send('Not authorised');
    return;
  }

  next();
};
