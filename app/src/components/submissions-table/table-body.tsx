import React from "react";
import { TableBodyPropGetter, TableBodyProps, Row } from "react-table";
import { TableBody as MuiTableBody, TableRow, TableCell } from "@mui/material";

import styles from "./index.module.scss";

type Props = {
  getTableBodyProps: (
    propGetter?: TableBodyPropGetter<Object> | undefined
  ) => TableBodyProps;
  page: Row<{}>[];
  prepareRow: (row: Row<Object>) => void;
};

const TableBody: React.FC<Props> = ({
  getTableBodyProps,
  page,
  prepareRow,
}) => {
  return (
    <MuiTableBody {...getTableBodyProps()}>
      {page.map((row) => {
        prepareRow(row);
        const { key, ...rowProps } = row.getRowProps();

        return (
          <TableRow hover key={key} {...rowProps}>
            {row.cells.map((cell) => {
              const { key, ...cellProps } = cell.getCellProps();

              return (
                <TableCell key={key} {...cellProps}>
                  {cell.render("Cell")}
                </TableCell>
              );
            })}
          </TableRow>
        );
      })}
    </MuiTableBody>
  );
};

export default TableBody;
