import React from "react";
import { Typography } from "@mui/material";

type Props = {
  hoursTaken: number;
  estTimeStart: number;
  estTimeEnd: number;
};

const DurationTakenCell: React.FC<Props> = ({
  hoursTaken,
  estTimeStart,
  estTimeEnd,
}) => {
  let timeComparison = "-";

  if (hoursTaken > estTimeEnd) {
    timeComparison = ">";
  }

  if (hoursTaken < estTimeEnd && hoursTaken >= estTimeStart) {
    timeComparison = "✅";
  }

  if (hoursTaken < estTimeStart) {
    timeComparison = "<";
  }

  return (
    <Typography
      variant="h5"
      noWrap
    >{`${hoursTaken} hours ${timeComparison}`}</Typography>
  );
};

export default DurationTakenCell;
