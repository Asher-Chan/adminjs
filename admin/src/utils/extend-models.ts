import { PrismaClient } from '@prisma/client';

export const Users = (prisma: PrismaClient['user']) => {
  return Object.assign(prisma, {
    /**
     * Get users full-name, example method only
     */
    async getFullName(userId: number): Promise<string> {
      const user = await prisma.findUnique({ where: { id: userId } });

      return user?.fullName || '';
    },
  });
};
