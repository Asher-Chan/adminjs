import React, { ReactElement } from "react";
import { CertificateStatus, ModuleProgressStatus } from "@prisma/client";
import { Typography } from "@mui/material";

import { requireAuthentication } from "@/utils/auth";
import DashboardLayout from "@/layouts/dashboard-layout";
import { NextPageWithLayout, PageProps } from "@/types/index";
import SubmissionsTable from "@/components/submissions-table";
import { prisma } from "@/utils/db";
import TalentCell from "@/components/submissions-table/cells/talent-cell";
import DurationTakenCell from "@/components/submissions-table/cells/duration-taken-cell";
import ActionsCell from "@/components/submissions-table/cells/actions-cell";
import CertificateCell from "@/components/submissions-table/cells/certificate-cell";
import ModuleCell from "@/components/submissions-table/cells/module-cell";

const TABLE_PAGE_SIZE = 2;

type Props = PageProps<{
  moduleProgressions: {
    talent: {
      talentName: string;
      profileImgURL?: string;
      lastUpdatedModule: string;
    };
    module: {
      moduleTitle: string;
      completedDate?: string;
      currentTaskNumber?: number;
    };
    dateEnrolled: string;
    durationTaken: {
      hoursTaken: number;
      estTimeStart: number;
      estTimeEnd: number;
    };
    actions: {
      moduleProgressId: number;
      talentId: number;
    };
    certificate: {
      status: CertificateStatus;
    };
  }[];
  currentPage?: number;
  pageCount: number;
  selectedFilterValue?: string;
}>;

const Submissions: NextPageWithLayout<Props> = (props) => {
  if (props.error) {
    return <>An error occured</>;
  }

  const { moduleProgressions, pageCount, currentPage, selectedFilterValue } =
    props;

  const sortedData = moduleProgressions.map((mp) => {
    const { lastUpdatedModule, ...talent } = mp.talent;
    const { completedDate, ...module } = mp.module;

    return {
      talent: (
        <TalentCell
          {...talent}
          lastUpdatedModule={new Date(lastUpdatedModule)}
        />
      ),
      module: (
        <ModuleCell
          {...module}
          completedDate={completedDate ? new Date(completedDate) : undefined}
        />
      ),
      dateEnrolled: (
        <Typography variant="h5" noWrap>
          {new Date(mp.dateEnrolled).toDateString()}
        </Typography>
      ),
      durationTaken: <DurationTakenCell {...mp.durationTaken} />,
      actions: <ActionsCell {...mp.actions} />,
      certificate: <CertificateCell {...mp.certificate} />,
    };
  });

  return (
    <div>
      <SubmissionsTable
        data={sortedData}
        fetchData={() => ({})}
        isLoading={false}
        pageCount={pageCount}
        pageSize={TABLE_PAGE_SIZE}
        currentPage={currentPage}
        selectedFilterValue={selectedFilterValue}
      />
    </div>
  );
};

export default Submissions;

export const getServerSideProps = requireAuthentication<Props>(async (ctx) => {
  const pageNumber = ctx.query.pageNumber as string;
  const certStatusFilter = ctx.query.certStatus as CertificateStatus;
  const moduleStatusFilter = ctx.query.moduleStatus as ModuleProgressStatus;
  const recentlyUpdatedFilter = ctx.query.recentlyUpdated as string;
  const searchFilter = ctx.query.searchString as string;

  const hasFilters =
    !!certStatusFilter ||
    !!moduleStatusFilter ||
    !!recentlyUpdatedFilter ||
    !!searchFilter;

  try {
    const moduleProgressions = await prisma.moduleProgress.findMany({
      ...(hasFilters
        ? {
            where: {
              ...(certStatusFilter
                ? { certificateStatus: certStatusFilter }
                : {}),
              ...(moduleStatusFilter ? { status: moduleStatusFilter } : {}),
              ...(searchFilter
                ? {
                    OR: [
                      {
                        module: {
                          title: {
                            contains: searchFilter,
                          },
                        },
                      },
                      {
                        talent: {
                          user: {
                            fullName: {
                              contains: searchFilter,
                            },
                          },
                        },
                      },
                    ],
                  }
                : {}),
            },
          }
        : {}),
      include: {
        talent: {
          include: {
            user: true,
          },
        },
        module: {
          include: {
            tasks: true,
          },
        },
        submissions: true,
      },
      take: TABLE_PAGE_SIZE,
      ...(pageNumber
        ? { skip: TABLE_PAGE_SIZE * (parseInt(pageNumber) - 1) }
        : {}),
    });

    const totalModuleProgressions = await prisma.moduleProgress.count(
      hasFilters
        ? {
            where: {
              ...(certStatusFilter
                ? { certificateStatus: certStatusFilter }
                : {}),
              ...(moduleStatusFilter ? { status: moduleStatusFilter } : {}),
              ...(searchFilter
                ? {
                    OR: [
                      {
                        module: {
                          title: {
                            contains: searchFilter,
                          },
                        },
                      },
                      {
                        talent: {
                          user: {
                            fullName: {
                              contains: searchFilter,
                            },
                          },
                        },
                      },
                    ],
                  }
                : {}),
            },
          }
        : undefined
    );

    console.log(totalModuleProgressions);

    const sortedData = moduleProgressions.map((mp) => {
      const { updatedAt, id, certificateStatus, createdAt, dateCompleted } = mp;
      const { estCompletionTimeEnd, estCompletionTimeStart, title, tasks } =
        mp.module;
      const { user, id: talentId } = mp.talent;
      const { fullName, profileImgURL } = user;
      const submissions = mp.submissions;

      const hoursTaken = submissions.reduce(
        (a, b) => a + (b?.durationTaken || 0),
        0
      );

      // This is assuming that tasks can only be completed in order
      const getCurrentTaskNumber = (): number => {
        const totalSubmissions = submissions.length;
        const totalTasks = tasks.length;

        return totalTasks - totalSubmissions || 0;
      };
      // TODO: Should add middleware to check that if talent has submitted task in modules, update the moduleProgress lastUpdated field

      return {
        talent: {
          talentName: fullName,
          ...(profileImgURL ? { profileImgURL } : {}),
          lastUpdatedModule: updatedAt,
        },
        actions: {
          moduleProgressId: id,
          talentId,
        },
        certificate: {
          status: certificateStatus,
        },
        dateEnrolled: createdAt,
        durationTaken: {
          hoursTaken,
          estTimeEnd: estCompletionTimeEnd,
          estTimeStart: estCompletionTimeStart,
        },
        module: {
          moduleTitle: title,
          ...(dateCompleted
            ? { completedDate: dateCompleted }
            : { currentTaskNumber: getCurrentTaskNumber() }),
        },
      };
    });

    return {
      props: {
        error: false,
        pageCount: Math.ceil(totalModuleProgressions / TABLE_PAGE_SIZE) || 1,
        selectedFilterValue: moduleStatusFilter || certStatusFilter || "",
        moduleProgressions: JSON.parse(JSON.stringify(sortedData)),
        ...(pageNumber ? { currentPage: parseInt(pageNumber) } : {}),
      },
    };
  } catch (err) {
    console.log(err);
    return {
      props: {
        error: true,
        errorMessage: "An error occured",
      },
    };
  }
});

Submissions.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
