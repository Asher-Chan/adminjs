import React, { ReactElement } from "react";

import { requireAuthentication } from "@/utils/auth";
import { NextPageWithLayout } from "@/types/index";
import DashboardLayout from "@/layouts/dashboard-layout";

type Props = {};

const AvailableModules: NextPageWithLayout<Props> = ({}) => {
  return <div>Dashboard - Available Modules</div>;

  // This is essentially the same as public page / available modules,
  // will probs just make into component <AvailableComponent /> since it uses client-side fetching anyway
  // And use in both places
  // Only difference is that when using in talent-dashboard they have on extra `status` search field
};

export default AvailableModules;

export const getServerSideProps = requireAuthentication<Props>(async (ctx) => {
  return {
    props: {},
  };
});

AvailableModules.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
