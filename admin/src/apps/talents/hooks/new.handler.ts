import {
  ActionContext,
  ActionRequest,
  ActionResponse,
  BaseRecord,
} from 'adminjs';
import { UserRoles } from '@prisma/client';

import { User } from '../../../models';
import { SortedTalentPayload } from '../../../types/forms';
import { uploadFile } from '../../../utils/cloudinary';
import { resourceIds } from '../../../constants/resource-ids';

export const handleNewTalent = async (
  req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
): Promise<ActionResponse> => {
  const { attachedUser, address, ...talent } =
    req.payload as SortedTalentPayload;

  const newUser = await User.create({
    data: {
      ...attachedUser,
      ...(context.uploadedProfilePhoto
        ? {
            profileImgURL: await uploadFile({
              filePath: context.uploadedProfilePhoto,
            }),
          }
        : {}),
      role: UserRoles.TALENT,
      isVerified: true,

      // Address
      address: { create: { ...address } },

      // Talent
      talent: { create: { ...talent } },
    },
    include: {
      talent: true,
    },
  });

  const record = new BaseRecord(newUser, context.resource).toJSON(
    context.currentAdmin
  );

  return {
    record,
    redirectUrl: context.h.showUrl(
      resourceIds.TALENTS,
      newUser.talent.id.toString()
    ),
  };
};
