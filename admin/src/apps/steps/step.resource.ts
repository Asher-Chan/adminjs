import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import {
  MultiImageInput,
  MultiImageView,
  ParentRecord,
  Resources,
  ResourcesInput,
  StepsList,
} from '../../admin/components.bundler';
import prisma from '../../prisma-client';

import { handleResources } from './hooks/new.handler';
import {
  populateStepsResourceData,
  populateStepsData,
} from './hooks/show.after';
import { validateData } from './hooks/validate.before';
import { handleMediaBeforeSave } from './hooks/handle-media.before';

const options: ResourceOptions = {
  id: resourceIds.STEPS,
  navigation: navigation.STEPS,
  actions: {
    list: {
      isAccessible: false,
      isVisible: false,
    },
    edit: {
      before: [validateData, handleMediaBeforeSave],
      handler: handleResources,
    },
    new: {
      before: [validateData, handleMediaBeforeSave],
      handler: handleResources,
    },
    show: {
      icon: 'Account',
      after: [populateStepsData, populateStepsResourceData],
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    title: {
      props: {
        placeholder: 'Ex. Get instructions on your task',
      },
    },
    desc: {
      type: 'richtext',
      custom: {
        modules: {
          // Edits the quill editor toolbar
          toolbar: {},
        },
      },
    },
    uploadedMediaFiles: {
      isArray: true,
      custom: {
        fieldName: 'mediaURLs',
      },
      components: {
        edit: MultiImageInput,
      },
      isVisible: {
        edit: true,
      },
    },
    mediaURLs: {
      isVisible: {
        show: true,
      },
      components: {
        show: MultiImageView,
      },
    },
    resources: {
      position: 110,
      type: 'string',
      isVisible: {
        edit: true,
        show: true,
      },
      components: {
        edit: ResourcesInput,
        show: Resources,
      },
    },
    task: {
      type: 'reference',
      position: 100,
      reference: resourceIds.TASKS,
      isVisible: {
        edit: true,
        show: true,
      },
      isRequired: true,
      props: {
        searchParam: 'taskId',
        fieldName: 'task',
      },
      components: {
        edit: ParentRecord,
      },
    },
    // Other steps in this task
    steps: {
      position: 111,
      isVisible: {
        show: true,
      },
      props: {
        showAddButton: false,
        label: 'Other Steps in this Task',
      },
      components: {
        show: StepsList,
      },
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.Step },
  options,
};
