import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';
import styled from 'styled-components';

const Tag = styled.div`
  font-size: 0.8rem;
  border-radius: 6px;
  background-color: lightgray;
  padding: 0.4rem 0.8rem;
  margin-right: 0.4rem;
`;

const Tags: React.FC<ShowPropertyProps> = ({ property, record }) => {
  const { tags } = record.params;

  return (
    <Box mb={36}>
      <Label uppercase variant="light">
        {property.label}
      </Label>
      <Box flex>
        {tags?.map((tag) => (
          <Tag key={tag}>{tag}</Tag>
        ))}
      </Box>
    </Box>
  );
};

export default Tags;
