import React, { useCallback, useEffect, useRef, useState } from "react";
import { useTable, usePagination, Column } from "react-table";
import { useRouter } from "next/router";
import {
  Card,
  CardActions,
  CardHeader,
  Pagination,
  Button as MuiButton,
  Divider,
  TableContainer,
  Table,
  Menu,
  MenuItem,
  Box,
  InputAdornment,
  styled,
  TextField,
  Grid,
} from "@mui/material";
import ExpandMoreTwoToneIcon from "@mui/icons-material/ExpandMoreTwoTone";
import SearchTwoToneIcon from "@mui/icons-material/SearchTwoTone";

import Button from "@/components/button";

import TableHeader from "./table-header";
import TableBody from "./table-body";
import { CertificateStatus, ModuleProgressStatus } from "@prisma/client";

type Props = {
  data: Object[];
  pageCount: number;
  currentPage?: number;
  isLoading: boolean;
  fetchData: () => {};
  pageSize: number;
  selectedFilterValue?: string;
};

const SearchInputWrapper = styled(TextField)(
  ({ theme }) => `
    background: ${theme.colors.alpha.white[100]};
    border-radius: ${theme.general.borderRadius};

    .MuiInputBase-input {
        font-size: ${theme.typography.pxToRem(16)};
    }
  `
);

const SubmissionsTable: React.FC<Props> = ({
  data,
  pageCount: controlledPageCount,
  isLoading,
  currentPage,
  pageSize: controlledPageSize,
  selectedFilterValue,
}) => {
  const router = useRouter();
  const columns = React.useMemo(
    () => [
      {
        Header: "Talent",
        accessor: "talent",
      },
      {
        Header: "Module",
        accessor: "module",
      },
      {
        Header: "Date Enrolled",
        accessor: "dateEnrolled",
      },
      {
        Header: "Duration Taken",
        accessor: "durationTaken",
      },
      {
        Header: "Actions",
        accessor: "actions",
      },
      {
        Header: "Certificate",
        accessor: "certificate",
      },
    ],
    []
  ) as Column<Object>[];

  const actionRef1 = useRef<any>(null);
  const [openPeriod, setOpenMenuPeriod] = useState<boolean>(false);

  const [filtersDirty, setFiltersDirty] = useState<boolean>(false);
  const [searchString, setSearchString] = useState<string>("");

  const filterOptions = [
    {
      searchKey: "recentlyUpdated",
      value: "recentlyUpdated",
      text: "Recently Updated",
    },
    {
      searchKey: "moduleStatus",
      value: ModuleProgressStatus.COMPLETED,
      text: "Completed",
    },
    {
      searchKey: "moduleStatus",
      value: ModuleProgressStatus.STARTED,
      text: "In Progress",
    },
    {
      searchKey: "recentTalents",
      value: "recentTalents",
      text: "Recent Talents",
    },
    {
      searchKey: "certStatus",
      value: CertificateStatus.AWARDED,
      text: "Awarded Certs",
    },
    {
      searchKey: "certStatus",
      value: CertificateStatus.PENDING,
      text: "Pending Certs",
    },
    {
      searchKey: "certStatus",
      value: CertificateStatus.READY_TO_RECIEVE,
      text: "Award Now",
    },
  ];

  const [selectedFilter, setSelectedFilter] = useState(
    filterOptions.find((option) => option.value === selectedFilterValue) ||
      filterOptions[0]
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageCount,
    gotoPage,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageIndex: currentPage ? currentPage - 1 : 0,
        pageSize: controlledPageSize,
      },
      manualPagination: true,
      pageCount: controlledPageCount,
    },
    usePagination
  );

  const fetchData = useCallback(() => {
    const queryStrings = [];

    let pageNumber = pageIndex + 1;

    if (selectedFilter) {
      queryStrings.push(`${selectedFilter.searchKey}=${selectedFilter.value}`);
    }

    if (searchString) {
      queryStrings.push(`searchString=${searchString}`);
    }

    console.log(searchString);

    if (filtersDirty) {
      pageNumber = 1;
    }

    queryStrings.push(`pageNumber=${pageNumber}`);

    router.push(`?${queryStrings.join("&")}`);
  }, [pageIndex, selectedFilter, filtersDirty, searchString]);

  useEffect(() => {
    fetchData();
    setFiltersDirty(false);
  }, [fetchData, selectedFilter, pageIndex]);

  return (
    <>
      <Box pb={3}>
        <Grid container>
          <Grid item xs={11}>
            {/* Not using button, instead queries on each character typed */}
            <SearchInputWrapper
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchTwoToneIcon />
                  </InputAdornment>
                ),
              }}
              value={searchString}
              onChange={(e) => {
                setSearchString(e.target.value);
              }}
              placeholder="Search keywords here"
              fullWidth
            />
          </Grid>
          <Grid item xs={1}>
            <Button label="Search" onClickHandler={() => fetchData()} />
          </Grid>
        </Grid>
      </Box>
      <Card>
        <CardHeader
          title="Results"
          action={
            <>
              <MuiButton
                size="small"
                variant="outlined"
                ref={actionRef1}
                onClick={() => setOpenMenuPeriod(true)}
                endIcon={<ExpandMoreTwoToneIcon fontSize="small" />}
              >
                {selectedFilter.text}
              </MuiButton>
              <Menu
                disableScrollLock
                anchorEl={actionRef1.current}
                onClose={() => setOpenMenuPeriod(false)}
                open={openPeriod}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
              >
                {filterOptions.map((_period) => (
                  <MenuItem
                    key={_period.value}
                    onClick={() => {
                      setSelectedFilter(_period);
                      setOpenMenuPeriod(false);
                      if (!filtersDirty) {
                        setFiltersDirty(true);
                      }
                    }}
                  >
                    {_period.text}
                  </MenuItem>
                ))}
              </Menu>
              {/* Essentialy a clear filter button i suppose */}
              <Button label="View All" variant="primary" />
            </>
          }
        />
        <Divider />
        <TableContainer>
          <Table {...getTableProps()}>
            <TableHeader headerGroups={headerGroups} />
            <TableBody
              getTableBodyProps={getTableBodyProps}
              page={page}
              prepareRow={prepareRow}
            />
          </Table>
        </TableContainer>

        <CardActions
          disableSpacing
          sx={{
            p: 2,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Pagination
            shape="rounded"
            count={pageCount}
            color="primary"
            page={pageIndex + 1}
            onChange={(_e, value) => {
              gotoPage(value - 1);
            }}
          />
        </CardActions>
      </Card>
    </>
  );
};

export default SubmissionsTable;
