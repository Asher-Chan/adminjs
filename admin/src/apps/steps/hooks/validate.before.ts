import { ActionRequest, ValidationError } from 'adminjs';

import { StepFormValues } from '../../../types/forms';

export const validateData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { desc } = req.payload as StepFormValues;

    const errors: { [key: string]: { message: string } } = {};

    if (!desc) {
      errors.desc = {
        message: 'A description is required',
      };
    }

    const numOfErrors = Object.keys(errors).length;

    if (numOfErrors) {
      throw new ValidationError(
        {
          ...errors,
        },
        {
          message: 'Some field are missing or invalid.',
        }
      );
    }
  }

  return req;
};
