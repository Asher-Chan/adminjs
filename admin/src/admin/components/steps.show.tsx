import React from 'react';
import { Box, H4, Button, Icon, Link } from '@adminjs/design-system';
import { BaseRecord, ShowPropertyProps } from 'adminjs';
import styled from 'styled-components';

import { resourceIds } from '../../constants/resource-ids';

const Step = styled(Box)`
  background: #8172e0;
  border-radius: 6px;
  padding: 1.2rem;
  color: #fff;
  margin-bottom: 0.8rem;

  &:hover {
    background: #9d93dc;
    cursor: pointer;
  }
`;

const Steps: React.FC<ShowPropertyProps> = ({ record, property, resource }) => {
  const records = record.populated.steps as unknown as BaseRecord[];

  const currentStepId =
    resource.name === resourceIds.TASKS ? '' : record.params.id;

  const filterdRecords =
    records?.flatMap((item) => {
      if (item.params.id === currentStepId) {
        return [];
      }

      return item;
    }) || [];

  return (
    <Box>
      <Box flex flexDirection="row">
        <Box flexGrow={1}>
          <H4>{property.props.label || 'Steps'}</H4>
        </Box>
        {!!property.props.showAddButton && (
          <Box flexShrink={0}>
            <Link href={record.params.addNewLink}>
              <Button flex variant="light">
                <Icon icon="Add" />
                Add Step
              </Button>
            </Link>
          </Box>
        )}
      </Box>

      {records?.length ? (
        // Not mapping `filteredRecord` becuase need to correct order and step number
        records.flatMap((item, idx) => {
          if (item.params.id === currentStepId) {
            return [];
          }

          return (
            <Link key={item.params.id} href={item.params.itemLink}>
              <Step>
                {idx + 1}. {item.params.title}
              </Step>
            </Link>
          );
        })
      ) : (
        <div>No steps have been added yet.</div>
      )}
      {records?.length === 1 && records?.length !== filterdRecords.length && (
        <div>No other steps in this task.</div>
      )}
    </Box>
  );
};

export default Steps;
