import { Prisma } from '@prisma/client';

const modelsToSoftDelete = ['Company', 'Talent'];

// Records are soft deleted by we must handle not showing soft deleted records ourselves
export const softDelete: Prisma.Middleware = async (params, next) => {
  const updatedParams = params;
  if (modelsToSoftDelete.includes(params.model)) {
    if (params.action === 'delete') {
      updatedParams.action = 'update';
      updatedParams.args.data = { isDeleted: true, deletedAt: new Date() };
    }

    if (params.action === 'deleteMany') {
      updatedParams.action = 'updateMany';
      if (params.args.data !== undefined) {
        updatedParams.args.data.isDeleted = true;
        updatedParams.args.data.deletedAt = new Date();
      } else {
        updatedParams.args.data = { isDeleted: true, deletedAt: new Date() };
      }
    }
  }

  return next(updatedParams);
};
