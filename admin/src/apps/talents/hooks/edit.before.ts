import { ActionContext, ActionRequest } from 'adminjs';

import { User } from '../../../models';
import { uploadFile } from '../../../utils/cloudinary';

import { SortedTalentPayload } from '../../../types/forms';

export const handleBeforeSave = async (
  req: ActionRequest,
  context: ActionContext
) => {
  if (req.method === 'post') {
    const { attachedUser, address, ...company } =
      req.payload as SortedTalentPayload;

    await User.update({
      where: {
        id: parseInt(company.userId),
      },
      data: {
        ...attachedUser,
        ...(context.uploadedProfilePhoto
          ? {
              profileImgURL: await uploadFile({
                filePath: context.uploadedProfilePhoto,
              }),
            }
          : {}),

        // Address
        address: { upsert: { create: { ...address }, update: { ...address } } },
      },
    });
  }

  return req;
};
