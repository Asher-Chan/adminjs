import { AdminJSOptions } from 'adminjs';
import { resourceIds } from '../constants/resource-ids';

const getDefaultActionTranslations = (
  name: string,
  pluralName: string
): {
  new: string;
  edit: string;
  list: string;
  show: string;
} => ({
  new: `Add ${name}`,
  edit: `Edit ${name}`,
  list: pluralName,
  show: name,
});

const locale: AdminJSOptions['locale'] = {
  language: 'en',
  translations: {
    labels: {
      loginWelcome: '',
      certificateGeneratorPage: 'Cert Generator',
    },
    messages: {
      loginWelcome: '',
    },
    resources: {
      // Must be a key of resource names
      [resourceIds.COMPANIES]: {
        actions: {
          ...getDefaultActionTranslations('Company', 'Companies'),
        },
        properties: {
          fullName: 'Company Full Name',
          uploadedLogoFile: 'Company Logo',
          uploadedProfilePhoto: 'Profile Photo',
          uploadedSignatureFile: 'Signature to show on Certificates',
          signatureName: 'Signature name to show on Certificates',
          signatureTitle: 'Signature title to show on Certificates',
        },
      },

      [resourceIds.TALENTS]: {
        actions: {
          ...getDefaultActionTranslations('Talent', 'Talents'),
        },
      },

      [resourceIds.CERTIFICATES]: {
        actions: {
          ...getDefaultActionTranslations('Certificate', 'Certificates'),
        },
      },

      [resourceIds.MODULES]: {
        actions: {
          ...getDefaultActionTranslations('Module', 'Modules'),
        },
        properties: {
          title: 'Module Title',
          estCompletionTime: 'Estimated Completion Time',
          uploadedOpeningImage: 'Upload opening image that depicts module',
          uploadedIntroVideo: 'Upload introduction to module video (optional)',
          desc: 'Why should talents join this module?',
        },
      },

      [resourceIds.MODULE_PROGRESS]: {
        actions: {
          ...getDefaultActionTranslations('Submissions', 'Talent Submissions'),
        },
        properties: {
          createdAt: 'Date Enrolled',
        },
      },

      [resourceIds.TASKS]: {
        actions: {
          ...getDefaultActionTranslations('Task', 'Tasks'),
        },
        properties: {
          shorthandTitle:
            "Short Task Title / Shorthand (Appears On Talent's Progress Counter)",
          tags: 'What will the Talent learn from this module? (Up to 8 tags)',
          desc: 'Task Description',
          summary: 'Full Task Summary',
        },
      },

      [resourceIds.STEPS]: {
        actions: {
          ...getDefaultActionTranslations('Step', 'Steps'),
        },
        properties: {
          title: 'Step',
          desc: 'Description',
          mediaURLs: 'Media',
          uploadedMediaFiles: 'Media',
        },
      },

      [resourceIds.USERS]: {
        actions: {
          ...getDefaultActionTranslations('Admin', 'Admins'),
        },
      },
    },
  },
};

export { locale };
