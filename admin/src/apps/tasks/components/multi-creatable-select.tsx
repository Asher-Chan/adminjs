import React, { useState, useEffect } from 'react';
import { EditPropertyProps, ApiClient } from 'adminjs';
import { StylesConfig, OnChangeValue } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import { Box, Label } from '@adminjs/design-system';

type Props = {
  actionName?: string;
  resourceName?: string;
};

const selectStlyes: StylesConfig = {
  control: (provided) => ({
    ...provided,
    borderRadius: 0,
  }),
};

const MultiCreatableSelect: React.FC<EditPropertyProps> = ({
  onChange,
  property,
  record,
}) => {
  const [tagOptions, setTagOptions] = useState([]);
  const [defaultValues, setDefaultValues] = useState(null);

  const props = property.props as Props;

  const api = new ApiClient();

  const fetchRecords = async () => {
    if (!props.actionName || !props.resourceName) {
      return;
    }

    const result = await api.resourceAction({
      actionName: props.actionName,
      resourceId: props.resourceName,
    });

    const options = result.data.map((option) => {
      // We create the action to fetch this but the format of data retuned is determined by adminjs
      return {
        value: option.id,
        label: option.title,
      };
    });

    setTagOptions(options);
  };

  const fetchDefaultTags = async () => {
    const result = await api.resourceAction({
      actionName: 'fetchDefaultTags',
      resourceId: props.resourceName,
      data: {
        recordId: record.id,
      },
    });

    const defaultTags = result.data.map((option) => {
      return {
        value: option.id,
        label: option.title,
      };
    });

    setDefaultValues(defaultTags);
  };

  const handleChange = (newValues: OnChangeValue<{}, true>) => {
    if (newValues.length > 8) {
      return;
    }
    onChange(property.propertyPath, newValues);
    onChange('tagsCount', newValues?.length);
  };

  useEffect(() => {
    fetchRecords();
    fetchDefaultTags();
  }, []);

  return (
    <Box mb={36}>
      <Label required={property.isRequired}>{property.label}</Label>
      {defaultValues ? (
        <CreatableSelect
          styles={selectStlyes}
          defaultValue={defaultValues}
          isMulti
          options={tagOptions}
          onChange={handleChange}
          isOptionDisabled={(_inputValue, values) => {
            if (values.length >= 8) {
              return true;
            }
            return false;
          }}
        />
      ) : null}
    </Box>
  );
};

export default MultiCreatableSelect;
