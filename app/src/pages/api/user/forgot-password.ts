import jwt from "jsonwebtoken";

import { NextAPIRes, NextAPIReq } from "@/types/index";
import { prisma } from "@/utils/db";
import { sendResetLinkEmail } from "@/utils/send-mail";

export default async function handler(
  req: NextAPIReq<{}, { email: string }>,
  res: NextAPIRes
) {
  const { email } = req.body;

  if (!email) {
    res
      .status(400)
      .json({ error: true, errorMessage: "Email was not provided" });
    return;
  }

  try {
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (user) {
      const payload = {
        id: user.id,
        email: user.email,
      };
      const secret = user.password + "-" + user.createdAt; // Unique secret
      const token = jwt.sign(payload, secret, { expiresIn: "60m" }); // Change expiring duration
      const resetLink = `http://localhost:3000/auth/reset-password?id=${payload.id}&token=${token}`;
      await sendResetLinkEmail(resetLink);
    }

    res.status(200).json({ error: false });
    return;
  } catch (err) {
    res
      .status(400)
      .json({ error: true, errorMessage: "Error sending reset-password link" });
    return;
  }
}
