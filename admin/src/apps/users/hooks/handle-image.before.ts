import { ActionRequest } from 'adminjs';

import { uploadFile } from '../../../utils/cloudinary';

export const handleImageUpload = async (req: ActionRequest) => {
  if (req.method === 'post') {
    if (req.payload.uploadedProfileImg?.path) {
      req.payload.profileImgURL = await uploadFile({
        filePath: req.payload.uploadedProfileImg?.path,
      });
    }
  }

  return req;
};
