import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';

const BrandColorShow: React.FC<ShowPropertyProps> = ({ record, property }) => {
  const { brandColor } = record.params;

  return (
    <Box mb={24}>
      <Label uppercase variant="light">
        {property.label}
      </Label>
      <div
        style={{
          backgroundColor: brandColor,
          width: '60px',
          height: '30px',
          borderRadius: '10px',
        }}
      />
    </Box>
  );
};

export default BrandColorShow;
