import { ActionContext, ActionRequest, ValidationError } from 'adminjs';

import { User } from '../../../models';
import { uploadFile } from '../../../utils/cloudinary';

import { SortedCompanyPayload } from '../../../types/forms';

export const handleBeforeSave = async (
  req: ActionRequest,
  context: ActionContext
) => {
  if (req.method === 'post') {
    const { attachedUser, address, ...company } =
      req.payload as SortedCompanyPayload;

    if (!req.payload.briefIntro) {
      throw new ValidationError(
        {
          briefIntro: {
            message: 'Brief intro is required',
          },
        },
        {
          message: 'Some field are missing',
        }
      );
    }

    if (context.uploadedLogoFile) {
      req.payload.logoURL = await uploadFile({
        filePath: context.uploadedLogoFile,
      });
    }

    if (context.uploadedSignatureFile) {
      req.payload.signatureFileURL = await uploadFile({
        filePath: context.uploadedSignatureFile,
      });
    }

    await User.update({
      where: {
        id: parseInt(company.userId),
      },
      data: {
        ...attachedUser,
        ...(context.uploadedProfilePhoto
          ? {
              profileImgURL: await uploadFile({
                filePath: context.uploadedProfilePhoto,
              }),
            }
          : {}),

        // Address
        address: { upsert: { create: { ...address }, update: { ...address } } },
      },
    });
  }

  return req;
};
