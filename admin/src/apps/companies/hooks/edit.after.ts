import { ActionResponse, ActionRequest, ActionContext } from 'adminjs';
import { resourceIds } from '../../../constants/resource-ids';

// This is repeated alot, extract into a function
export const redirectAfterSave = async (
  originalResponse: ActionResponse,
  _req: ActionRequest,
  context: ActionContext
) => {
  const { record } = originalResponse;
  const redirectUrl = context.h.showUrl(
    resourceIds.COMPANIES,
    record.params.id
  );

  return {
    ...originalResponse,
    redirectUrl,
  };
};
