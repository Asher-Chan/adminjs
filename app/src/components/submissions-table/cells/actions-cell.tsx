import React from "react";

import Button from "@/components/button";

import styles from "../index.module.scss";

type Props = {
  moduleProgressId: number;
  talentId: number;
};

const ActionsCell: React.FC<Props> = ({ moduleProgressId, talentId }) => {
  return (
    <div className={styles.actionsCell}>
      <Button
        label="View Submissions"
        onClickHandler={() => ({})}
        variant="primary"
      />
      <Button
        label="View Profile"
        onClickHandler={() => ({})}
        variant="secondary"
      />
    </div>
  );
};

export default ActionsCell;
