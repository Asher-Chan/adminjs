import 'dotenv/config';
import path from 'path';
import express from 'express';
import formidableMiddleware from 'express-formidable';

import { initAdminJSRouter } from './admin/admin.router';

import { softDelete } from './middleware';
import { encryptPassword, populateUserFields } from './middleware/user';
import prisma from './prisma-client';

// Middlewares
prisma.$use(encryptPassword);
prisma.$use(softDelete);
prisma.$use(populateUserFields);

const main = async () => {
  const app = express();
  const port = process.env.PORT || 8080;

  app.use(express.static(path.join(__dirname, '/public')));
  app.use(formidableMiddleware());

  initAdminJSRouter(app, prisma);

  app.listen(port, () =>
    console.log(`🚀 Server started and running on http://localhost:${port}`)
  );
};

main()
  .catch((e) => console.log(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
