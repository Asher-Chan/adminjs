import React, { ReactElement } from "react";
import { useRouter } from "next/router";
import { Grid } from "@mui/material";

import { requireAuthentication } from "@/utils/auth";
import { NextPageWithLayout, PageProps } from "@/types/index";
import DashboardLayout from "@/layouts/dashboard-layout";
import { prisma } from "@/utils/db";
import { ModuleWithCompany, ModuleWithModuleProgress } from "@/types/modules";
import ModuleCard from "@/components/module-card";

import styles from "./index.module.scss";

type BaseProps = {
  pageTitle: string;
};

type Props = PageProps<
  | (BaseProps & {
      userType: "company";
      modules: ModuleWithCompany[];
    })
  | (BaseProps & {
      userType: "talent";
      modules: ModuleWithModuleProgress[];
    })
>;

const MyModules: NextPageWithLayout<Props> = (props) => {
  const router = useRouter();

  if (props.error) {
    return <div>Error loading the data</div>;
  }

  const { userType, modules, pageTitle } = props;

  function renderModuleCards() {
    if (userType === "talent") {
      return modules.map((m) => {
        return (
          <ModuleCard
            key={m.id}
            moduleId={m.id}
            companyBrandColor={m.company?.brandColor}
            companyName={m.company?.user?.fullName || ""}
            moduleDurationEnd={m.estCompletionTimeEnd}
            moduleDurationStart={m.estCompletionTimeStart}
            moduleTitle={m.title}
            moduleActions={
              m.moduleProgress.status === "COMPLETED"
                ? [
                    {
                      action: () =>
                        router.push(`/modules/progress/${m.moduleProgress.id}`),
                      actionName: "View Completed Module",
                    },
                  ]
                : [
                    {
                      action: (id) => router.push(`/modules/overview/${id}`),
                      actionName: "Resume Module",
                    },
                  ]
            }
            progressStatus={m.moduleProgress.status}
          />
        );
      });
    }

    return modules.map((m) => {
      return (
        <ModuleCard
          key={m.id}
          moduleId={m.id}
          companyBrandColor={m.company?.brandColor}
          companyName={m.company?.user?.fullName || ""}
          moduleDurationEnd={m.estCompletionTimeEnd}
          moduleDurationStart={m.estCompletionTimeStart}
          moduleTitle={m.title}
        />
      );
    });
  }

  return (
    <div>
      <div>Dashboard - {pageTitle}</div>
      <div className={styles.moduleCardsContainer}>
        <Grid
          sx={{ px: 4 }}
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          {modules.length ? renderModuleCards() : null}
        </Grid>
      </div>
    </div>
  );
};

export default MyModules;

export const getServerSideProps = requireAuthentication<Props>(
  async (_ctx, session) => {
    const { auth } = session;

    const isCompany = auth.role === "COMPANY";

    try {
      if (isCompany) {
        const modules = await prisma.module.findMany({
          where: {
            company: {
              userId: auth.id,
            },
          },
          include: {
            company: {
              include: {
                user: true,
              },
            },
          },
        });

        return {
          props: {
            error: false,
            userType: "company",
            pageTitle: isCompany ? `${auth.fullName} Modules` : "My Modules",
            modules: JSON.parse(JSON.stringify(modules)),
          },
        };
      }

      const moduleProgressions = await prisma.moduleProgress.findMany({
        where: {
          talent: {
            userId: auth.id,
          },
        },
        include: {
          module: {
            include: {
              company: {
                include: {
                  user: true,
                },
              },
            },
          },
        },
      });

      const modules = moduleProgressions.map(({ module, ...mp }) => ({
        ...module,
        moduleProgress: mp,
      }));

      return {
        props: {
          error: false,
          userType: "talent",
          pageTitle: isCompany ? `${auth.fullName} Modules` : "My Modules",
          modules: JSON.parse(JSON.stringify(modules)),
        },
      };
    } catch (err) {
      return {
        props: {
          error: true,
          errorMessage: "Error",
        },
      };
    }
  }
);

MyModules.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
