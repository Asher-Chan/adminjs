import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import {
  ImageUpload,
  ImageView,
  MultiCreatableSelect,
  ParentRecord,
  StepsList,
  Tags,
  TasksList,
} from '../../admin/components.bundler';
import prisma from '../../prisma-client';

import { redirectAfterSave } from './hooks/edit.after';
import { populateStepsData, populateTasksData } from './hooks/show.after';
import { sortTagsData } from './hooks/sort-tags.before';
import { fetchTags, fetchDefaultTags } from './hooks/tags.action';
import { newTaskHandler } from './hooks/new.handler';
import { handleTagsBeforeSave } from './hooks/edit.before';
import { validateData } from './hooks/validate.before';
import { handleImages } from './hooks/handle-images.before';

const options: ResourceOptions = {
  id: resourceIds.TASKS,
  navigation: navigation.TASKS,
  actions: {
    list: {
      showInDrawer: false,
      hideActionHeader: true,
      isAccessible: false,
      isVisible: false,
    },
    new: {
      before: [sortTagsData, validateData, handleImages],
      handler: newTaskHandler,
    },
    edit: {
      before: [sortTagsData, validateData, handleTagsBeforeSave, handleImages],
      after: redirectAfterSave,
    },
    show: {
      icon: 'Account',
      after: [populateStepsData, populateTasksData],
    },
    fetchTags: {
      isVisible: false,
      handler: fetchTags,
    },
    fetchDefaultTags: {
      isVisible: false,
      handler: fetchDefaultTags,
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    module: {
      reference: resourceIds.MODULES,
      isVisible: {
        edit: true,
        show: true,
      },
      position: 100,
      isRequired: true,
      props: {
        searchParam: 'moduleId',
        fieldName: 'module',
      },
      components: {
        edit: ParentRecord,
      },
    },
    tags: {
      reference: resourceIds.TAGS,
      props: {
        resourceName: resourceIds.TASKS,
        actionName: 'fetchTags',
      },
      components: {
        edit: MultiCreatableSelect,
        show: Tags,
      },
    },
    moduleId: {
      isVisible: false,
      reference: resourceIds.MODULES,
    },
    uploadedOverviewImage: {
      isRequired: true,
      position: 110,
      custom: {
        fieldName: 'overviewImgURL',
      },
      components: {
        edit: ImageUpload,
      },
      isVisible: {
        edit: true,
      },
    },
    overviewImgURL: {
      isVisible: {
        show: true,
      },
      components: {
        show: ImageView,
      },
    },
    createdAt: {
      isVisible: false,
    },
    title: {
      props: {
        placeholder:
          'Ex. Welcome  & Introduction - Using Python for basic tasks',
      },
    },
    shorthandTitle: {
      isRequired: true,
      props: {
        placeholder: 'Ex. Python',
      },
    },
    desc: {
      type: 'textarea',
      isRequired: true,
      props: {
        placeholder:
          'Ex. See the reasoning behind the design of an Vitrox hosting architecture and learn how to communicate the decisions to clients, as well as inform on how they will be billed.',
        rows: 3,
      },
    },
    summary: {
      type: 'textarea',
      props: {
        placeholder:
          'Ex. What is cybersecurity risk mitigation? Cybersecurity risk mitigation involves the use of security policies and processes to reduce the overall risk or impact of a cybersecurity threat. In regard to cybersecurity, risk mitigation can be separated into three elements: prevention, detection, and remediation.',
        rows: 5,
      },
    },
    updatedAt: {
      isVisible: false,
    },
    // Seperately done or may be done here ? This is one of the fixed steps
    exampleFileURL: {
      isVisible: false,
    },
    // Steps in current task
    steps: {
      position: 111,
      isVisible: {
        show: true,
      },
      props: {
        showAddButton: true,
      },
      components: {
        show: StepsList,
      },
    },
    // Other tasks in module
    tasks: {
      position: 110,
      isVisible: {
        show: true,
      },
      props: {
        label: 'Other Tasks in this Module',
      },
      components: {
        show: TasksList,
      },
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.Task },
  options,
};
