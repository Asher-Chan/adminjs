import { ActionRequest } from 'adminjs';

export const sortTagsData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const tagsToConnect = [];
    const tagsToCreate = [];
    const { tagsCount } = req.payload;

    Array.from(Array(Number(tagsCount || 0)).keys()).forEach((_t, idx) => {
      const isNewTag = req.payload[`tags.${idx}.__isNew__`] === 'true';
      const value = req.payload[`tags.${idx}.value`];

      if (isNewTag) {
        const tag = {
          name: value,
        };
        tagsToCreate.push(tag);
      } else {
        const tag = {
          id: value,
        };
        tagsToConnect.push(tag);
      }
    });
    
    req.payload.tagsToConnect = tagsToConnect;
    req.payload.tagsToCreate = tagsToCreate;
  }

  return req;
};
