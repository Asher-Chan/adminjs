import { AdminJSOptions } from 'adminjs';

export type ResourceTranslation =
  AdminJSOptions['locale']['translations']['resources'];

export type ResourceNames =
  | 'COMPANIES'
  | 'TALENTS'
  | 'USERS'
  | 'MODULES'
  | 'TASKS'
  | 'STEPS'
  | 'TAGS'
  | 'SUBMISSIONS'
  | 'MODULE_PROGRESS'
  | 'CERTIFICATES';
