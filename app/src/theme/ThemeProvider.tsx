import { ThemeProvider } from "@mui/material";
import { StylesProvider } from "@mui/styles";
import { ReactNode } from "react";

import { PureLightTheme } from "./schemes/PureLightTheme";

type Props = {
  children?: ReactNode;
};

const ThemeProviderWrapper: React.FC<Props> = (props) => {
  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={PureLightTheme}>{props.children}</ThemeProvider>
    </StylesProvider>
  );
};

export default ThemeProviderWrapper;
