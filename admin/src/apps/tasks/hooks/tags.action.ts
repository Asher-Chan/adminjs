import {
  ActionRequest,
  ActionResponse,
  ActionContext,
  BaseRecord,
} from 'adminjs';

import { resourceIds } from '../../../constants/resource-ids';
import { Tag, Task } from '../../../models';

export const fetchTags = async (
  _req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
) => {
  const tags = await Tag.findMany({});

  // eslint-disable-next-line no-underscore-dangle
  const tagResource = context._admin.findResource(resourceIds.TAGS);

  const tagRecords = tags?.map((tag) =>
    new BaseRecord(tag, tagResource).toJSON(context.currentAdmin)
  );

  return tagRecords;
};

export const fetchDefaultTags = async (
  req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
) => {
  if (req.payload.recordId) {
    const { recordId } = req.payload;

    const { tags: taskTags } = await Task.findUnique({
      where: {
        id: recordId,
      },
      select: {
        tags: true,
      },
    });

    // eslint-disable-next-line no-underscore-dangle
    const tagResource = context._admin.findResource(resourceIds.TAGS);

    const tagRecords = taskTags?.map((tag) =>
      new BaseRecord(tag, tagResource).toJSON(context.currentAdmin)
    );

    return tagRecords;
  }

  return [];
};
