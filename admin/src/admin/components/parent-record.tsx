import React, { useState, useEffect } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { EditPropertyProps, ApiClient } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';
import Select, { StylesConfig } from 'react-select';
import styled from 'styled-components';

type Props = {
  searchParam: string;
  fieldName: string;
  actionName?: string;
  resourceName?: string;
};

type Option = {
  value: number;
  label: string;
};

const ErrorText = styled(Box)`
  color: red;
  font-size: 0.75rem;
`;

const selectStlyes: StylesConfig = {
  control: (provided) => ({
    ...provided,
    borderRadius: 0,
  }),
};

// Better name please
const ParentRecord: React.FC<EditPropertyProps> = ({
  onChange,
  property,
  record,
}) => {
  const [selectOptions, setSelectOptions] = useState([]);
  const [selectedOption, setSelectedOption] = useState(null);

  const { errors } = record;

  const { search } = useLocation();
  const { actionName } = useParams();

  const props = property.props as Props;

  const api = new ApiClient();

  const fetchRecords = async () => {
    if (!props.actionName || !props.resourceName) {
      return;
    }

    const result = await api.resourceAction({
      actionName: props.actionName,
      resourceId: props.resourceName,
    });

    const options = result.data.map((option) => {
      return {
        value: option.id,
        label: option.title,
      };
    });

    setSelectOptions(options);
  };

  const handleOptionChange = (option?: Option) => {
    if (!option) {
      onChange(props.fieldName, null);
      setSelectedOption(null);
      return;
    }
    setSelectedOption(option);
    onChange(props.fieldName, option.value);
  };

  const recordId = new URLSearchParams(search).get(props.searchParam);

  useEffect(() => {
    if (recordId && actionName === 'new') {
      onChange(props.fieldName, recordId);
    }
  }, [recordId]);

  useEffect(() => {
    fetchRecords();
  }, []);

  if (actionName === 'edit' || !props.actionName || !props.resourceName) {
    return null;
  }

  if (!recordId && actionName === 'new') {
    return (
      <Box mb={36}>
        <Label required={property.isRequired}>{property.label}</Label>
        <Select
          styles={selectStlyes}
          value={selectedOption}
          onChange={handleOptionChange}
          options={selectOptions}
        />
        {errors[property.propertyPath] && (
          <ErrorText mt={12}>{errors[property.propertyPath].message}</ErrorText>
        )}
      </Box>
    );
  }

  return null;
};

export default ParentRecord;
