import { NextAPIReq, NextAPIRes } from "@/types/index";
import { sendContactUsEmail } from "@/utils/send-mail";

export default async function handler(
  req: NextAPIReq<
    {},
    { name: string; email: string; messageTitile: string; message: string }
  >,
  res: NextAPIRes
) {
  const { name, email, messageTitle, message } = req.body;

  if (!name || !email || !messageTitle || !message) {
    res.status(400).json({ error: true, errorMessage: "Missing information" });
  }

  try {
    await sendContactUsEmail({ name, email, messageTitle, message });
    res.status(200).json({ error: false });
  } catch (err) {
    console.log(err);
    res.status(400).json({ error: true, errorMessage: "An error occured" });
  }
}
