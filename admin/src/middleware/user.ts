import { Prisma } from '@prisma/client';
import bcrypt from 'bcrypt';

// Not working as expected, alternative is to do this in adminjs, before action hook
export const encryptPassword: Prisma.Middleware = async (params, next) => {
  if (
    params.model === 'User' &&
    (params.action === 'create' || params.action === 'update')
  ) {
    const { data, ...otherArgs } = params.args;
    const { password, ...otherDataArgs } = data;

    if (!password) {
      return next(params);
    }

    return next(params); // Temp disabled

    // const encryptedPassword = await bcrypt.hash(
    //   password,
    //   Number(process.env.HASH_ROUNDS)
    // );

    // return next({
    //   ...params,
    //   args: {
    //     ...otherArgs,
    //     data: {
    //       ...otherDataArgs,
    //       password: encryptedPassword,
    //     },
    //   },
    // });
  }

  return next(params);
};

// Ensure company/talent records always have a `fullName` field in order for adminjs to use it for refernces
export const populateUserFields: Prisma.Middleware = async (params, next) => {
  const updatedParams = params;
  if (
    (params.action === 'findFirst' ||
      params.action === 'findMany' ||
      params.action === 'findUnique') &&
    (params.model === 'Company' || params.model === 'Talent')
  ) {
    updatedParams.args.include = {
      user: {
        select: {
          email: true,
          fullName: true,
          createdAt: true,
          displayName: true,
          isActive: true,
        },
      },
    };

    if (params.action === 'findMany') {
      updatedParams.args.orderBy = [
        {
          user: {
            fullName: 'asc',
          },
        },
      ];
    }

    const results = await next(updatedParams);

    if (updatedParams.action === 'findMany') {
      return (
        results?.map((result) => {
          const fullName = result.user?.fullName;
          return {
            ...result,
            ...(fullName ? { fullName } : {}),
          };
        }) || []
      );
    }

    const fullName = results?.user?.fullName;

    return {
      ...results,
      ...(fullName ? { fullName } : {}),
    };
  }

  return next(updatedParams);
};
