import "../styles/globals.css";
import type { AppProps } from "next/app";
import { SessionProvider } from "next-auth/react";
import { QueryClient, QueryClientProvider } from "react-query";
import CssBaseline from "@mui/material/CssBaseline";

import Layout from "@/layouts/main-layout";
import { NextPageWithLayout } from "@/types/index";
import ThemeProvider from "../theme/ThemeProvider";

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

const queryClient = new QueryClient();

function MyApp({
  Component,
  pageProps: { session, ...pageProps },
}: AppPropsWithLayout) {
  const getLayout = Component.getLayout || ((page) => page);

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider>
        <SessionProvider session={session}>
          <Layout>{getLayout(<Component {...pageProps} />)}</Layout>
          <CssBaseline />
        </SessionProvider>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default MyApp;
