import { NextAPIRes, NextAPIReq } from "@/types/index";

import { prisma } from "@/utils/db";
import { UserRoles } from "@prisma/client";

export default async function handler(
  req: NextAPIReq<{}, { email: string; password: string, fullName: string }>, // Fix this
  res: NextAPIRes
) {
  const { email, password, fullName } = req.body;

  if (!email || !password || !fullName) {
    // Addon basic password validation i guess? Need to check
    res.status(400).redirect("http://localhost:3000/auth/sign-up?error=true");
    return;
  }

  const emailExists = await prisma.user.findUnique({
    where: {
      email,
    },
  });

  if (emailExists) {
    res
      .status(400)
      .redirect("http://localhost:3000/auth/sign-up?emailExists=true");
    return;
  }

  try {
    const newUser = await prisma.user.create({
      data: {
        email,
        password, // hash this
        isActive: true,
        isVerified: false,
        role: UserRoles.TALENT,
        fullName,
        talent: {
          create: {},
        },
      },
      include: {
        talent: true,
      },
    });

    res.status(200).redirect(`/user/complete-profile?userId=${newUser.id}`);
  } catch (err) {
    console.log(err);
    res.status(400).json({
      error: true,
      errorMessage: "Failed to register user",
    });
  }
}
