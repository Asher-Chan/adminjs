import Mailgun from "mailgun-js";

const mailgun = Mailgun({
  apiKey: process.env.MAILGUN_API_KEY!,
  domain: process.env.MAILGUN_DOMAIN!,
});

export const sendResetLinkEmail = async (resetLink: string) => {
  const data: Mailgun.messages.SendData = {
    to: "asher@whiteroom.work",
    from: "Upsurge Team <me@example.uprsurge.org>",
    subject: "Reset password link",
    text: resetLink,
  };

  try {
    const body = await mailgun.messages().send(data);
    console.log(body);
  } catch (err) {
    console.log(err);
    throw new Error("Could not send reset password email");
  }
};

// TODO: turn this into generic send HTML email
export const sendContactUsEmail = async ({
  name,
  email,
  messageTitle,
  message,
}: {
  name: string;
  email: string;
  messageTitle: string;
  message: string;
}) => {
  const data: Mailgun.messages.SendData = {
    to: "asher@whiteroom.work",
    from: "Upsurge Team <me@example.uprsurge.org>",
    subject: messageTitle,
    html: `
      <div>
        <h5>${name} - ${email}</h5>
        <br />
        <p>${message}</p>
      </div>
    `,
  };

  try {
    const body = await mailgun.messages().send(data);
    console.log(body);
  } catch (err) {
    console.log(err);
    throw new Error("Could not send contact from details");
  }
};

export const sendBecomHiringPartnerEmail = async ({
  name,
  email,
  companyName,
  contactNumber,
  message,
  attachmentPath,
}: {
  name: string;
  email: string;
  companyName: string;
  contactNumber: string;
  message: string;
  attachmentPath?: string;
}) => {
  const data: Mailgun.messages.SendData = {
    to: "asher@whiteroom.work",
    from: "Upsurge Team <me@example.uprsurge.org>",
    subject: `${companyName} Wants to become a Hiring Partner`,
    html: `
      <div>
        <h5>${name} - ${email}</h5>
        <h5>${companyName} - ${contactNumber}</h5>
        <br />
        <p>${message}</p>
      </div>
    `,
    ...(attachmentPath ? { attachment: attachmentPath } : {}),
  };

  try {
    const body = await mailgun.messages().send(data);
    console.log(body);
  } catch (err) {
    console.log(err);
    throw new Error("Could not send email");
  }
};
