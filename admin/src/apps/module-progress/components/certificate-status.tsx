import React from 'react';
import { BasePropertyProps } from 'adminjs';
import { Box, Icon, Label, Link } from '@adminjs/design-system';
import { CertificateStatus as CertificateStatusEnum } from '@prisma/client';
import styled from 'styled-components';

// TODO: Handle onclick `AWARD NOW`
// Use Link from 'adminjs'

const Button = styled(Box)`
  background-color: rgba(116, 97, 232, 0.2);
  width: 100%;
  border-radius: 6px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0.2rem 0.2rem;

  &:hover {
    cursor: pointer;
  }
`;

const CertificateStatus: React.FC<BasePropertyProps> = ({
  record,
  property,
  where,
}) => {
  const value =
    record.params[property.propertyPath] || CertificateStatusEnum.PENDING;

  const isShow = where === 'show';

  let text = '';
  let color = '';
  let icon = '';

  switch (value) {
    case CertificateStatusEnum.PENDING:
      color = '#FCC619';
      icon = 'Time';
      text = value;
      break;
    case CertificateStatusEnum.AWARDED:
      color = '#29CC7A';
      icon = 'FaceActivated';
      text = value;
      break;
    case CertificateStatusEnum.READY_TO_RECIEVE:
      color = '#7461EB';
      icon = 'Trophy';
      text = 'AWARD';
      break;
    default:
      break;
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Box onClick={() => console.log('Hello')} mb={isShow ? 24 : 0}>
      {!!isShow && <Label vairiant="light">Certificate Status</Label>}
      <Box
        style={{ color }}
        flex
        flexDirection="row"
        justifyContent="start"
        alignItems="center"
      >
        {value === CertificateStatusEnum.READY_TO_RECIEVE ? (
          <Button
            onClick={() => console.log('Hello')}
            style={{ width: isShow ? 'auto' : '100%' }}
          >
            <Icon color={color} icon={icon} />
            <Box ml={12}>{text}</Box>
          </Button>
        ) : (
          <>
            <Icon color={color} icon={icon} />
            <Box ml={12}>{text}</Box>
          </>
        )}
      </Box>
    </Box>
  );
};

export default CertificateStatus;
