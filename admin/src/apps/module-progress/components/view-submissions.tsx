import React from 'react';
import { BaseRecord, ShowPropertyProps, ViewHelpers } from 'adminjs';
import { Box, Text, Button } from '@adminjs/design-system';
import styled from 'styled-components';

import { resourceIds } from '../../../constants/resource-ids';

import TaskSubmission from './task-submission';

const ModuleTitle = styled.span`
  font-weight: bold;
`;

const ModuleSummary = styled(Box)`
  border-radius: 6px;
  padding: 1rem 0rem;
  background: #e8ebf9;
  text-align: center;
`;

const SummaryText = styled(Box)`
  font-weight: normal;
  margin: 1rem 0rem;

  span {
    padding-left: 0.8rem;
    font-weight: bold;
  }
`;

const ViewSubmissions: React.FC<ShowPropertyProps> = ({ record }) => {
  const h = new ViewHelpers(); // To help construct urls
  const talent = record.populated.talent.params;
  const module = record.populated.module.params;
  // Use proper typings
  const tasks = (record.populated.tasks as unknown as BaseRecord[]) || [];
  const submissions =
    (record.populated.submissions as unknown as BaseRecord[]) || [];

  const talentURL = h.recordActionUrl({
    resourceId: resourceIds.TALENTS,
    recordId: talent.id,
    actionName: 'show',
  });

  let totalDuration = 0;

  const taskSubmissions = tasks.map((task, idx) => {
    const submission = submissions.find(
      (s) => s.params.taskId === task.params.id
    );

    if (submission?.params?.durationTaken) {
      totalDuration += submission.params.durationTaken;
    }

    return {
      taskNumber: ((idx + 1) / 10).toFixed(1),
      taskId: task.params.id,
      title: task.params.title,
      desc: task.params.desc,
      talentName: talent.fullName,

      ...(submission
        ? {
            submissionId: submission.params.id,
            submissionDate: new Date(submission.params.dateCompleted)
              .toISOString()
              .split('T')[0],
            submissionLink: submission.params.submissionURL,
            submissionDuration: submission.params.durationTaken,
            status: submission.params.status,
          }
        : {}),
    };
  });

  const getAverageDurationDisplayString = () => {
    const totalInMinutes = totalDuration * 60;

    const averageInMinutes = totalInMinutes / tasks.length;

    const numberOfHours = averageInMinutes / 60;

    const remainingMinutes = (averageInMinutes - numberOfHours * 60).toFixed(0);

    return `${numberOfHours} hours ${remainingMinutes} mins`;
  };

  const dateEnrolled = new Date(record.params.createdAt)
    .toISOString()
    .split('T')[0];

  const dateCompleted = record.params.dateCompleted
    ? new Date(record.params.dateCompleted).toISOString().split('T')[0]
    : '-';

  return (
    <Box mx={28}>
      <Box variant="card" mb={36}>
        <Box
          flex
          flexDirection="row"
          justifyContent="between"
          alignItems="center"
        >
          <Box flexGrow={1}>
            <Text variant="lg" mr={12}>
              {talent.fullName}
            </Text>
          </Box>
          <Box flexShrink={0}>
            {/** Use <a /> or react-router-dom */}
            <a href={talentURL}>
              <Button variant="primary">View Talent</Button>
            </a>
          </Box>
        </Box>
        <Box my={18}>
          Module: <ModuleTitle>{module.title}</ModuleTitle>
        </Box>
        <Box>Date Enrolled: {dateEnrolled}</Box>
      </Box>

      {taskSubmissions.length
        ? taskSubmissions.map((taskSubmission) => {
            return (
              // eslint-disable-next-line react/jsx-props-no-spreading
              <TaskSubmission key={taskSubmission.taskId} {...taskSubmission} />
            );
          })
        : 'No data to show'}

      <ModuleSummary mt={28}>
        <SummaryText>
          Total Module Duration: <span>{totalDuration} hours</span>
        </SummaryText>
        <SummaryText>
          Average Duration Per Task:
          <span>{getAverageDurationDisplayString()}</span>
        </SummaryText>
        <SummaryText>
          Date of Module Completion: <span>{dateCompleted}</span>
        </SummaryText>
      </ModuleSummary>
    </Box>
  );
};

export default ViewSubmissions;
