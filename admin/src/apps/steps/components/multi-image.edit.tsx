import React from 'react';
import {
  Box,
  Label,
  DropZone,
  DropZoneProps,
  DropZoneItem,
} from '@adminjs/design-system';
import { EditPropertyProps } from 'adminjs';

const MultiImageUpload: React.FC<EditPropertyProps> = ({
  property,
  onChange,
  record,
}) => {
  const uploadedFiles =
    JSON.parse(record.params[property.custom.fieldName] || '[]') || [];
  const handleDropZoneChange: DropZoneProps['onChange'] = (files) => {
    onChange(property.path, files);
  };

  const handleOnRemove = (idx: number) => {
    const filteredList = uploadedFiles.filter(
      (_file, fileIndex) => idx !== fileIndex
    );
    onChange(property.custom.fieldName, JSON.stringify(filteredList));
  };

  return (
    <Box mb={24}>
      <Label>{property.label}</Label>
      <DropZone onChange={handleDropZoneChange} multiple />
      {uploadedFiles.map((fileURL, idx) => (
        <DropZoneItem
          key={fileURL}
          onRemove={() => handleOnRemove(idx)}
          src={fileURL}
        />
      ))}
    </Box>
  );
};

export default MultiImageUpload;
