import React from "react";
import { GetServerSideProps, NextPage } from "next";
import {
  Task,
  Module,
  Company,
  Submission,
  SubmissionStatus,
} from "@prisma/client";

import TaskStep, { StepWithResource } from "@/components/task-step";
import TaskHeroBanner from "@/components/task-hero-banner";
import { prisma } from "@/utils/db";
import { requireAuthentication } from "@/utils/auth";

type Props = {
  module: Module & {
    company?: Company;
  };
  task: Task & {
    steps?: StepWithResource[];
  };
  taskPosition: number;
  talentsSubmission?: Submission;
};

const Task: NextPage<Props> = ({
  module,
  task,
  taskPosition,
  talentsSubmission,
}) => {
  return (
    <div>
      <TaskHeroBanner
        companyLogoURL={module.company?.logoURL || ""}
        moduleId={module.id}
        moduleTitle={module.title}
        taskOverviewImgURL={task.overviewImgURL || ""}
        taskPosition={taskPosition}
        taskTitle={task.shorthandTitle || ""}
      />

      {talentsSubmission?.status === SubmissionStatus.COMPLETED ? (
        <h2>You have completed this submission</h2>
      ) : (
        <form
          action="/api/tasks/submission"
          method="POST"
          encType="multipart/form-data"
        >
          <label htmlFor="file">Submission</label>
          <input type="file" name="file" />
          <button type="submit">Submit Me</button>
          <input type="hidden" name="taskId" value={task.id} />
          <input type="hidden" name="moduleId" value={module.id} />
        </form>
      )}

      {task.steps?.length ? (
        task.steps?.map((step, idx) => {
          return <TaskStep key={step.id} step={step} stepPosition={idx + 1} />;
        })
      ) : (
        <div>No Steps</div>
      )}
    </div>
  );
};

export default Task;

export const getServerSideProps: GetServerSideProps = requireAuthentication(
  async (ctx, session) => {
    const taskId = parseInt(ctx.query.id as string);

    const submission = await prisma.submission.findFirst({
      where: {
        taskId,
        talent: {
          // get this from auth context
          userId: session.auth.id,
        },
      },
    });

    const task = await prisma.task.findUnique({
      where: {
        id: taskId,
      },
      include: {
        steps: {
          include: {
            stepResources: true,
          },
        },
        module: {
          include: {
            company: true,
          },
        },
      },
    });

    const modulesTasks = await prisma.task.findMany({
      where: {
        moduleId: task?.moduleId,
      },
      orderBy: {
        createdAt: "desc",
      },
    });

    const taskIndex = modulesTasks.findIndex((t) => t.id === taskId);

    const props: Props = {
      module: JSON.parse(JSON.stringify(task?.module || {})),
      task: JSON.parse(JSON.stringify(task || {})),
      taskPosition: taskIndex >= 0 ? taskIndex + 1 : 0,
      ...(submission
        ? { talentsSubmission: JSON.parse(JSON.stringify(submission || {})) }
        : {}),
    };

    return {
      props,
    };
  }
);
