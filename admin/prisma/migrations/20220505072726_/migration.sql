-- AlterTable
ALTER TABLE `Company` ADD COLUMN `signatureFileURL` VARCHAR(191) NULL,
    ADD COLUMN `signatureName` VARCHAR(191) NULL,
    ADD COLUMN `signatureTitle` VARCHAR(191) NULL;
