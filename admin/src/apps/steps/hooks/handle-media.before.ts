import { ActionRequest } from 'adminjs';

import { uploadFile } from '../../../utils/cloudinary';

export const handleMediaBeforeSave = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const uploadedMediaFiles = [];
    const { mediaURLs } = req.payload;
    const urls = [...(mediaURLs ? JSON.parse(mediaURLs) : [])];

    // Find better way?
    Object.entries(req.payload).forEach(([key, value]) => {
      if (key.includes('uploadedMediaFiles')) {
        uploadedMediaFiles.push(value);
      }
    });

    for (let i = 0; i < uploadedMediaFiles.length; i += 1) {
      urls.push(
        // eslint-disable-next-line no-await-in-loop
        await uploadFile({
          filePath: uploadedMediaFiles[i].path,
          options: {
            resource_type: 'raw',
          },
        })
      );
    }

    req.payload.mediaURLs = urls.length ? urls : undefined;
  }

  return req;
};
