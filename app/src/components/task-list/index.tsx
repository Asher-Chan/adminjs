import React from "react";
import Link from "next/link";
import { Task, Tag } from "@prisma/client";

import styles from "./index.module.scss";

export type TaskWithTags = Pick<
  Task,
  "id" | "desc" | "title" | "shorthandTitle"
> & {
  tags?: Tag[];
};

type Props = {
  moduleId: number;
  tasks?: TaskWithTags[];
};

const TaskList: React.FC<Props> = ({ tasks, moduleId }) => {
  const getTaskNumber = (idx: number): string => {
    return ((idx + 1) / 10).toFixed(1);
  };

  return (
    <div className={styles.container}>
      {tasks?.length ? (
        tasks.map((task, idx) => (
          <Link key={task.id} href={`/modules/task/${task.id}`}>
            <div className={styles.task}>
              <div className={styles.taskTitle}>{`${getTaskNumber(idx)} ${
                task.title
              }`}</div>
              <p>{task.desc}</p>

              {task.tags?.length ? (
                <>
                  <div>What you will learn:</div>
                  <ul>
                    {task.tags.map((tag) => (
                      <li key={tag.id}>{tag.name}</li>
                    ))}
                  </ul>
                </>
              ) : null}
            </div>
          </Link>
        ))
      ) : (
        <div>No tasks</div>
      )}
    </div>
  );
};

export default TaskList;
