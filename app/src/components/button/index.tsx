import React from "react";
import classnames from 'classnames';

import Spinner from '../spinner'

import styles from "./index.module.scss";

type Props = {
  label: string;
  type?: "button" | "submit" | "reset";
  onClickHandler?: () => void;
  variant?: "primary" | "secondary";
  loading?: boolean;
  disabled?: boolean;
};

const Button: React.FC<Props> = ({
  label,
  type,
  onClickHandler,
  variant = "primary",
  loading = false,
  disabled= false,
}) => (
  <button
    type={type}
    disabled={disabled}
    className={classnames(variant === "secondary" ? styles.buttonSecondary : styles.button, disabled && styles.__disabled)}
    {...(onClickHandler ? { onClick: onClickHandler } : {})}
  >
    {loading ? <Spinner /> :  label}
  </button>
);

export default Button;
