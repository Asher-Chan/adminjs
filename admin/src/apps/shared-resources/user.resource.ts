import { ResourceOptions } from 'adminjs';

import {
  AddressDisplay,
  DisplayNameInput,
  ImageUpload,
  ImageView,
} from '../../admin/components.bundler';
import { resourceIds } from '../../constants/resource-ids';

/**
 * Shared user properties options used by `Company` & `Talent`
 */

export const userProperties: ResourceOptions['properties'] = {
  fullName: {
    type: 'string',
    isTitle: true,
    isRequired: true,
  },
  isActive: {
    type: 'boolean',
  },
  createdAt: {
    type: 'date',
    isVisible: {
      list: true,
    },
  },
  displayName: {
    type: 'string',
    isRequired: true,
    components: {
      edit: DisplayNameInput,
    },
  },
  uploadedProfilePhoto: {
    components: {
      edit: ImageUpload,
    },
    custom: {
      fieldName: 'profileImgURL', // Change key to better name
    },
    isVisible: {
      edit: true,
    },
  },
  profileImgURL: {
    isVisible: {
      show: true,
    },
    components: {
      show: ImageView,
    },
  },
  password: {
    type: 'password',
    isRequired: true,
    isVisible: {
      edit: true,
    },
  },
  email: {
    type: 'string',
    isRequired: true,
    isVisible: true,
    props: {
      type: 'email',
    },
  },
  linkedinUsername: {
    type: 'string',
    isRequired: true,
    isVisible: {
      edit: true,
      show: true,
    },
  },
  phone: {
    type: 'string',
    isVisible: {
      edit: true,
      show: true,
    },
  },
  isVerified: {
    type: 'boolean',
    isVisible: {
      show: true,
    },
  },
  user: {
    reference: resourceIds.USERS,
    isVisible: false,
  },
  // Addres Model Fields
  address: {
    type: 'string',
    isVisible: {
      show: true,
    },
    components: {
      show: AddressDisplay,
    },
  },
  firstLine: {
    type: 'string',
    props: { isRequired: true },
    isVisible: {
      edit: true,
    },
  },
  secondLine: {
    type: 'string',
    isVisible: {
      edit: true,
    },
  },
  state: {
    type: 'string',
    isVisible: {
      edit: true,
    },
  },
  country: {
    type: 'string',
    isVisible: {
      edit: true,
    },
  },
  postCode: {
    type: 'string',
    isVisible: {
      edit: true,
    },
  },
  city: {
    type: 'string',
    isVisible: {
      edit: true,
    },
  },
};
