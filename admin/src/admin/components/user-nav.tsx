import React from 'react';
import { CurrentUserNav } from '@adminjs/design-system';

type Props = {
  paths: {
    loginPath: string;
    logoutPath: string;
    rootPath: string;
  };
  session: {
    email?: string;
    fullName?: string;
    id: string;
    profileImgURL?: string;
    lastName?: string;
    isActive?: boolean;
    role?: string;
  };
};

const UserNav: React.FC<Props> = (props) => {
  const { session, paths } = props;
  return (
    <CurrentUserNav
      name={session.fullName}
      // title={session.role}
      lineActions={[{ icon: 'Person', label: 'People' }]} // Icon buttons next to username
      dropActions={[{ label: 'Log out', href: paths.logoutPath }]} // Menu actions
      avatarUrl={session.profileImgURL || ''}
    />
  );
};

export default UserNav;
