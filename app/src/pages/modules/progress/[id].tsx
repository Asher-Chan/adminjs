import React from "react";
import { GetServerSideProps, NextPage } from "next";
import Link from "next/link";
import { getSession } from "next-auth/react";

import { prisma } from "@/utils/db";
import { TalentsModuleSubmissionProgress } from "@/types/modules";
import { SubmissionStatus } from "@prisma/client";

type Props = {
  moduleProgress: TalentsModuleSubmissionProgress;
};

const ModuleProgress: NextPage<Props> = ({ moduleProgress }) => {
  const { module, submissions } = moduleProgress;

  let totalDuration = 0;

  const taskSubmissions = module?.tasks.map((task, idx) => {
    const submission = submissions.find((s) => s.taskId === task.id);

    if (submission?.durationTaken) {
      totalDuration += submission.durationTaken;
    }

    return {
      taskNumber: ((idx + 1) / 10).toFixed(1),
      taskId: task.id,
      title: task.title,
      desc: task.desc,

      ...(submission
        ? {
            submissionId: submission.id,
            submissionDate: submission.dateCompleted
              ? new Date(submission.dateCompleted).toISOString().split("T")[0]
              : "",
            submissionLink: submission.submissionURL,
            submissionDuration: submission.durationTaken,
            status: submission.status,
          }
        : {}),
    };
  });

  return (
    <div>
      <div>{module?.company?.user?.fullName}</div>
      <div>Module: {module?.title}</div>
      <div>
        Date enrolled:{" "}
        {new Date(moduleProgress.createdAt).toISOString().split("T")[0]}
      </div>

      <Link href={`/modules/overview/${module?.id}`}>
        <button>See module overview</button>
      </Link>

      {taskSubmissions?.length
        ? taskSubmissions.map((taskSubmission) => {
            return (
              // eslint-disable-next-line react/jsx-props-no-spreading
              <div key={taskSubmission.taskId}>
                <div>
                  <div>
                    <div>{`${taskSubmission.taskNumber} ${taskSubmission.title}`}</div>
                    <div>
                      {taskSubmission.status === SubmissionStatus.COMPLETED
                        ? "Submitted"
                        : "Not Complete"}
                    </div>
                  </div>
                  <div>
                    <div>{taskSubmission.desc}</div>
                  </div>
                  <div>
                    <div>
                      Duration taken to complete:
                      <span>
                        {taskSubmission.submissionDuration || "-"} hours
                      </span>
                    </div>
                  </div>
                </div>
                <div>
                  <div>
                    {taskSubmission.status === SubmissionStatus.COMPLETED ? (
                      <>
                        <div>Last Submitted:</div>
                        <div>{taskSubmission.submissionDate}</div>
                        <div>Download</div>
                      </>
                    ) : (
                      <div>No submission</div>
                    )}
                  </div>
                </div>
              </div>
            );
          })
        : "No data to show"}
    </div>
  );
};

export default ModuleProgress;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getSession(ctx);
  const userId = session?.auth.id;

  if (!userId) {
    return {
      notFound: true,
    };
  }

  const moduleProgressId = parseInt(ctx.query.id as string);

  const moduleProgress = await prisma.moduleProgress.findUnique({
    where: {
      id: moduleProgressId,
    },
    include: {
      module: {
        include: {
          tasks: {
            include: {
              tags: true,
            },
          },
          company: {
            include: {
              user: true,
            },
          },
        },
      },
      submissions: {
        where: {
          talent: {
            userId,
          },
        },
      },
      talent: true,
    },
  });

  if (!moduleProgress || moduleProgress?.talent.userId !== userId) {
    return {
      notFound: true,
    };
  }

  const props: Props = {
    moduleProgress: JSON.parse(JSON.stringify(moduleProgress)),
  };

  return {
    props,
  };
};
