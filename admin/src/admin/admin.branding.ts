import { AdminJSOptions } from 'adminjs';

import theme from './admin.theme';

const branding: AdminJSOptions['branding'] = {
  companyName: 'Upsurge',
  softwareBrothers: false,
  theme,
  // From /public || cloudinary ?
  logo: '/images/upsurge-logo-full.png',
  favicon: '/images/upsurge-logo.png',
};

export { branding };
