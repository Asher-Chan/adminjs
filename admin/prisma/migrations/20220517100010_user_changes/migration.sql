-- AlterTable
ALTER TABLE `Talent` ADD COLUMN `onboardingCompleted` BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE `User` ADD COLUMN `isGoogleLogin` BOOLEAN NOT NULL DEFAULT false,
    MODIFY `password` VARCHAR(191) NULL;
