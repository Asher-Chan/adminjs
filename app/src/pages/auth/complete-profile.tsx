import React, { ChangeEvent, useState } from "react";
import { GetServerSideProps, NextPage } from "next";
import { useMutation } from "react-query";

import { prisma } from "@/utils/db";

type Props = {
  userId: number;
};

const CompleteProfile: NextPage<Props> = ({ userId }) => {
  const [data, setData] = useState({
    userId,
    fullName: "",
    phone: "",
    levelOfStudy: "",
    major: "",
    educationalInstituition: "",
    jobPosition: "",
    currentCompany: "",
  });

  // Learn to handle react-query responses, errors

  const {
    mutate,
    isLoading,
    data: resData,
    isError,
  } = useMutation(async () => {
    // TODO: Use axios, clean up use proper way find examples
    try {
      const res = await fetch("/api/user/complete-profile", {
        method: "POST",
        body: JSON.stringify({
          ...data,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      return await res.json();
    } catch (err) {
      throw err;
    }
  });

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    setData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <div>
      <h3>Complete your profile</h3>

      {isLoading && <div>Loading...</div>}

      {!!resData?.error && <div>{resData?.errorMessage  || 'Error occured'}</div>}

      <form>
        <label htmlFor="fullName">Full Name</label>
        <div>
          <input
            type="text"
            name="fullName"
            value={data.fullName}
            onChange={handleOnChange}
          />
        </div>
        <label htmlFor="phone">Phone</label>
        <div>
          <input
            type="text"
            name="phone"
            value={data.phone}
            onChange={handleOnChange}
          />
        </div>
        <label htmlFor="levelOfStudy">Level of study</label>
        <div>
          <input
            type="text"
            name="levelOfStudy"
            value={data.levelOfStudy}
            onChange={handleOnChange}
          />
        </div>
        <label htmlFor="major">Major</label>
        <div>
          <input
            type="text"
            name="major"
            value={data.major}
            onChange={handleOnChange}
          />
        </div>
        <label htmlFor="educationalInstituition">
          Educational Instituition
        </label>
        <div>
          <input
            type="text"
            name="educationalInstituition"
            value={data.educationalInstituition}
            onChange={handleOnChange}
          />
        </div>
        <label htmlFor="jobPosition">Job Position</label>
        <div>
          <input
            type="text"
            name="jobPosition"
            value={data.jobPosition}
            onChange={handleOnChange}
          />
        </div>
        <label htmlFor="currentCompany">Current Company</label>
        <div>
          <input
            type="text"
            name="currentCompany"
            value={data.currentCompany}
            onChange={handleOnChange}
          />
        </div>

        <button type="button" onClick={() => mutate()}>
          Complete Profile
        </button>
      </form>
    </div>
  );
};

export default CompleteProfile;

export const getServerSideProps: GetServerSideProps<Props> = async (ctx) => {
  const { userId } = ctx.query as { userId?: string };

  // Do a check that the talent has not completed onboarding yet, if completed redirect

  if (!userId) {
    return {
      redirect: {
        destination: "/auth/sign-up",
        permanent: false,
      },
    };
  }

  const user = await prisma.user.findUnique({
    where: {
      id: parseInt(userId),
    },
    include: {
      talent: true,
    },
  });

  if (!user) {
    return {
      redirect: {
        destination: "auth/sign-up",
        permanent: false,
      },
    };
  }

  return {
    props: {
      userId: parseInt(userId),
    },
  };
};
