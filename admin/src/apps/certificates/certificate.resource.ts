import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import { FileURL } from '../../admin/components.bundler';
import prisma from '../../prisma-client';

const options: ResourceOptions = {
  id: resourceIds.CERTIFICATES,
  navigation: navigation.CERTIFICATES,
  listProperties: ['certId', 'talent', 'module', 'fileURL', 'createdAt'],
  filterProperties: ['certId', 'talent', 'module', 'fileURL', 'createdAt'],
  actions: {
    new: {
      isVisible: false,
      isAccessible: false,
    },
    edit: {
      isVisible: false,
      isAccessible: false,
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    certId: {
      isTitle: true,
    },
    module: {
      type: 'reference',
      reference: resourceIds.MODULES,
    },
    talent: {
      type: 'reference',
      reference: resourceIds.TALENTS,
    },
    fileURL: {
      components: {
        list: FileURL,
        show: FileURL,
      },
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.Certificate },
  options,
};
