import {
  ActionContext,
  ActionRequest,
  ActionResponse,
  BaseRecord,
} from 'adminjs';

import { resourceIds } from '../../../constants/resource-ids';

import { Company } from '../../../models';

export const fetchCompanies = async (
  _req: ActionRequest,
  _res: ActionResponse,
  context: ActionContext
) => {
  const companies = await Company.findMany({
    include: {
      user: {
        select: {
          fullName: true,
        },
      },
    },
  });

  // eslint-disable-next-line no-underscore-dangle
  const companyResource = context._admin.findResource(resourceIds.COMPANIES);

  const companyRecords = companies?.map((company) =>
    new BaseRecord(
      {
        ...company,
        fullName: company.user.fullName || '',
      },
      companyResource
    ).toJSON(context.currentAdmin)
  );

  return companyRecords;
};
