import React, { ChangeEvent } from "react";

import styles from "./index.module.scss";

type Props = {
  name: string;
  label: string;
  value?: string | number;
  onChange?: (e: ChangeEvent<HTMLTextAreaElement>) => void;
  rows?: number;
};

const TextArea: React.FC<Props> = ({ name, value, onChange, label, rows = 5 }) => {
  return (
    <div className={styles.inputContainer}>
      <label className={styles.label}>{label}</label>
      <textarea
        name={name}
        rows={rows}
        className={styles.input}
        value={value}
        onChange={(e) => onChange?.(e)}
      />
    </div>
  );
};

export default TextArea;
