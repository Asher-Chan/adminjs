import React from "react";
import Link from "next/link";
import { useSession } from "next-auth/react";

import styles from "./index.module.scss";

type Props = {
  children: React.ReactNode;
};

const DashboardLayout: React.FC<Props> = ({ children }) => {
  const { data, status } = useSession();

  return (
    <div className={styles.container}>
      <div className={styles.sideNav}>
        <div className={styles.userRole}>{data?.auth.role}</div>
        <ul>
          <Link href="/dashboard/overview">
            <li>Overview</li>
          </Link>
          <Link href="/dashboard/my-modules">
            <li>
              {data?.auth.role === "COMPANY"
                ? `${data.auth.fullName} Modules`
                : "My Modules"}
            </li>
          </Link>
          {data?.auth.role === "COMPANY" && (
            <Link href="/dashboard/submissions">
              <li>Submission</li>
            </Link>
          )}
          {data?.auth.role === "TALENT" && (
            <>
              <Link href="/dashboard/my-distinctions">
                <li>My Distinctions</li>
              </Link>
              <Link href="/dashboard/available-modules">
                <li>Available Modules</li>
              </Link>
            </>
          )}
        </ul>
      </div>
      <div className={styles.content}>{children}</div>
    </div>
  );
};

export default DashboardLayout;
