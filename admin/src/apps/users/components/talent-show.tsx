import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';

const TalentShow: React.FC<ShowPropertyProps> = (props) => {
  const { record } = props;
  const { talent } = record.populated;

  if (!talent) {
    return null;
  }

  const { params } = talent;

  return (
    <Box>
      <Label uppercase variant="primary">
        Talent Details
      </Label>
      <Box>
        <Box mb={24}>
          <Label variant="light">Current Level Of Study</Label>
          {params.currentLevelOfStudy || '-'}
        </Box>
        <Box mb={24}>
          <Label variant="light">Major</Label>
          {params.major || '-'}
        </Box>
      </Box>
    </Box>
  );
};

export default TalentShow;
