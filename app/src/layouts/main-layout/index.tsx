import React from "react";

import NavBar from "@/components/nav-bar";

import styles from "./index.module.scss";

type Props = {
  children: React.ReactNode;
};

const MainLayout: React.FC<Props> = ({ children }) => {
  return (
    <>
      <NavBar />
      <main className={styles.main}>{children}</main>
    </>
  );
};

export default MainLayout;
