import { NextAPIReq, NextAPIRes } from "@/types/index";
import { prisma } from "@/utils/db";

type Answers = {
  reasonInterested: string;
  companyKnowledgeLevel: number;
  joinCompanyPotentialLevel: number;
  likelinessToApplyLevel: number;
};

export default async function handler(
  req: NextAPIReq<
    {}, // Not providing types
    {
      talentId: number;
      moduleId: number;
      answers: Answers;
    }
  >,
  res: NextAPIRes
) {
  const { moduleId, talentId, ...answers } = req.body;

  try {
    await prisma.$transaction([
      prisma.registrationForm.create({
        data: {
          talent: {
            connect: {
              id: talentId,
            },
          },
          module: {
            connect: {
              id: moduleId,
            },
          },
          ...answers,
        },
      }),
      prisma.moduleProgress.create({
        data: {
          talent: {
            connect: {
              id: talentId,
            },
          },
          module: {
            connect: {
              id: moduleId,
            },
          },
          // See if next two fields can be default in Prisma
          status: "STARTED",
          certificateStatus: "PENDING",
        },
      }),
    ]);
    res.status(200).json({ error: false });
  } catch (err) {
    console.log(err);
    res
      .status(400)
      .json({ error: true, errorMessage: "Error submitting form" });
  }
}
