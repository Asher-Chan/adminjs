import React, { ReactElement } from "react";

import { requireAuthentication } from "@/utils/auth";
import DashboardLayout from "@/layouts/dashboard-layout";
import { NextPageWithLayout } from "@/types/index";

type Props = {};

const Overview: NextPageWithLayout<Props> = ({}) => {
  return <div>Dashboard - Overview</div>;
};

export default Overview;

export const getServerSideProps = requireAuthentication<Props>(async (ctx) => {
  return {
    props: {},
  };
});

Overview.getLayout = function getLayout(page: ReactElement) {
  return <DashboardLayout>{page}</DashboardLayout>;
};
