import { ActionResponse, ActionRequest, ActionContext } from 'adminjs';
import { resourceIds } from '../../../constants/resource-ids';

export const redirectAfterSave = async (
  originalResponse: ActionResponse,
  _req: ActionRequest,
  context: ActionContext
) => {
  const { record } = originalResponse;
  const redirectUrl = context.h.showUrl(resourceIds.MODULES, record.params.id);

  return {
    ...originalResponse,
    redirectUrl,
  };
};
