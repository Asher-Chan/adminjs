import { ThemeOverride } from '@adminjs/design-system';

export default {
  colors: {
    primary100: '#8172E0',
    success: '#00FF00',
  },
} as ThemeOverride;

// Theme -> https://docs.adminjs.co/Theme.html

// Components-> https://storybook.adminjs.co/?path=/story/designsystem-atoms-icon--default
