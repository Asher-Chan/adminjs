import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';

const AddressShow: React.FC<ShowPropertyProps> = ({ record, property }) => {
  const { firstLine, secondLine, city, state, country, postCode } =
    record.params;

  const addressStrings = [
    ...(firstLine ? [firstLine] : []),
    ...(secondLine ? [secondLine] : []),
    ...(city ? [city] : []),
    ...(postCode ? [postCode] : []),
    ...(state ? [state] : []),
    ...(country ? [country] : []),
  ].join(', ');

  return addressStrings.length ? (
    <Box mb={24}>
      <Label variant="light">{property.label}</Label>
      {addressStrings}
    </Box>
  ) : null;
};

export default AddressShow;
