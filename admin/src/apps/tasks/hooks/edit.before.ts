import { ActionRequest } from 'adminjs';

import { Task } from '../../../models';

export const handleTagsBeforeSave = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { id, tagsToCreate, tagsToConnect } = req.payload;

    await Task.update({
      where: {
        id: parseInt(id),
      },
      data: {
        tags: {
          ...(tagsToConnect.length || tagsToCreate.length ? { set: [] } : {}),
          ...(tagsToConnect.length ? { connect: tagsToConnect } : {}),
          ...(tagsToCreate.length
            ? {
                create: tagsToCreate.map((tag) => ({
                  name: tag.name,
                })),
              }
            : {}),
        },
      },
    });
  }

  return req;
};
