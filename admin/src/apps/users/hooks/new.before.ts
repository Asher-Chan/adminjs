import { ActionRequest } from 'adminjs';
import { UserRoles } from '@prisma/client';
import bcrypt from 'bcrypt';

export const handleBeforeSave = async (req: ActionRequest) => {
  return {
    ...req,
    payload: {
      ...req.payload,
      isVerified: true,
      role: UserRoles.ADMIN,
      ...(req.payload.password
        ? {
            password: await bcrypt.hash(
              req.payload.password,
              Number(process.env.HASH_ROUNDS)
            ),
          }
        : {}),
    },
  };
};
