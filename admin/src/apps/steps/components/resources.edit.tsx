/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import { EditPropertyProps } from 'adminjs';
import { FileTypes } from '@prisma/client';
import {
  Box,
  Label,
  Input,
  Button,
  Icon,
  DropZone,
  DropZoneItem,
} from '@adminjs/design-system';
import styled from 'styled-components';

type Resource = {
  id: string;
  title: string;
  desc: string;
  type?: FileTypes;
  resourceURL: string;
};

const defaultResource: Resource = {
  id: '',
  title: '',
  desc: '',
  resourceURL: '',
};

const ErrorText = styled(Box)`
  color: red;
  font-size: 0.75rem;
`;

const ResourcesEdit: React.FC<EditPropertyProps> = ({
  // Handle default values
  record,
  property,
  onChange,
}) => {
  const [resources, setResources] = useState(
    (record.populated.stepResources as unknown as any[]) || []
  );
  const [editingIdx, setEditingIdx] = useState(null);
  const [errors, setErrors] = useState({});

  const handleInputOnChange = (
    value: string,
    fieldName: string,
    idx: number
  ) => {
    setResources((prevResources) => {
      const updatedResources = [...prevResources];
      updatedResources[idx] = {
        ...updatedResources[idx],
        [fieldName]: value,
      };

      return updatedResources;
    });
  };

  // (TS error) File type doesn't exist in Node.js, but can be used in React Components
  // eslint-disable-next-line no-undef
  const handleOnFileChange = (file: File, idx: number) => {
    setResources((prevResources) => {
      const updatedResources = [...prevResources];
      updatedResources[idx] = {
        ...updatedResources[idx],
        resourceFile: file,
        resourceURL: '',
      };

      return updatedResources;
    });
  };

  const handleRemove = (idx: number) => {
    const resource = resources[idx];

    if (resource.__isNew__) {
      setResources((prevResources) => {
        const updatedResources = prevResources.filter(
          (_resource, resourceIdx) => resourceIdx !== idx
        );

        onChange(property.propertyPath, updatedResources);
        return updatedResources;
      });
    } else {
      setResources((prevResources) => {
        const updatedResources = [...prevResources];
        updatedResources[idx] = {
          ...updatedResources[idx],
          __isDeleted__: true,
        };

        onChange(property.propertyPath, updatedResources);
        return updatedResources;
      });
    }

    setEditingIdx(null);
  };

  const handleOnSave = (idx: number) => {
    if (!resources[idx].title) {
      setErrors({
        [idx]: {
          title: 'Resource title is required',
        },
      });
      return;
    }
    setErrors({});
    if (
      !resources[idx].type ||
      (!resources[idx].resourceURL && !resources[idx].resourceFile) ||
      (resources[idx].__isNew__ &&
        resources[idx].type === FileTypes.WEBSITE_LINK &&
        !resources[idx].resourceURL)
    ) {
      setErrors({
        [idx]: {
          file: 'Please add a resource',
        },
      });
      return;
    }
    setEditingIdx(null);
    setErrors({});
    onChange(property.propertyPath, resources);
    onChange('resourceCount', resources.length);
  };

  const handleOnClickAdd = () => {
    const updatedResources = [
      ...resources,
      // Have to add id, prevent React problem, use smtg else tho
      // https://stackoverflow.com/questions/42573017/in-react-es6-why-does-the-input-field-lose-focus-after-typing-a-character
      { ...defaultResource, id: Math.random(), __isNew__: true },
    ];
    setResources(updatedResources);
    setEditingIdx(updatedResources.length - 1);
  };

  const onClickEditResource = (idx: number) => {
    setEditingIdx(idx);
  };

  const onRemoveFile = (idx: number) => {
    setResources((prevResources) => {
      const updatedResources = [...prevResources];
      updatedResources[idx] = {
        ...updatedResources[idx],
        resourceFile: '',
        resourceURL: '',
      };

      return updatedResources;
    });
  };

  const getMimeType = (fileType: FileTypes): string[] => {
    switch (fileType) {
      case FileTypes.DOCUMENT:
        return [
          'application/msword',
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
          'text/csv',
          'text/plain',
        ];
      case FileTypes.PDF:
        return ['application/pdf'];
      case FileTypes.ZIP:
        return ['application/zip', 'application/vnd.rar'];
      default:
        return [];
    }
  };

  function renderResourceTypeInput(
    type: FileTypes,
    value: string,
    // (TS error) File type doesn't exist in Node.js, but can be used in React Components
    // eslint-disable-next-line no-undef
    file: File,
    idx: number
  ) {
    if (type === FileTypes.WEBSITE_LINK) {
      return (
        <>
          <Label>Website Link</Label>
          <Input
            mb={36}
            width="100%"
            value={value}
            onChange={(e) =>
              handleInputOnChange(e.target.value, 'resourceURL', idx)
            }
          />
        </>
      );
    }

    const mimeTypes = getMimeType(type);

    return (
      <Box mb={24}>
        <Label>Resource - {type}</Label>
        <DropZone
          validate={{
            ...(mimeTypes.length ? { mimeTypes } : {}),
          }}
          files={file ? [file] : []}
          onChange={(files) => handleOnFileChange(files[0], idx)}
          multiple={!!property.props.multipleFiles}
        />
        {value && (
          <DropZoneItem onRemove={() => onRemoveFile(idx)} src={value} />
        )}
      </Box>
    );
  }

  useEffect(() => {
    if (resources) {
      onChange('resourceCount', resources.length);
    }
  }, [resources]);

  return (
    <Box mb={36}>
      <Box flex flexDirection="row">
        <Box flexGrow={1}>
          <Label>Resources</Label>
        </Box>
        <Box flexShrink={0}>
          {editingIdx === null ? (
            <Button
              type="button"
              flex
              variant="light"
              onClick={handleOnClickAdd}
            >
              <Icon icon="Add" />
              Add Resource
            </Button>
          ) : null}
        </Box>
      </Box>
      {resources.length ? (
        resources.flatMap((resource, idx) => {
          if (resource.__isDeleted__) {
            return [];
          }

          return (
            <Box key={resource.id} m={12} variant="card">
              {editingIdx === idx ? (
                <>
                  <Label uppercase mb={24} variant="primary">
                    Add Resource ({idx + 1})
                  </Label>

                  <Label>Resource Title</Label>
                  {errors[idx]?.title && (
                    <ErrorText>{errors[idx].title}</ErrorText>
                  )}
                  <Input
                    mb={36}
                    width="100%"
                    placeholder="GitHub Repository 1"
                    value={resource.title}
                    onChange={(e) =>
                      handleInputOnChange(e.target.value, 'title', idx)
                    }
                  />

                  <Label>Resource Description</Label>
                  <Input
                    mb={36}
                    width="100%"
                    value={resource.desc}
                    placeholder="Understand the open-source C++ Pac-Mac rules"
                    onChange={(e) =>
                      handleInputOnChange(e.target.value, 'desc', idx)
                    }
                  />

                  {resource.type
                    ? renderResourceTypeInput(
                        resource.type,
                        resource.resourceURL,
                        resource.resourceFile,
                        idx
                      )
                    : null}

                  {errors[idx]?.file && (
                    <ErrorText mb={36}>{errors[idx].file}</ErrorText>
                  )}

                  <Box
                    mx={12}
                    mb={36}
                    flex
                    flexDirection="row"
                    justifyContent="space-around"
                  >
                    <Button
                      variant="info"
                      type="button"
                      onClick={() =>
                        handleInputOnChange(FileTypes.PDF, 'type', idx)
                      }
                    >
                      Add PDF
                    </Button>
                    <Button
                      variant="info"
                      type="button"
                      onClick={() =>
                        handleInputOnChange(FileTypes.WEBSITE_LINK, 'type', idx)
                      }
                    >
                      Add website link
                    </Button>
                    <Button
                      variant="info"
                      type="button"
                      onClick={() =>
                        handleInputOnChange(FileTypes.ZIP, 'type', idx)
                      }
                    >
                      Add zipped file
                    </Button>
                    <Button
                      variant="info"
                      type="button"
                      onClick={() =>
                        handleInputOnChange(FileTypes.DOCUMENT, 'type', idx)
                      }
                    >
                      Add document
                    </Button>
                  </Box>

                  <Box flex flexDirection="row" justifyContent="flex-end">
                    <Button
                      variant="danger"
                      type="button"
                      onClick={() => handleRemove(idx)}
                    >
                      Remove
                    </Button>
                    <Button
                      variant="primary"
                      type="button"
                      ml={8}
                      onClick={() => handleOnSave(idx)}
                    >
                      Save
                    </Button>
                  </Box>
                </>
              ) : (
                <Box
                  flex
                  flexDirection="row"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <div>{resource.title}</div>
                  <Button
                    type="button"
                    onClick={() => onClickEditResource(idx)}
                  >
                    Edit
                  </Button>
                </Box>
              )}
            </Box>
          );
        })
      ) : (
        <div>No resources for this step added yet</div>
      )}
    </Box>
  );
};

export default ResourcesEdit;
