import React, { useEffect, useState } from "react";
import { useController, useFormContext } from "react-hook-form";

import styles from "index.module.scss";

type Props = {
  name: string;
};

const FileInput: React.FC<Props> = ({ name }) => {
  const [value, setValue] = useState(""); // Fake path here if want to show on client-side
  const { control } = useFormContext();
  const { field } = useController({ control, name });

  useEffect(() => {
    if (!field.value) {
      setValue("");
    }
  }, [field.value]);

  return (
    <input
      type="file"
      value={value}
      onChange={(e) => {
        setValue(e.target.value);
        field.onChange(e.target.files);
      }}
    />
  );
};

export default FileInput;
