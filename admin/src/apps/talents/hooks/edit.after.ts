import { ActionResponse, ActionRequest, ActionContext } from 'adminjs';
import { resourceIds } from '../../../constants/resource-ids';

// STOP REPEATING THIS
export const redirectAfterSave = async (
  originalResponse: ActionResponse,
  _req: ActionRequest,
  context: ActionContext
) => {
  const { record } = originalResponse;
  const redirectUrl = context.h.showUrl(resourceIds.TALENTS, record.params.id);

  return {
    ...originalResponse,
    redirectUrl,
  };
};
