import { UserRoles } from '@prisma/client';
import { ActionRequest } from 'adminjs';

export const showAdminsOnly = async (
  req: ActionRequest,
) => {
  return {
    ...req,
    query: {
      ...req.query,
      'filters.role': UserRoles.ADMIN,
    },
  };
};
