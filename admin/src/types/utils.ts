export type Modify<T, TReplace> = Omit<T, keyof TReplace> & TReplace;
