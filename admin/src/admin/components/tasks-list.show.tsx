import React from 'react';
import { Box, H4, Button, Icon, Link } from '@adminjs/design-system';
import { BaseRecord, ShowPropertyProps } from 'adminjs';
import styled from 'styled-components';

import { resourceIds } from '../../constants/resource-ids';

const Task = styled(Box)`
  background: rgba(116, 97, 232, 0.15);
  box-shadow: 2px 4px 5px rgba(0, 0, 0, 0.15);
  border-radius: 6px;
  padding: 1.2rem;
  color: #000;
  margin-bottom: 0.8rem;

  &:hover {
    background: rgba(116, 97, 232, 0.6);
    cursor: pointer;
  }
`;

const TaskTitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const TaskTitle = styled.div`
  text-decoration: underline;
  font-weight: normal;
`;

// Used in both `Modules` & `Task`
const List: React.FC<ShowPropertyProps> = ({ record, property, resource }) => {
  const records = record.populated.tasks as unknown as BaseRecord[];

  // When on task page, make sure to remove own task from list, if is module record this is ignored
  const currentTaskId =
    resource.name === resourceIds.MODULES ? '' : record.params.id;

  const getTaskNumber = (idx: number): string => {
    return ((idx + 1) / 10).toFixed(1);
  };

  const filteredRecords =
    records?.flatMap((task) => {
      if (task.params.id === currentTaskId) {
        return [];
      }

      return task;
    }) || [];

  return (
    <Box mb={36}>
      <Box flex flexDirection="row">
        <Box flexGrow={1}>
          <H4>{property.props.label || 'Tasks'}</H4>
        </Box>
        {!!property.props.showAddButton && (
          <Box flexShrink={0}>
            <Link href={record.params.addNewLink}>
              <Button flex variant="light">
                <Icon icon="Add" />
                Add Task
              </Button>
            </Link>
          </Box>
        )}
      </Box>

      {records?.length ? (
        records.flatMap((task, idx) => {
          if (task.params.id === currentTaskId) {
            return [];
          }

          return (
            <Link key={task.params.id} href={task.params.itemLink}>
              <Task>
                <TaskTitleContainer>
                  <TaskTitle>{`${getTaskNumber(idx)} ${task.params.title} [${
                    task.params.shorthandTitle
                  }]`}</TaskTitle>
                </TaskTitleContainer>
              </Task>
            </Link>
          );
        })
      ) : (
        <div>No tasks have been added yet.</div>
      )}
      {records?.length === 1 && records?.length !== filteredRecords.length && (
        <div>No other tasks in this module.</div>
      )}
    </Box>
  );
};

export default List;
