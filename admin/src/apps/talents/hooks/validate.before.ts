import { ActionRequest, ValidationError } from 'adminjs';

import { User } from '../../../models';
import { SortedTalentPayload } from '../../../types/forms';

export const validateData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { attachedUser, ...talent } = req.payload as SortedTalentPayload;

    const errors: { [key: string]: { message: string } } = {};

    const emailTaken = await User.findUnique({
      where: {
        email: attachedUser.email,
      },
    });

    if (!!emailTaken && emailTaken?.id.toString() !== talent.userId) {
      errors.email = {
        message: 'Email must be unique',
      };
    }

    const numOfErrors = Object.keys(errors).length;

    if (numOfErrors) {
      throw new ValidationError(
        {
          ...errors,
        },
        {
          message: 'Some field are missing or invalid.',
        }
      );
    }
  }

  return req;
};
