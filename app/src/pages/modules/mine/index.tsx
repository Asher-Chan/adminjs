import React from "react";
import { GetServerSideProps, NextPage } from "next";
import Link from "next/link";
import { getSession } from "next-auth/react";

import { ModuleWithTalentProgress } from "@/types/modules";
import { getAllModuleProgressOfTalent } from "@/api/modules/mine";
import { requireAuthentication } from "@/utils/auth";

type Props = {
  modules: ModuleWithTalentProgress[];
};

const MyModules: NextPage<Props> = ({ modules }) => {
  return (
    <div>
      <h3>My Modules</h3>

      {!!modules.length &&
        modules.map((module) => (
          <div key={module.id}>
            <div>{module.title}</div>
            <div>{module.company?.user?.fullName || "-"}</div>

            <b>{module.status}</b>

            <Link href={`/modules/progress/${module.moduleProgressId}`}>
              <button>View progress</button>
            </Link>
          </div>
        ))}
    </div>
  );
};

export default MyModules;

export const getServerSideProps: GetServerSideProps =
  requireAuthentication(async (ctx) => {
    const session = await getSession(ctx);
    const userId = session?.auth.id;

    // Get all module progress of current talent
    if (!userId) {
      return {
        redirect: {
          destination: "/modules",
          permanent: false,
        },
        props: {},
      };
    }

    try {
      const data = await getAllModuleProgressOfTalent(userId);
      return {
        props: {
          modules: JSON.parse(JSON.stringify(data)),
        },
      };
    } catch (err) {
      const errMessage = !(err instanceof Error)
        ? "Error fetching modules"
        : err.message;
      return {
        props: {
          modules: [],
        },
      };
    }
  });
