import multiparty from "multiparty";

import { NextAPIReq, NextAPIRes } from "@/types/index";
import { sendBecomHiringPartnerEmail } from "@/utils/send-mail";

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async function handler(
  req: NextAPIReq<
    {},
    {
      name: string;
      email: string;
      companyName: string;
      contactNumber: string;
      message: string;
    }
  >,
  res: NextAPIRes
) {
  const form = new multiparty.Form();

  try {
    // TODO: Find better way or add TS
    form.parse(req, async (err, fields, files) => {
      if (err) {
        throw new Error(err.message || "Error parsing form");
      }

      const { name, email, companyName, message, contactNumber } = fields;

      if (!name || !email || !companyName || !message || !contactNumber) {
        throw new Error("Missing information");
      }

      await sendBecomHiringPartnerEmail({
        name,
        email,
        companyName,
        message,
        contactNumber,
        attachmentPath: files?.attachment?.[0]
          ? (files?.attachment?.[0].path as string)
          : undefined,
      });
    });
    res.status(200).json({ error: false });
  } catch (err) {
    console.log(err);
    res.status(400).json({ error: true, errorMessage: "An error occured" });
  }
}
