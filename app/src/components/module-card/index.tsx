import React from "react";
import { ModuleProgressStatus } from "@prisma/client";
import Link from "next/link";
import {
  Box,
  Card,
  Avatar,
  Typography,
  AvatarGroup,
  Grid,
} from "@mui/material";

import Button from "../button";

type ModuleAction = {
  actionName: string;
  action: (moduleId: number) => void;
};

type Props = {
  moduleId: number;
  companyName: string;
  companyBrandColor?: string;
  moduleDurationStart: number;
  moduleDurationEnd: number;
  moduleTitle: string;
  moduleActions?: [ModuleAction] | [ModuleAction, ModuleAction];
  progressStatus?: ModuleProgressStatus;
};

const ModuleCard: React.FC<Props> = ({
  moduleId,
  companyName,
  companyBrandColor,
  moduleActions,
  moduleDurationEnd,
  moduleDurationStart,
  moduleTitle,
  progressStatus,
}) => {
  const durationString = `${moduleDurationStart} - ${moduleDurationEnd} hours`;

  return (
    // <div className={styles.card}>
    //   {/* Company */}
    //   <div className={styles.company}>
    //     <div
    //       className={styles.companyLogo}
    //       style={{ backgroundColor: companyBrandColor }}
    //     >
    //       {companyName[0]}
    //     </div>
    //     <div className={styles.companyName}>{companyName}</div>
    //   </div>

    //   {/* Module */}
    //   <div className={styles.module}>
    //     <div className={styles.duration}>{durationString}</div>
    //     <div className={styles.moduleTitle}>{moduleTitle}</div>
    //     {progressStatus && (
    //       <div className={styles.progressStatus}>
    //         {progressStatus === "COMPLETED" ? "(Completed)" : "(In Progress)"}
    //       </div>
    //     )}
    //   </div>

    //   {/* Module Actions */}
    //   {!!moduleActions?.length && (
    //     <div className={styles.actionsContainer}>
    //       {moduleActions.map((action, idx) => {
    //         return (
    //           <Button
    //             key={idx}
    //             label={action.actionName}
    //             onClickHandler={() => action.action(moduleId)}
    //             variant={idx === 0 ? "primary" : "secondary"}
    //           />
    //         );
    //       })}
    //     </div>
    //   )}
    // </div>
    <Grid item xs={12} md={4}>
      <Card
        variant="outlined"
        sx={{
          boxShadow: "none",
          p: 2.5,
          height: "100%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Avatar
              sx={{
                mr: 1,
                background: companyBrandColor,
              }}
            >
              {companyName[0]}
            </Avatar>
            <Typography variant="h5">{companyName}</Typography>
          </Box>
        </Box>
        <Box
          sx={{
            py: 3,
          }}
        >
          <Typography variant="subtitle2" gutterBottom>
            {durationString}
          </Typography>
          <Typography variant="h3">{moduleTitle}</Typography>
        </Box>
        <Typography variant="subtitle1">Other Participants:</Typography>
        <Box
          display="flex"
          justifyContent="flex-start"
          sx={{
            pb: 3,
          }}
        >
          <AvatarGroup max={5}>
            <Avatar
              sx={{
                width: 30,
                height: 30,
              }}
              alt="Remy Sharp"
              src="http://res.cloudinary.com/baked-potatoes/image/upload/v1651549986/qangbatfwof2yntjmqkr.png"
            />
            <Avatar
              sx={{
                width: 30,
                height: 30,
              }}
              alt="Travis Howard"
              src="http://res.cloudinary.com/baked-potatoes/image/upload/v1651549986/qangbatfwof2yntjmqkr.png"
            />
            <Avatar
              sx={{
                width: 30,
                height: 30,
              }}
              alt="Cindy Baker"
              src="http://res.cloudinary.com/baked-potatoes/image/upload/v1651549986/qangbatfwof2yntjmqkr.png"
            />
            <Avatar
              sx={{
                width: 30,
                height: 30,
              }}
              alt="Agnes Walker"
              src="http://res.cloudinary.com/baked-potatoes/image/upload/v1651549986/qangbatfwof2yntjmqkr.png"
            />
            <Avatar
              sx={{
                width: 30,
                height: 30,
              }}
              alt="Trevor Henderson"
              src="http://res.cloudinary.com/baked-potatoes/image/upload/v1651549986/qangbatfwof2yntjmqkr.png"
            />
          </AvatarGroup>
        </Box>
        {!!moduleActions?.length && (
          <Box>
            {moduleActions.map((action, idx) => {
              return (
                <Button
                  key={idx}
                  label={action.actionName}
                  onClickHandler={() => action.action(moduleId)}
                  variant={idx === 0 ? "primary" : "secondary"}
                />
              );
            })}
          </Box>
        )}
      </Card>
    </Grid>
  );
};

export default ModuleCard;
