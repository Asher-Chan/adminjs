import { NextAPIRes, NextAPIReq } from "@/types/index";
import { prisma } from "@/utils/db";

export default async function handler(
  req: NextAPIReq<
    {},
    {
      userId: string;
      fullName?: string;
      phone?: string;
      major?: string;
      levelOfStudy?: string;
      educationalInstituition?: string;
      jobPosition?: string;
      currentCompany?: string;
    }
  >,
  res: NextAPIRes
) {
  const { userId: formUserId, ...formValues } = req.body;

  if (!formUserId) {
    res.status(400).json({ error: true, errorMessage: "Missing user ID" });
    return;
  }

  const userId = parseInt(formUserId);

  const user = await prisma.user.findUnique({
    where: {
      id: userId,
    },
    include: {
      talent: true,
    },
  });

  if (!user) {
    res.status(400).json({ error: true, errorMessage: "User does not exist" });
    return;
  }

  try {
    await prisma.user.update({
      where: {
        id: userId,
      },
      data: {
        ...(formValues.fullName
          ? { fullName: formValues.fullName, displayName: formValues.fullName }
          : {}),
        ...(formValues.phone ? { phone: formValues.phone } : {}),
        talent: {
          update: {
            ...(formValues.major ? { major: formValues.major } : {}),
            ...(formValues.levelOfStudy
              ? { levelOfStudy: formValues.levelOfStudy }
              : {}),
            ...(formValues.educationalInstituition
              ? { educationalInstituition: formValues.educationalInstituition }
              : {}),
            ...(formValues.jobPosition
              ? { jobTitle: formValues.jobPosition }
              : {}),
            ...(formValues.currentCompany
              ? { currentCompany: formValues.currentCompany }
              : {}),
          },
          // Addon - onboarding complete status
        },
      },
    });

    // send verification email

    res.status(200).json({ error: false });
    return;
  } catch (err) {
    console.log(err);
    res.status(400).json({ error: true, errorMessage: "Error updating user" });
    return;
  }
}
