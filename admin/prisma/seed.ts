import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';

const prisma = new PrismaClient();

const main = async () => {
  // Create default WR admin
  await prisma.user.create({
    data: {
      email: 'admin@whiteroom.work',
      fullName: 'wradmin',
      role: 'ADMIN',
      isActive: true,
      isVerified: true,
      profileImgURL:
        'https://res.cloudinary.com/baked-potatoes/image/upload/v1649748220/nxjpywthp5k9adyhjcdd.png',
      password: await bcrypt.hash(
        'WhiteRoom@@123',
        Number(process.env.HASH_ROUNDS)
      ),
    },
  });
};

main()
  .catch((err) => {
    console.log(err);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
