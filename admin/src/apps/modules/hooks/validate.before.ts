import { ActionRequest, ValidationError } from 'adminjs';

import { ModuleFormValues } from '../../../types/forms';

export const validateData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const {
      company,
      registrationFee,
      desc,
      openingImageURL,
      uploadedOpeningImage,
      estCompletionTimeEnd,
      estCompletionTimeStart,
    } = req.payload as ModuleFormValues;

    const errors: { [key: string]: { message: string } } = {};

    if (!company) {
      errors.company = {
        message: 'Company is required',
      };
    }

    if (!registrationFee) {
      errors.registrationFee = {
        message: 'Registration fee is required',
      };
    }

    if (!desc) {
      errors.desc = {
        message: 'Please fill out why talents should join this module',
      };
    }

    if (!openingImageURL && !uploadedOpeningImage) {
      errors.uploadedOpeningImage = {
        message: 'Opening image is required',
      };
    }

    if (!estCompletionTimeEnd) {
      errors.estCompletionTime = {
        message: 'Estimated completion time end is required',
      };
    }

    if (!estCompletionTimeStart) {
      errors.estCompletionTimeStart = {
        message: 'Estimated completion time start is required',
      };
    }

    if (estCompletionTimeEnd < estCompletionTimeStart) {
      errors.estCompletionTime = {
        message: 'Starting completion time cannot be higher than end time',
      };
    }

    const numOfErrors = Object.keys(errors).length;

    if (numOfErrors) {
      throw new ValidationError(
        {
          ...errors,
        },
        {
          message: 'Some field are missing or invalid.',
        }
      );
    }
  }

  return req;
};
