import React, { useState } from "react";
import { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import { getSession, signIn, SignInResponse } from "next-auth/react";
import { UserRoles } from "@prisma/client";

import TextInput from "@/components/inputs/text-input";
import Button from "@/components/button";

import styles from "./index.module.scss";
import Link from "next/link";
import StatusBar from "@/components/status-bar";

// TODO: Use react-hook-form to handle validations and errors
const SignIn: NextPage = () => {
  const [credentails, setCredentials] = useState({
    email: "",
    password: "",
  });

  const [error, setError] = useState("");

  const router = useRouter();

  const handleSignIn = async () => {
    try {
      const signInResponse = (await signIn("custom-credentials", {
        redirect: false,
        callbackUrl: "/",
        email: credentails.email,
        password: credentails.password,
        requiredUserRole: UserRoles.TALENT,
      })) as unknown as SignInResponse;

      if (signInResponse.error) {
        setError(signInResponse.error);
        return;
      }

      if (signInResponse.ok) {
        router.push("/");
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <h1>Talent Login</h1>
      <Button
        label="Google Sign In"
        onClickHandler={() => signIn("google", { redirect: true })}
        variant="secondary"
      />
      <form onSubmit={(e) => e.preventDefault()}>
        <div className={styles.divider} />
        {!!error && <StatusBar status="error" customMessage={error} />}
        <TextInput
          label="Email"
          name="email"
          type="email"
          onChange={(e) =>
            setCredentials((prev) => ({
              ...prev,
              email: e.target.value,
            }))
          }
          value={credentails.email}
        />
        <TextInput
          label="Password"
          name="password"
          onChange={(e) =>
            setCredentials((prev) => ({
              ...prev,
              password: e.target.value,
            }))
          }
          type="password"
          value={credentails.password}
        />
        <div className={styles.buttonContainer}>
          <Link href="/auth/forgot-password">
            <a className={styles.forgotPasswordLink}>Forgot password?</a>
          </Link>
          <Button label="Sign In" onClickHandler={() => handleSignIn()} />
        </div>
      </form>
    </>
  );
};

export default SignIn;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getSession(ctx);

  if (session?.auth) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};
