
import { Company, Talent, Certificate, Module } from '../../models';

export const dashboardHandler = async () => {
  const totalCompanies = await Company.count();
  const totalTalents = await Talent.count();
  const totalCertificates = await Certificate.count();
  const totalModules = await Module.count();

  return {
    totalCompanies,
    totalTalents,
    totalCertificates,
    totalModules,
  };
};
