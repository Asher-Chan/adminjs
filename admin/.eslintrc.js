module.exports = {
  env: {
    es2021: true,
    node: true,
  },
  extends: ['plugin:react/recommended', 'airbnb', 'prettier'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  globals: {
    NodeJS: true,
  },
  plugins: ['react', '@typescript-eslint'],
  rules: {
    'import/prefer-default-export': 'off',
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'no-console': 'off',
    'import/no-extraneous-dependencies': 0,
    'react/function-component-definition': 0,
    'arrow-body-style': 0,
    'no-unused-vars': 'warn',
    'jsx-a11y/anchor-is-valid': 0,
    radix: 0,
    'react/jsx-filename-extension': [
      2,
      { extensions: ['.js', '.jsx', '.ts', '.tsx'] },
    ],
  },
};
