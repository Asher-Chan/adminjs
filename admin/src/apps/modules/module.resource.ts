import { ResourceOptions } from 'adminjs';

import { getModels } from '../../utils';
import { resourceIds } from '../../constants/resource-ids';
import { navigation } from '../../admin/admin.navigation';
import {
  CompletionTimeInput,
  ImageUpload,
  ImageView,
  ParentRecord,
  TasksList,
} from '../../admin/components.bundler';

import { sortData } from './hooks/sort-data.before';
import { populateTasksData } from './hooks/show.after';
import { redirectAfterSave } from './hooks/redirect.after';
import { fetchCompanies } from './hooks/fetch-companies.action';
import { validateData } from './hooks/validate.before';
import prisma from '../../prisma-client';

const options: ResourceOptions = {
  id: resourceIds.MODULES,
  navigation: navigation.MODULES,
  listProperties: [
    'title',
    'includesCertificate',
    'company',
    'createdAt',
    'updatedAt',
  ],
  filterProperties: [
    'title',
    'registrationFee',
    'includesCertificate',
    'company',
    'createdAt',
    'updatedAt',
  ],
  actions: {
    new: {
      before: [validateData, sortData],
      after: [redirectAfterSave],
    },
    edit: {
      before: [validateData, sortData],
      after: redirectAfterSave,
    },
    show: {
      icon: 'Document',
      after: populateTasksData,
    },
    fetchCompanies: {
      isVisible: false,
      handler: fetchCompanies,
    },
  },
  properties: {
    id: {
      isVisible: false,
    },
    title: {
      isTitle: true,
      props: {
        placeholder: 'Ex. Software Engineering Senior Virtual Experience',
      },
    },
    registrationFee: {
      position: 2,
      isRequired: true,
      availableValues: [
        {
          label: 'Complimentary for Individuals',
          value: 'complimentaryForIndividuals',
        },
        {
          label: 'Complimentary for All',
          value: 'complimentaryForAll',
        },
        {
          label: 'Complimentary for Companies',
          value: 'complimentaryForCompanies',
        },
        {
          label: 'Premium Talent Account Only',
          value: 'premiumTalentAccountOnly',
        },
        {
          label: 'Premium Company Account Only',
          value: 'premiumCompanyAccountOnly',
        },
      ],
    },
    company: {
      position: -2,
      reference: resourceIds.COMPANIES,
      isRequired: true,
      props: {
        searchParam: 'companyId',
        fieldName: 'company',
        actionName: 'fetchCompanies',
        resourceName: resourceIds.MODULES,
      },
      components: {
        edit: ParentRecord,
      },
    },
    estCompletionTime: {
      isRequired: true,
      isVisible: {
        edit: true,
      },
      components: {
        edit: CompletionTimeInput,
      },
    },
    estCompletionTimeStart: {
      type: 'number',
      isVisible: {
        edit: false,
      },
    },
    estCompletionTimeEnd: {
      type: 'number',
      isVisible: {
        edit: false,
      },
    },
    desc: {
      type: 'textarea',
      isRequired: true,
      props: {
        rows: 10,
        placeholder:
          'Ex. ViTrox Corporation Berhad is a Malaysian based electronics company located at Penang, Malaysia. Vitrox specializes in designing and developing automated vision inspection system and equipment testers for the semiconductor and electronic packaging industries as well as electronic communications equipment.',
      },
    },
    uploadedOpeningImage: {
      isRequired: true,
      position: 110,
      custom: {
        fieldName: 'openingImageURL',
      },
      components: {
        edit: ImageUpload,
      },
      isVisible: {
        edit: true,
      },
    },
    openingImageURL: {
      isVisible: {
        show: true,
      },
      components: {
        show: ImageView,
      },
    },
    uploadedIntroVideo: {
      position: 111,
      custom: {
        fieldName: 'introVideoURL',
      },
      components: {
        edit: ImageUpload,
      },
      isVisible: {
        edit: true,
      },
    },
    introVideoURL: {
      isVisible: {
        show: true,
      },
      components: {
        show: ImageView,
      },
    },
    // Tasks
    tasks: {
      reference: resourceIds.TASKS,
      position: 120,
      isVisible: {
        show: true,
      },
      props: {
        showAddButton: true,
      },
      components: {
        show: TasksList,
      },
    },
    createdAt: {
      isVisible: false,
    },
    updatedAt: {
      isVisible: false,
    },
  },
};

export default {
  resource: { model: getModels(prisma).modelMap.Module },
  options,
};
