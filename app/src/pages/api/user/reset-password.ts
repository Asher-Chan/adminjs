import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

import { NextAPIRes, NextAPIReq } from "@/types/index";
import { prisma } from "@/utils/db";

export default async function handler(
  req: NextAPIReq<{}, { token: string; password: string; id: string }>,
  res: NextAPIRes
) {
  const { token, password, id } = req.body;

  if (!token || !password || !id) {
    res.status(400).json({ error: true, errorMessage: "An error occured" });
    return;
  }

  try {
    const user = await prisma.user.findUnique({
      where: {
        id: parseInt(id),
      },
    });

    if (user) {
      const secret = user.password + "-" + user.createdAt; // Unique secret
      jwt.verify(token, secret);

      await prisma.user.update({
        where: {
          id: parseInt(id),
        },
        data: {
          password: await bcrypt.hash(password, 10), // Get from process.env
        },
      });
    }

    res.status(200).json({ error: false });
    return;
  } catch (err) {
    res
      .status(400)
      .json({ error: true, errorMessage: "Unable to reset password" });
    return;
  }
}
