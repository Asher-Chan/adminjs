import React from "react";
import Image from "next/image";
import { Box, Avatar, Typography } from "@mui/material";

import styles from "../index.module.scss";
import { format } from "path";

type Props = {
  profileImgURL?: string;
  talentName: string;
  lastUpdatedModule: Date;
};

const TalentCell: React.FC<Props> = ({
  profileImgURL,
  talentName,
  lastUpdatedModule,
}) => {
  // Calculate time difference between last update

  return (
    // <div className={styles.talentCell}>
    //   <div className={styles.profileImg}>
    //     {profileImgURL && <Image height="100%" width="100%" src={profileImgURL} alt="profile-img" />}
    //   </div>
    //   <div className={styles.talent}>
    //     <div className={styles.talentName}>{talentName}</div>
    //     <p className={styles.lastUpdated}>{lastUpdatedModule.toDateString()}</p>
    //   </div>
    // </div>
    <Box display="flex" alignItems="center">
      <Avatar src={profileImgURL} />
      <Box
        sx={{
          ml: 1,
        }}
      >
        <Typography variant="h5" noWrap>
          {talentName}
        </Typography>
        <Typography variant="subtitle1" noWrap>
          {lastUpdatedModule.toDateString()}
        </Typography>
      </Box>
    </Box>
  );
};

export default TalentCell;
