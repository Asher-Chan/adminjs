import { Users } from './utils/extend-models';
import prisma from './prisma-client';

const {
  tag: Tag,
  talent: Talent,
  user: prismaUser,
  company: Company,
  address: Address,
  module: Module,
  task: Task,
  step: Step,
  stepResource: StepResource,
  submission: Submission,
  certificate: Certificate,
} = prisma;

// Wrapped with model extension
const User = Users(prismaUser);
export {
  Tag,
  Talent,
  Company,
  Address,
  Module,
  User,
  Task,
  Step,
  StepResource,
  Submission,
  Certificate,
};
