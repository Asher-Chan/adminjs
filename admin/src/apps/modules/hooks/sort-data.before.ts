import { ActionRequest } from 'adminjs';
import { ModuleFormValues } from '../../../types/forms';
import { uploadFile } from '../../../utils/cloudinary';

export const sortData = async (req: ActionRequest) => {
  if (req.method === 'post') {
    const { uploadedIntroVideo, uploadedOpeningImage, ...module } =
      req.payload as ModuleFormValues;

    if (uploadedIntroVideo?.path) {
      module.introVideoURL = await uploadFile({
        filePath: uploadedIntroVideo.path,
        options: {
          resource_type: 'raw',
        },
      });
    }

    if (uploadedOpeningImage?.path) {
      module.openingImageURL = await uploadFile({
        filePath: uploadedOpeningImage.path,
      });
    }

    return {
      ...req,
      payload: module,
    };
  }

  return req;
};
