import { Express } from 'express';
import AdminJS from 'adminjs';
import AdminJSExpress from '@adminjs/express';
import { Database, Resource } from '@adminjs/prisma';
import { PrismaClient } from '@prisma/client';

import {
  adminjsAuthentication,
  checkAccess,
  checkLoginRouteFlag,
} from '../utils/auth';

import adminJSConfig from './admin.options';

const dev = process.env.NODE_ENV === 'development';

export const initAdminJSRouter = (app: Express, prisma: PrismaClient): void => {
  AdminJS.registerAdapter({ Database, Resource });

  const adminJs = new AdminJS({
    ...adminJSConfig,
    resources: adminJSConfig.resources.map((config) => ({
      ...config,
      resource: { ...config.resource, client: prisma },
    })),
  });

  if (!dev) {
    const router = AdminJSExpress.buildAuthenticatedRouter(adminJs, {
      authenticate: adminjsAuthentication,
      cookiePassword: process.env.COOKIE_PASSWORD,
    });

    app.use(adminJs.options.loginPath, checkAccess, checkLoginRouteFlag);

    app.use(adminJs.options.rootPath, router);
  } else {
    const router = AdminJSExpress.buildRouter(adminJs);

    app.use(adminJs.options.rootPath, router);
  }
};
