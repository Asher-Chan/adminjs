import React, { ChangeEvent, useState } from "react";
import { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { useInfiniteQuery } from "react-query";

import { ModuleWithCompany } from "@/types/modules";

import styles from "./index.module.scss";

type InfiniteQueryResult = {
  modules: ModuleWithCompany[];
  nextCursor: number;
};

const Modules: NextPage = () => {
  const [filters, setFilters] = useState({
    tags: [],
    status: "",
    search: "",
  });

  const {
    data,
    error,
    isFetching,
    isFetchingNextPage,
    fetchNextPage,
    hasNextPage,
    refetch,
  } = useInfiniteQuery(
    "projects",
    async ({ pageParam = 0 }) => {
      const tagsParam = filters.tags.length
        ? `&tags=${filters.tags.join(",")}`
        : "";
      const searchParam = filters.search ? `&search=${filters.search}` : "";
      const statusParam = filters.status ? `&status=${filters.status}` : "";

      const res = await fetch(
        "/api/modules?cursor=" +
          pageParam +
          searchParam +
          statusParam +
          tagsParam
      );
      const data = await res.json();
      return data as InfiniteQueryResult;
    },
    {
      getNextPageParam: (lastPage) => lastPage.nextCursor,
    }
  );

  const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setFilters((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const onSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
    setFilters((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <div>
      <Head>
        <title>Upsurge | Modules</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div>{isFetching && "Fetching ..."}</div>
      <div>{isFetchingNextPage && "Fetching more ..."}</div>
      <div>{!!error && "Error"}</div>

      <input type="text" name="search" onChange={onInputChange} />
      <input type="text" name="status" onChange={onInputChange} />

      {/* Meant to be a multi-select */}
      <select name="tags" onChange={onSelectChange}>
        <option value="developent">Development</option>
        <option value="design">Design</option>
      </select>

      <button onClick={() => refetch()}>Search</button>

      {/* Modules */}
      <ul className={styles.list}>
        {data?.pages.map((page, idx) => (
          <React.Fragment key={idx}>
            {page.modules.map((module) => (
              <li key={module.id}>
                <div>{module.company?.fullName}</div>
                <div>{`${module.estCompletionTimeStart} - ${module.estCompletionTimeEnd} hrs`}</div>

                <div>{module.title}</div>

                <Link href={`/modules/join/${module.id}`}>
                  <button>Join Now</button>
                </Link>
                <Link href={`/modules/${module.id}`}>
                  <button>More Info</button>
                </Link>
              </li>
            ))}
          </React.Fragment>
        ))}
      </ul>

      <button
        onClick={() => fetchNextPage()}
        disabled={!hasNextPage || isFetchingNextPage}
      >
        Fetch more
      </button>
    </div>
  );
};

export default Modules;
