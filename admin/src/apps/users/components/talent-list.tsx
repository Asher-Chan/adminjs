import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Link } from '@adminjs/design-system';

const TalentList: React.FC<ShowPropertyProps> = (props) => {
  const { record } = props;

  const { talent } = record.populated;

  if (!talent) {
    return <div> - </div>;
  }

  const { params } = talent;

  return (
    <Link
      href={`/admin/resources/Talent/records/${params.id}/show`}
      variant="info"
    >
      View Talent
    </Link>
  );
};

export default TalentList;
