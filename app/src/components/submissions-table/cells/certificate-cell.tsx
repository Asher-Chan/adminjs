import React from "react";
import { CertificateStatus } from "@prisma/client";

import styles from "../index.module.scss";

type Props = {
  status: CertificateStatus;
};

const CertificateCell: React.FC<Props> = ({ status }) => {
  function renderStatus() {
    switch (status) {
      case "AWARDED":
        return <div className={styles.__awardedCert}>Awarded</div>;
      case "PENDING":
        return <div className={styles.__pendingCert}>Pending</div>;
      case "READY_TO_RECIEVE":
        return <div className={styles.awardCertBtn}>Award Now</div>;
      default:
        return null;
    }
  }

  return <div className={styles.certificateStatus}>{renderStatus()}</div>;
};

export default CertificateCell;
