import React from 'react';
import { ShowPropertyProps } from 'adminjs';
import { Box, Label } from '@adminjs/design-system';

const MultiImageView: React.FC<ShowPropertyProps> = ({ record, property }) => {
  const urls: string[] = record.params[property.path]
    ? JSON.parse(record.params[property.path])
    : [];

  return urls.length ? (
    <Box>
      <Label uppercase variant="light">
        {property.label}
      </Label>
      {urls.map((url) => (
        <img key={url} src={url} alt={property.label} />
      ))}
    </Box>
  ) : null;
};

export default MultiImageView;
