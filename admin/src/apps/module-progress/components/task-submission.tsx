/* eslint-disable react/require-default-props */
import React from 'react';
import { Box, Button } from '@adminjs/design-system';
import { SubmissionStatus } from '@prisma/client';
import { saveAs } from 'file-saver';
import styled from 'styled-components';

type Props = {
  taskNumber: string;
  title: string;
  desc: string;
  //   talentName: string;
  submissionDate?: string;
  submissionLink?: string;
  submissionDuration?: string;
  status?: SubmissionStatus;
};

const TaskTitle = styled(Box)`
  text-decoration: underline;
  font-size: large;
`;

const TaskText = styled(Box)`
  font-weight: normal;
  font-size: normal;

  span {
    padding-left: 0.4rem;
    font-weight: bold;
  }
`;

const TaskContainer = styled(Box)`
  border-bottom: 1px solid #7461eb;
  padding: 4rem 0rem;
`;

const SubmissionStatusTag = styled(Box)`
  padding: 0.2rem 0.4rem;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  background-color: ${(props) => (props.isComplete ? '#18C970' : '#FF1A43')};
`;

const SubmissionContainer = styled(Box)`
  padding: 1rem;
  background: #e8ebf9;
  border-radius: 6px;
`;

const SubmissionDate = styled(Box)`
  font-weight: bold;
`;

const DownloadButton = styled(Button)`
  border-radius: 6px;
`;

const TaskSubmission: React.FC<Props> = ({
  taskNumber,
  title,
  desc,
  submissionDate,
  submissionDuration,
  submissionLink,
  status,
  //   talentName,
}) => {
  const handleSubmissionDownload = () => {
    // Can add dynamic filename but we don'tknow what format it is
    saveAs(submissionLink);
  };

  const submissionComplete = status === SubmissionStatus.COMPLETED;

  return (
    <TaskContainer
      p={20}
      flex
      flexDirection="row"
      justifyContent="center"
      alignItems="center"
    >
      <Box flexGrow={1}>
        <Box mb={26} flex>
          <TaskTitle mr={10}>{`${taskNumber} ${title}`}</TaskTitle>
          <SubmissionStatusTag isComplete={submissionComplete}>
            {submissionComplete ? 'Submitted' : 'Not Complete'}
          </SubmissionStatusTag>
        </Box>
        <Box px={16} mb={16}>
          <TaskText>{desc}</TaskText>
        </Box>
        <Box px={16}>
          <TaskText>
            Duration taken to complete:
            <span>{submissionDuration || '-'} hours</span>
          </TaskText>
        </Box>
      </Box>
      <Box flexShrink={0}>
        <SubmissionContainer
          flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          {submissionComplete ? (
            <>
              <Box>Last Submitted:</Box>
              <SubmissionDate py={10}>{submissionDate}</SubmissionDate>
              <DownloadButton
                variant="primary"
                onClick={handleSubmissionDownload}
              >
                Download
              </DownloadButton>
            </>
          ) : (
            <TaskText>No submission</TaskText>
          )}
        </SubmissionContainer>
      </Box>
    </TaskContainer>
  );
};

export default TaskSubmission;
